import puppeteer from 'puppeteer';
import _ from 'lodash';
import fse from 'fs-extra'
import url from "url";
import express from 'express';
import path from 'path';
import critical from 'critical';
import { minify } from "html-minifier";

const chalk = require('chalk');
const Vinyl = require('vinyl');
const parse = url.parse;
const port = process.env.PORT || 8081;
const serverURL = 'http://localhost:' + port;
const app = express();

const getCss = async (allStyles, bodyHTMLold: string) => {
    const cssFiles = allStyles.map((style, index) => {
        return new Vinyl({
            cwd: '/',
            base: '/test/',
            path: '/test/file.js',
            contents: new Buffer(style)
        });
    })

    const { css } = await critical.generate({
        base: './',
        assetPaths: [serverURL],
        html: bodyHTMLold,
        css: cssFiles,
        width: 1366,
        height: 1768,
        extract: true,
        ignore: {
            atrule: ['@font-face'],
            decl: (node, value) => /url\(/.test(value),
        },
    });

    return css
}

fse.copyFileSync(path.resolve(__dirname, 'build', 'index.html'), path.resolve(__dirname, 'build', 'buildIndex.html'))

app.use("/static", express.static(__dirname + '/build/static'));
app.get('*', function (request, response) {
    response.sendFile(path.resolve(__dirname, 'build', 'buildIndex.html'));
});

let allURL = [
    '/articlessecrets',
    '/loader',
    '/404',
    '/word/beyond',
    '/word/forward',
    '/word/pleasure',
    
    // '/articles'
]

const run = async () => {
    let browser: puppeteer.Browser;
    let page: puppeteer.Page;
    try {
        browser = await puppeteer.launch({headless: true});
        page = await browser.newPage();
        page.emulate({
            viewport: {
                width: 1366,
                height: 1768
            },
            userAgent: ''
        });
        page.on('console', msg => console.log('PAGE LOG:', msg.text()));

        for(let i = 0; i < allURL.length; i++) {
            const url = allURL[i];
        // for (const url of allURL) {
            await page.goto(serverURL + url, { timeout: 120 * 1000 });

            await page.waitFor(10 * 1000); // TODO replace with wait for selector for better perf

            await page.evaluate(() => {
                document.querySelectorAll('#react-tooltip').forEach(el => el.remove())
            })
            const bodyHTMLold = await page.evaluate(() => document.documentElement.outerHTML);
            const allStyle = await page.evaluate(() => {
                return Array.from(document.querySelectorAll('style')).map(style => style.innerHTML).join("\n")
            })

            const allStyleFromHref = await page.evaluate(() => {
                return Promise.all(
                    Array.from(
                        document.querySelectorAll('link[rel="stylesheet"]'))
                        .map(item => item.getAttribute('href'))
                        .map(item => fetch(item).then((res) => res.text())
                    )
                )
            })
            

            const css = await getCss([...allStyleFromHref, allStyle], bodyHTMLold);

            const bodyHTML = await page.evaluate((css) => {
                const pageState = (window as any).PAGE_STATE
                // if (pageState) {
                //     document.head.innerHTML += `<script>window.PAGE_STATE=${JSON.stringify(pageState)}</script>`
                // }
                document.head.innerHTML = `<style type="text/css" id="critical-css">${css}</style>` + document.head.innerHTML;
                document.querySelectorAll('link[rel="stylesheet"]').forEach((link: any) => {
                    link.rel = "preload";
                    link.as = "style";
                    link.setAttribute("onload", "this.onload=null;this.rel='stylesheet'");
                })
                document.querySelectorAll('[data-jss]').forEach(item => item.remove())
                if (location.pathname === '/articlessecrets') {
                    document.querySelectorAll('head title, head meta[name="description"], link[rel="canonical"]').forEach(item => item.remove())
                }
                return document.documentElement.outerHTML.split('type="afterBuildSeo"').join('')
                    .replace('<script src="/static', '<script defer src="/static')
                    .replace('<script src="/static', '<script defer src="/static')
            }, css);

            const bodyHTMLMinified = minify(bodyHTML, {
                removeAttributeQuotes: true,
                minifyJS: true
            })

            if (url === '/articlessecrets') {
                const articlesPart = await page.evaluate((bodyHTML) => {
                    return Array.from(document.querySelectorAll('[data-type="article"]')).map(item => {
                        const pageStateAll = (window as any).PAGE_STATE_ARTICLE
                        const pageStateScript = `<script>window.PAGE_STATE=${JSON.stringify(pageStateAll[item.getAttribute('data-url')])}</script>`
                        return ({
                            html: bodyHTML
                                .replace('</head>', item.getAttribute('data-head') + ' ' + pageStateScript + '</head>')
                                .replace(document.querySelector('[data-type=secretParent]').outerHTML, item.innerHTML)
                                .replace(
                                    '<title>Артикли в английском A / An / The / zero article | Clearenglish</title>', 
                                    `<title>${item.getAttribute('data-title')}</title>`
                                ),
                            url: item.getAttribute('data-url')
                        });
                    })
                }, bodyHTML)

                // console.log(articlesPart)
                // console.log(bodyHTML)

                for(const article of articlesPart ) {
                    const minifier = minify(article.html, {
                        removeAttributeQuotes: true,
                        minifyJS: true
                    })
                    await fse.outputFile(`build/article/${article.url}.html`, minifier);
                    console.log(chalk.red(`The Article file [${article.url}] was saved!`));
                }
            }

            const _hrefs = await page.$$eval('a', (as) => (as as HTMLAnchorElement[]).map(a => a.href));
            const hrefs = _hrefs.map(url => parse(url).pathname)
                .filter(url => !url.includes('article/'))
                .filter(url => !url.includes('dashboard/'))
            allURL = _.union(allURL, hrefs) 

            console.log(chalk.red(`We find next URL in this page: ${hrefs.join(" ")}`));
            let fileName = parse(page.url()).pathname
            if (fileName === '/') {
                fileName = '/index'
            }
            await fse.outputFile(`build${fileName}.html`, bodyHTMLMinified);
            console.log(chalk.red(`The file [${fileName}] was saved!`));
        }
    } catch (e) {
        console.warn(e);
    } finally {
        browser.close();
        server.close();
    }
}

const server = app.listen(port, () => {
    run()
});
