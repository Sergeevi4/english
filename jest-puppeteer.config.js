module.exports = {
    launch: {
      headless: (process.env.HEADLESS === 'true'),
    },
    browserContext: 'default',
  }
