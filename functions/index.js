const functions = require('firebase-functions');
const LiqPay = require('./liqpay');
var base64 = require('base-64');
var utf8 = require('utf8');
var admin = require("firebase-admin");
const cors = require('cors')({
    origin: true
});
const serviceAccount = require("./english-9231c-firebase-adminsdk-hhfem-bc5680b201.json");


// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://english-9231c.firebaseio.com",
    storageBucket: "gs://english-9231c.appspot.com"
});

exports.payment = functions.https.onRequest((request, response) => {
    console.log(request.body)
    const orderId = request.body.uid + '_' + Date.now()
    const date = request.body.date
    cors(request, response, () => {
        try {
        var liqpayTest = new LiqPay('sandbox_i94060430074', 'sandbox_Fwf7rKveBknmyOZu3oqIfUX5e4tZueoJ1mQvQfTd');
        var liqpay = new LiqPay('i43421020726', '6eoWsQlnd4bexYE3Wcyqwh55ZXFvuUampcMkaWAN');
        var html29 = liqpay.cnb_form({
            'action': 'subscribe',
            'amount': '14',
            'currency': 'UAH',
            'description': 'Clear English подписка',
            'order_id': orderId,
            'version': '3',
            'subscribe': '1',
            'server_url': 'https://us-central1-english-9231c.cloudfunctions.net/liqPayCallback',
            'subscribe_periodicity': 'month',
            'subscribe_date_start': date,
            'result_url': 'https://clearenglish.io/payment-process'
        })
        var html99 = liqpay.cnb_form({
            'action': 'subscribe',
            'amount': '49',
            'currency': 'UAH',
            'description': 'Clear English подписка',
            'order_id': orderId,
            'version': '3',
            'subscribe': '1',
            'server_url': 'https://us-central1-english-9231c.cloudfunctions.net/liqPayCallback',
            'subscribe_periodicity': 'month',
            'subscribe_date_start': date,
            'result_url': 'https://clearenglish.io/payment-process'
        })
        var html29Test = liqpayTest.cnb_form({
            'action': 'subscribe',
            'amount': '14',
            'currency': 'UAH',
            'description': 'Clear English подписка',
            'order_id': orderId,
            'version': '3',
            'subscribe': '1',
            'server_url': 'https://us-central1-english-9231c.cloudfunctions.net/liqPayCallback',
            'subscribe_periodicity': 'month',
            'subscribe_date_start': date,
            'result_url': 'http://localhost:3000/payment-process'
        })
        var html99Test = liqpayTest.cnb_form({
            'action': 'subscribe',
            'amount': '49',
            'currency': 'UAH',
            'description': 'Clear English подписка',
            'order_id': orderId,
            'version': '3',
            'subscribe': '1',
            'server_url': 'https://us-central1-english-9231c.cloudfunctions.net/liqPayCallback',
            'subscribe_periodicity': 'month',
            'subscribe_date_start': date,
            'result_url': 'http://localhost:3000/payment-process'
        })
        response.send({html29, html99, html29Test, html99Test})
        
        
        }catch(e){
            response.send(e.message)
        }
    })
});


exports.liqPayCallback = functions.https.onRequest((request, response) => {
        console.log(request.body)
        
        const bytes = base64.decode(request.body.data)
        const decode = utf8.decode(bytes);

        //const decode = Buffer.from(testCode64, 'base64').toString('utf-8')
        const parse = JSON.parse(decode)
        const userId = parse.order_id.split('_')[0]
        let paymentStatus = ''
        if(parse.amount === 14){
            paymentStatus = 'standard'
        }
        if(parse.amount === 49){
            paymentStatus = 'premium'
        }
        admin.database().ref('users/' + userId + '/payment').update({
            fullInfo: parse,
            status: paymentStatus,
            date: Date.now()
        });
        response.send()
});