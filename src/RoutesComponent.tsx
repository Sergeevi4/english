import React from "react";
import { Redirect, Route } from "react-router-dom";
import { connect } from "./Store";
import Spinner from "./Components/Spinner/Spinner";

export const PrivateRoute = connect("user")(({ component: Component, store, ...rest }) => {
  if (store.user === undefined) {
    return <Spinner />
  }
  return (<Route {...rest} render={(reactRouterData) => {
    return store.user
      ? <Component {...reactRouterData} />
      : <Redirect to={{ pathname: '/' }} />;
  }} />);
})

export const GuestRoute = connect("user")(({ component: Component, store, ...rest }) => {
  // if (store.user === undefined) {
  //   return <div>Loading...</div>
  // }
  return (<Route {...rest} render={(reactRouterData) => {
    return !store.user
      ? <Component {...reactRouterData} />
      : <Redirect to={{ pathname: '/dashboard' }} />;
  }} />);
})

