import * as React from 'react';
import _ from 'lodash';
import Alert from 'react-s-alert';
import { Switch, Route } from 'react-router-dom';
import { set as storeSet } from "./Store";
import { auth, database } from 'firebase/app';
import history from './history'

import Auth from './Components/Auth/Auth';
import Header from './Components/Header/Header';
import { ResetPassword } from './Components/ResetPassword/ResetPassword';
import Articles from './Components/Article/Articles';
import { GuestRoute, PrivateRoute } from './RoutesComponent';
import Article from './Components/Article/Article';
import { asyncComponent } from './Components/AsyncComponent/AsyncComponent';
import { WordInfo } from './Components/Word/word';
import { PrivacyPolicy } from './Components/PrivacyPolicy/PrivacyPolicy';
import Landing from './Components/LandingPage/Landing';
import ReactTooltip from 'react-tooltip';
import { PaymentProcess }  from './Components/PaymentProcess/PaymentProcess';

import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';

import 'react-s-alert/dist/s-alert-default.css';
import './App.scss';
import { Page404 } from './Components/Page404/Page404';
import Footer from './Components/Footer/Footer';
import Spinner from './Components/Spinner/Spinner';
import { AboutUs } from './Components/AboutUs/AboutUs';
import { getSignUpUserInfo } from './getSignUpUserInfo';
import { Terms } from './Components/Terms/Terms';
import { API } from './API';
import { isUserAndUserInfoLoadedResolve, dateForPayment } from './Utilities';
import { hot } from 'react-hot-loader/root'
import { ArticlesSecrets } from './Components/Article/ArticlesSecrets';
import UserLevel from './Components/UserLevel/UserLevel';
// import { NewLanding } from './Components/LandingPage/NewLanding';
// import './parseMuller'  // IMPORTANT, uncomment will cause to load BIG JSON, and parse 50K words, and upload it to firebase

const Dashboard = asyncComponent(() => import('./Dashboard').then(result => result.Dashboard));

const theme = createMuiTheme({
  palette: {
    primary: { main: "#5b86e5" }, // Purple and green play nicely together.
  },
  typography: {
    useNextVariants: true,
  },
});

let onPaymentLoaded: Function
export let paymentButtons = new Promise(resolve => {
  onPaymentLoaded = resolve;
})

class App extends React.Component<{}, {}> {
  componentDidMount() {
    console.log('%c Oh my heavens! Hacker learn english?', 'background: #222; color: #bada55; font-size: 24px');
    console.log('%c return - вернуть; \n if - если; \n every - каждый; \n some - некоторый', 'background: #222; color: #bada55; font-size: 16px');
    if (process.env.NODE_ENV === 'development') {
      document.documentElement.className += ' fonts-loaded '
    }

    auth().onAuthStateChanged((user) => {
      storeSet({ user: user });
      if (!user) return;

      const body = JSON.stringify({
        uid: user.uid,
        date: dateForPayment()
      })
      fetch('https://us-central1-english-9231c.cloudfunctions.net/payment', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body
      }).then(response => onPaymentLoaded(response.json()))


      // const moveBook = () => {
      //   API.myUser().once((data: any) => {
      //     API.books().update({
      //       sherlockHolmes: data.books["-Lml3Umi4vMP_JKRPNUk"]
      //     })
      //   })
  
      //   API.books().once(data => {
      //     API.booksPreview().update({
      //       ...data
      //     })
      //   })
      // }

      // moveBook();
      
      

      if (window.authWithPayment) {
        history.push('/dashboard/account/payment')
        window.authWithPayment = false
      }

      const setUserInfoToFB = _.once((isNewUser) => {
        isNewUser && history.push('/user-level')
        API.myUser().update({
          uid: user.uid,
          photoURL: user.photoURL,
          email: user.email,
          displayName: user.displayName,
          online: true,
          ...getSignUpUserInfo(isNewUser, user), // detect login or signup
        })

        API.myUser().stat('visit').value.push({
          from: database.ServerValue.TIMESTAMP,
          to: 'nowOnline'
        }).onDisconnect().update({ to: database.ServerValue.TIMESTAMP })
      })

      API.myUser().on((_userInfo) => {
        const userInfo = _userInfo || {}
        userInfo.payment = userInfo.payment || { status: 'basic' }
        storeSet({ userInfo })
        setUserInfoToFB(_userInfo === null); // detect login or signup, if we cannot find user in FB - it is new user
        isUserAndUserInfoLoadedResolve();
      })
      API.myUser().onDisconnect().update({ online: false })
    })
  }

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <Alert stack={{ limit: 3 }} html={true} />
          <Header />
          <ReactTooltip />
          <CssBaseline />
          <Switch>
            <GuestRoute exact={true} path='/' component={Landing} />
            <GuestRoute path='/:type(login|signup)' component={Auth} />
            <GuestRoute path='/:type(invite)/:uid' component={Auth} />
            <GuestRoute path='/reset-password' component={ResetPassword} />
            <Route path='/articles' component={Articles} />

            <Route path='/article/:id' component={Article} />
            <Route path='/word/:id' component={WordInfo} />
            <Route path='/privacy-policy' component={PrivacyPolicy} />
            <Route path='/about-us' component={AboutUs} />
            <Route path='/terms-and-conditions' component={Terms} />
            <Route path='/payment-process' component={PaymentProcess} />
            <Route path='/articlesSecrets' component={ArticlesSecrets} /> {/* This page use only for build SEO, nobody should visit it! */}
            <Route path='/loader' component={() => <Spinner />} /> {/* This page use only for build SEO, nobody should visit it! */}
            <PrivateRoute path='/user-level' component={UserLevel} />
            <Route path='/dashboard' component={Dashboard} />
            <Route component={Page404} />
          </Switch>
          <Footer />
        </div>
      </MuiThemeProvider>
    );
  }
}




export default process.env.NODE_ENV === "development" ? hot(App) : App
// export default App;
