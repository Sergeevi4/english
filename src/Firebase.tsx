// import * as firebase from 'firebase';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = process.env.NODE_ENV === 'development' 
  ? {
    apiKey: "AIzaSyC-k60odjae8yF_Jt14XaV22gXVsL39VRE",
    authDomain: "englishdev-a5ccb.firebaseapp.com",
    databaseURL: "https://englishdev-a5ccb.firebaseio.com",
    projectId: "englishdev-a5ccb",
    storageBucket: "",
    messagingSenderId: "171246082034",
    appId: "1:171246082034:web:6ec9d4cc3fb18fe6"
  }
  : {
    apiKey: "AIzaSyDYeiKt-EZr_XBZwgx6UhwzTonjSqH4Niw",
    authDomain: "english-9231c.firebaseapp.com",
    databaseURL: "https://english-9231c.firebaseio.com",
    projectId: "english-9231c",
    storageBucket: "english-9231c.appspot.com",
    messagingSenderId: "359597734709"
  }

firebase.initializeApp(config)



export default firebase;
