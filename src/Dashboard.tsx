import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import _ from 'lodash';
import {Books} from './Components/Book/Books';
import MainPage from './Components/MainPage/MainPage';
import {VocabularyTabs, Pack} from './Components/Vocabulary/index';
import Training from './Components/Training/Training';
import UserLevel from './Components/UserLevel/UserLevel';
import { PrivateRoute } from './RoutesComponent'

import 'react-s-alert/dist/s-alert-default.css';
import './App.scss';

import ErrorBoundary from './Components/ErrorBoundary/ErrorBoundary';
import UserPage from './Components/UserPage/UserPage';
import Watch, { watchDev } from './Components/Watch/Watch';
import { Page404 } from './Components/Page404/Page404';
import Helmet from 'react-helmet';
import {StartTrainScreen} from './Components/StartTrainScreen/StartTrainScreen';

import { hot } from 'react-hot-loader';
import Book from './Components/Book/Book';


class _Dashboard extends React.Component<{}, {}> {
    render() {
        return <ErrorBoundary>
            <Helmet defer={false}>
                <title>Учи английский просто</title>
            </Helmet>
            <Switch>
                <PrivateRoute path='/dashboard' exact={true} component={() => <ErrorBoundary><MainPage /></ErrorBoundary>} />
                <PrivateRoute path='/dashboard/user-level' component={UserLevel} />
                <PrivateRoute path='/dashboard/books' component={Books} />
                <PrivateRoute path='/dashboard/book/:id' component={Book} />
                <PrivateRoute path='/dashboard/vocabulary' component={VocabularyTabs} />
                <PrivateRoute path='/dashboard/pack/:id' component={Pack} />
                <PrivateRoute path='/dashboard/account/payment' component={UserPage} />
                <PrivateRoute path='/dashboard/account' component={UserPage} />
                <PrivateRoute path='/dashboard/watch' component={Watch} />
                <PrivateRoute path='/dashboard/watchDev' component={watchDev} />
                <PrivateRoute path='/dashboard/start-train/:type' component={StartTrainScreen} />
                <PrivateRoute path='/dashboard/training/:type(translate-eng-ru|translate-for-time|translate-from-audio|write|context)' component={Training} />
                <Route component={Page404} />
            </Switch>
        </ErrorBoundary>
    }
}

export const Dashboard =  hot(module)(_Dashboard); // <-- module will reload itself
