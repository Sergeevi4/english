import React, { Component } from "react";

export const asyncComponent = (importComponent) => class AsyncComponent extends Component {
    state = {
        component: null
    };

    async componentDidMount() {
        let result = await importComponent();
        let component = result.default || result
        if (!component) {
            console.warn('Something wrong with loading async component!')
        }

        this.setState({
            component: component,
        });
    }

    render() {
        const Component = this.state.component;
        return Component ? <Component {...this.props} /> : null;
    }
}
