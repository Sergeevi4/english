import React, { useState, useEffect } from 'react'

import styles from './Subscription.module.scss'
import { Button, Card } from '@material-ui/core';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import { ReactComponent as SubCheckIcon } from '../icons/subCheckIcon.svg'
import { ReactComponent as PaymentLine } from '../icons/paymentLine.svg'
import { paymentButtons } from '../../App';
import { useStore } from '../../Utilities';

interface IProps {
    type: string
}

export const Subscription = (props: IProps) => {
    const [payButton, setPayButton] = useState('')
    const store = useStore()

    useEffect(() => {
        if(!store.user) {return undefined}
        paymentButtons.then(result => setPayButton(result as any))
    }, [])

    const MyLink = (props) => {
        return <Link to={'./signup'} {...props} />
    }
    
    const data = [
        {
            title: 'Базовый',
            price: "Бесплатно",
            buttonText: "Используется сейчас",
            firstText: <>Доступ к <b>5 тренировкам</b> в день</>,
            secondText: <>Доступ книгам <b>20 минут</b> в день</>,
            thirdText: <>Возможность <b>влиять</b> на развитие проекта</>,
            fourthText: <>Возможность изучения <b>1 набора</b> слов</>,
            liqPayKey: '',
            withLine: false
        },
        {
            title: "Премиум",
            price: <><b>49</b> <b>грн</b>/месяц</>,
            buttonText: "Присоединиться",
            firstText: <><b>Безлимитный</b> доступ ко всем тренировкам</>,
            secondText: <><b>Безлимитный</b> доступ к книгам</>,
            thirdText: <>Приоритетная возможность <b>влиять</b> на развитие проекта</>,
            fourthText: <><b>Безлимитную</b> возможность добавлять наборы слов в словарь</>,
            liqPayKey: 'html99',
            withLine: false
        },
        {
            title: "Стандарт",
            price: <><b>14</b> <b>грн</b>/месяц</>,
            buttonText: "Присоединиться",
            firstText: <>Доступ к <b>10 тренировкам</b> в день</>,
            secondText: <>Доступ к книгам <b>60 минут</b> в день</>,
            thirdText: <>Приоритетная возможность <b>влиять</b> на развитие проекта</>,
            fourthText: <>Возможность изучения <b>3 наборов</b> слов</>,
            liqPayKey: 'html29',
            withLine: false
        }
    ]

    const SubItem = _.map(data, ({ title, price, buttonText, firstText, secondText, thirdText, fourthText, liqPayKey, withLine }) => {
        return <Card className={styles.subWrapper} key={title}>
            {withLine && <PaymentLine className={styles.paymentLine} />}
            <div className={styles.subTitle}>{title}</div>
            <div className={styles.textWrapper}>
                <div className={styles.lineWrapper}>
                    <SubCheckIcon className={styles.subCheckIcon} />
                    <div className={styles.subTextLine}>
                        {firstText}
                    </div>
                </div>
                <div className={styles.lineWrapper}>
                    <SubCheckIcon className={styles.subCheckIcon} />
                    <div className={styles.subTextLine}>
                        {thirdText}
                    </div>
                </div>
                <div className={styles.lineWrapper}>
                    <SubCheckIcon className={styles.subCheckIcon} />
                    <div className={styles.subTextLine}>
                        {fourthText}
                    </div>
                </div>
            </div>
            <div className={styles.subPrice}>{price}</div>
            {(liqPayKey && props.type === 'userPageSub')
                ?
                <div dangerouslySetInnerHTML={{ __html: payButton[liqPayKey] }} />
                :
                <Button
                    disabled={buttonText === "Используется сейчас"}
                    component={MyLink}
                    onClick={() => {
                        window.authWithPayment = true
                    }}
                    classes={{ label: styles.subButtonText, root: styles.subButtonBack }}
                    variant="contained"
                    className={styles.subButton}
                >
                    {buttonText}
                </Button>
            }
        </Card>
    })

    return (
        <>
            <div className={styles.mainWrapper + (props.type === 'userPageSub' ? (' ' + styles.userPageSub) : '')}>{SubItem}</div>
            <div className={styles.detailDescription}>
                <h2>Тренировки</h2>
                У нас есть три тренировки, вам дается одно английское слово (либо его аудиозапись) и 5 вариантов его перевода на русский, вам нужно выбрать правильный.
                <h2>Наборы слов</h2>
                Вы можете выбирать какие слова вы хотите учить!
                Мы сделали пять наборов слов: <br />
                "CSS + HTML", <br />
                "Пляжный отдых", <br />
                "150 самых популярных слов", <br />
                "Слова из игры <strong>Dota 2</strong>", <br />
                "Слова из игры  <strong>CS:GO</strong>".
                <h2>Возможность влиять на развитие проекта</h2>
                Также на нашем сайте много нового функционала в разработке, и именно вы решаете какой из них появится первым
            </div>
        </>
    )
}