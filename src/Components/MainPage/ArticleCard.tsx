import React from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import ReactTooltip from 'react-tooltip';
import Spinner from '../Spinner/Spinner';
import { getDateString } from '../../Utilities';
import { connect } from '../../Store';

import { ReactComponent as CheckIcon } from '../icons/checkIcon.svg'

import styles from './ArticleCard.module.scss'
import { API } from '../../API';
import { Store } from '../../interface';

interface IState {
    haveSeenArticles: any
}

interface IProps {
    url: string,
    date: number,
    title: string,
    store: Store
}

class ArticleCard extends React.Component<IProps, IState> {

    state: IState = {
        haveSeenArticles: undefined
    }

    async componentDidMount() {
        if (!this.props.store.user) return
        const haveSeenArticles = await API.myUser().haveSeenArticles().once();
        this.setState({ haveSeenArticles })
    }

    render() {
        if (this.state.haveSeenArticles === undefined) return <Spinner />
        const time = !this.state.haveSeenArticles
            ? 0
            : _.get(this.state.haveSeenArticles, `[${this.props.url}].time`) || 0
        const checkTime = time > 20000
        const checkTooltipText = checkTime && "Читал(а)"
        return (
            <Link to={"/article/" + this.props.url} className={styles.wrapper}>
                <ReactTooltip />
                <div className={styles.articleCard}>
                    {checkTime
                        &&
                        <div
                            data-tip={checkTooltipText}
                            className={styles.checkIcon}
                        >
                            <CheckIcon
                                className={styles.ArticleCheckIcon}
                            />
                        </div>
                    }
                    <div className={styles.footer}>
                        <div className={styles.footerDate}>
                            {getDateString(new Date(+this.props.date))}
                        </div>
                    </div>
                    <div className={styles.title}>
                        {this.props.title}
                    </div>
                </div>
            </Link>
        );
    }
}

export default connect("user")(ArticleCard);
