import React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames'

import styles from './TrainingCard.module.scss'
import { Card } from '@material-ui/core';

interface IProps {
    url: string,
    name: string,
    hoverText: string,
    children: any,
    isDisabled: boolean,
    onClick(): any,
    className: string,
    finishCounter?: number
}

class TrainingCard extends React.Component<IProps, any>{

    onClick = (e) => {
        if(this.props.isDisabled){
            e.preventDefault()
            this.props.onClick()
        }
    }

    render() {
        const MyLink = (props) => {
            return <Link to={"/dashboard/start-train/" + this.props.url }{...props}/>
        }
        return(
            <Card onClick={this.onClick} className={cn(styles.trainingWrapper, styles.trainingCard, this.props.className)} component={MyLink}>
                <div className={styles.trainingTitle}>
                    {this.props.name}
                </div>
                <div className={styles.hoverText}>
                    {this.props.hoverText}
                </div>
                <div className={styles.achiveWrapper}>{this.props.children}</div>
            </Card>
        )
    }
}
export default TrainingCard;
