import React from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import ArticleCard from './ArticleCard';
import TrainingCard from './TrainingCard';
import Spinner from '../Spinner/Spinner';
import cn from 'classnames';

import { ReactComponent as Arrow } from '../icons/right-arrow.svg'
import { connect } from '../../Store';
import Achive, { allAchives } from '../Achive/Achive';
import { allTrainingsInfo } from '../../Infos';
import { API } from '../../API';
import { LockPopup } from '../LockPopup/LockPopup';
import UserDailyRecomendation from '../UserDailyRecomendation/UserDailyRecomendation';
import { getEnergyInfo } from '../../Utilities';

import styles from './MainPage.module.scss'

interface IState {
  allArticles: any,
  activeStep: number,
  isLockPopupShow: Boolean
}

class MainPage extends React.Component<any, IState> {

  state:IState = {
    allArticles: null,
    activeStep: 0,
    isLockPopupShow: false
  }

  async componentDidMount() {
    const allArticles = await API.articlePreview().once();
    this.setState({ allArticles })
  }

  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  renderArticles = () => {
    if (!this.state.allArticles) return <Spinner />
    const sortdate = _.sortBy(this.state.allArticles, (article) => -article.date)
    const getFewArticles = _.take(sortdate, 8)
    return _.map(getFewArticles, ({ title, date, author, url }, key) => <ArticleCard
      key={key}
      title={title}
      date={date}
      author={author}
      url={url}
    />)
  }

  lockPopupChangeState = (isOpen: Boolean) => {
    this.setState({isLockPopupShow: isOpen})
  }

  renderTrainings = () => {
    if(!this.props.store.userInfo) return <Spinner />
    const trainList = _.map(allTrainingsInfo, ({ name, url, hoverText }, key) => {
      const currentStats = _.map(getEnergyInfo().userStats[url])
      const finishCounter = _.size(currentStats)
      return <TrainingCard
        hoverText={hoverText}
        key={key}
        name={name}
        url={url}
        finishCounter={finishCounter}
        onClick={() => this.lockPopupChangeState(true)}
        isDisabled={getEnergyInfo().isTrainsLock}
        className={getEnergyInfo().isTrainsLock ? 'is-disabled' : ''}
      ><Achive
          id={url + '1'}
          userData={getEnergyInfo().userStats[url]}
          achiveData={allAchives['trains']}
        />
        <Achive
          id={url + '2'}
          userData={getEnergyInfo().userStats[url]}
          achiveData={allAchives['trainsMistake']}
        />
      </TrainingCard>
    })

    return <div className={styles.mainPageTrainingBlock}>
      <div className={styles.mainPageTrainingHeader}>
        <div className={styles.trainsHelpWrapper}>
          <div className={styles.trainingTitle}>
            Тренировки
            </div>
        </div>
      </div>
      <div className={styles.itemsWrapper + ' ' + styles.trainingItems}>
        {trainList}
      </div>
    </div>
  }

  render() {
    return (
      <div className={cn(styles.mainPage, 'global pl pr')}>
        <div>
          <LockPopup open={this.state.isLockPopupShow} onClose={() => this.lockPopupChangeState(false)}/>
          {this.renderTrainings()}
          {/* <UserDailyRecomendation /> */}
        </div>
        <div className={styles.articles}>
          <div className={styles.articlesHeader}>
            <div className={styles.newArticles}>
              Новые статьи
            </div>
            <Link to="/articles" className={styles.allArticles}>
              Все <Arrow className={styles.arrowSvg} />
            </Link>
          </div>
          <div className={styles.itemsWrapper}>
            {this.renderArticles()}
          </div>
        </div>
      </div>
    );
  }
}

export default connect('_ALL')(MainPage);
