import React from 'react';
import { connect } from "../../Store";
import { auth } from 'firebase/app';
import onClickOutside from "react-onclickoutside";

import { ReactComponent as UserIcon } from '../icons/headerUser.svg'
import { ReactComponent as Star } from '../icons/userMenuStar.svg'
import { ReactComponent as Exit } from '../icons/exitIcon.svg'
import { Link } from 'react-router-dom';

import userIcon from '../icons/headerUser.svg'
import { ReactComponent as Arrow } from '../icons/right-arrow.svg'

import styles from "./User.module.scss"
import { Card, Paper } from '@material-ui/core';

class UserComponentClass extends React.Component<any, any>{

    render() {
        const userInfo = this.props.userInfo || this.props.store.user
        const renderUser = <>
            <img className={styles.userPhotoMobile} src={userInfo.photoURL || userIcon} alt='User photo' />
            <div className={styles.mainInfo}>
                <div className={styles.name}>{userInfo.displayName}</div>
                {this.props.type != 'header-menu' && <div className={styles.email}>{userInfo.email}</div>}
                {this.props.type === 'header-menu' && <div className={styles.details}>Детальнее <Arrow className={styles.detailsIcon} /></div>}
            </div>
        </>
        return this.props.type === 'header-menu'
            ? <Link className={styles.topSection} to={"/dashboard/account"} onClick={this.props.closeMenu}>{renderUser}</Link>
            : <div className={styles.topSection}>{renderUser}</div>
    }
}

export const UserComponent = connect('user')(UserComponentClass)

class UserInHeaderComponentClass extends React.Component<any, any> {

    state = {
        userMenuIsShow: false
    }

    logOut = () => {
        auth().signOut()
    }

    handleClickOutside = evt => {
        this.setState({
            userMenuIsShow: false
        })
    }

    userMenuShow = () => {
        this.setState({
            userMenuIsShow: true
        })
    }

    render() {
        // TODO looks like this div can be removed!
        const paymentStatus = {
            basic: "Базовый",
            standard: "Стандарт",
            premium: "Премиум"
        }
        return (
            <div className={styles.userWrapper} style={{ backgroundImage: `url(${this.props.store.user.photoURL})` }} onClick={this.userMenuShow} >
                <UserIcon className={styles.userMockIcon} />
                {this.state.userMenuIsShow && <Paper className={styles.userMenu}>
                    <Link to={'/dashboard/account'} className={styles.userName}>
                        <div className={styles.itemWrapper + " " + styles.userInfo}>
                            <div >
                                {this.props.store.user.displayName || "Гость"}
                            </div>
                            <div className={styles.paymentStatus}>
                                {paymentStatus[this.props.store.paymentStatus]}
                            </div>
                            <Arrow className={styles.detailsIconAbsolute} />
                        </div>
                    </Link>
                    {/*<div className="userName-itemWrapper">
                        <Star className="userMenu-icon" />
                        <div className="userMenu-text">Уровень: Basic </div>
                    </div>*/}
                    <div className={styles.itemWrapper}>
                        <Exit className={styles.ExitIcon} />
                        <div onClick={this.logOut} className={styles.exit}>Выход</div>
                    </div>
                </Paper>}
            </div>
        )
    }
}

export const UserInHeaderComponent = connect("user")(onClickOutside(UserInHeaderComponentClass))