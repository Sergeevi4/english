import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "../../Store";
import { IPropsStore } from '../../interface';
import { auth } from 'firebase/app';
import AppBar from '@material-ui/core/AppBar';
import { Drawer, IconButton } from '@material-ui/core';
import LockIcon from '@material-ui/icons/LockRounded';
import cn from 'classnames';
import ReactTooltip from 'react-tooltip';

import { ReactComponent as EnergyIcon } from '../icons/energyIcon.svg'
import { ReactComponent as LogoIcon } from '../icons/Logo.svg'
import { ReactComponent as MenuIcon } from '../icons/headerMenu.svg'
import { ReactComponent as ExitIcon } from '../icons/exitIcon.svg'

import styles from "./Header.module.scss"
import { UserComponent } from './User';
import { UserInHeaderComponent } from './User'
import _ from 'lodash';
import { LinkButton } from '../UI/LinkButton';
import { getEnergyInfo, useIntervalRerender } from '../../Utilities';

const data = [
  { title: 'Учиться', url: '/dashboard', isCommingSoon: false },
  { title: 'Книги', url: '/dashboard/books', isCommingSoon: true },
  { title: 'Словарь', url: '/dashboard/vocabulary', isCommingSoon: false },
  // {title: 'Фильмы', url: '/dashboard/watch', isCommingSoon: true},
  { title: 'Статьи', url: '/articles', isCommingSoon: false },
]

const buttonProps = { color: "inherit", variant: "text" }

const EnergyComponent = () => {
  const { maxAllowedEnergy, energy, MILLISECOND_TO_ONE_RESTORE, milliSecondToRestore, formattedTimeToRestoreOneEnergy } = getEnergyInfo();
  useIntervalRerender();

  const timeToRestoreOneEnergy = milliSecondToRestore % MILLISECOND_TO_ONE_RESTORE

  if (maxAllowedEnergy === +Infinity) {
    return null
  }

  console.log('forCheckRerenders', formattedTimeToRestoreOneEnergy)

  const tooltip = (timeToRestoreOneEnergy > 0) && <ReactTooltip id="Energy" effect="solid" getContent={[() => `До пополнения енергии осталось: ${formattedTimeToRestoreOneEnergy}`, 1000]}/>

  return <div className={styles.AttemptsForDay} data-for={"Energy"} data-tip>
    {tooltip}
    {energy}/{maxAllowedEnergy}
    <EnergyIcon className={styles.energyIcon} />
  </div>

}

class Header extends React.Component<IPropsStore, { menuIsShow: boolean }> {
  state = {
    menuIsShow: false,
  }

  onMenuChange = () => {
    this.setState({
      menuIsShow: !this.state.menuIsShow
    })
  }

  renderDesktopHeaderUserSectionWhenLogin = () => {
    if (!this.props.store.user || !this.props.store.userInfo) { return null }

    const { maxAllowedEnergy, energy } = getEnergyInfo();

    return <div className={styles.userPayment}>


      <EnergyComponent />


      {this.props.store.paymentStatus !== 'premium' &&
        <LinkButton {...buttonProps} to="/dashboard/account/payment" className={styles.paymentButton}>Премиум</LinkButton>
      }
      <LinkButton to="/dashboard/account" {...buttonProps} className={styles.headerButton + ' ' + styles.myPageHeader}>
        Моя страница
      </LinkButton>
      <UserInHeaderComponent />
    </div>
  }

  logOut = () => {
    auth().signOut()
    this.setState({
      menuIsShow: !this.state.menuIsShow
    })
  }

  renderMobileHeaderNavigationWhenLogin = () => {
    if (!this.props.store.user) { return null }
    const renderMenu = _.map(data, ({ title, url, isCommingSoon }) => {
      return <Link
        to={url}
        onClick={this.onMenuChange}
        className={styles.menuItem}
        key={title}
      >
        {title}
        {isCommingSoon && <div className={styles.comingSoonCircle} />}
      </Link>
    })

    return <Drawer
      open={this.state.menuIsShow}
      onClose={this.onMenuChange}
    >
      <div className={styles.menu}>
        <div className={styles.menuCross} onClick={this.onMenuChange}>&#10006;</div>
        <UserComponent type={'header-menu'} closeMenu={this.onMenuChange} />
        <div className={styles.navWrapper}>
          {renderMenu}
        </div>
        <div className={styles.exitWrapper} onClick={this.logOut}>
          <ExitIcon className={styles.exitIcon} />
          <div className={styles.exitButton}>Выход</div>
        </div>
      </div>
    </Drawer>

  }

  renderMobileHeaderNavigationWhenNoLogin = () => {
    return <Drawer open={this.state.menuIsShow} onClose={this.onMenuChange}>
      <div className={styles.menu + " " + styles.notLogin}>
        <div className={styles.menuCross} onClick={this.onMenuChange}>&#10006;</div>
        <Link to={'/articles'} onClick={this.onMenuChange} className={styles.articlesButton}>
          Статьи
        </Link>
        <div className={styles.authSectionWrapper}>
          <Link
            to={'/login'}
            onClick={this.onMenuChange}
            className={styles.menuItem + ' ' + styles.noLogin}
          >
            Вход
          </Link>
          <Link
            to={'/signup'}
            onClick={this.onMenuChange}
            className={styles.menuItem + ' ' + styles.noSignup}
          >
            Регистрация
        </Link>
        </div>
      </div>
    </Drawer>
  }


  renderDesktopHeaderUserSectionWhenNoLogin = () => {
    if (this.props.store.user === undefined || this.props.store.user) { return null }
    return <div className={styles.authButtons}>
      <LinkButton {...buttonProps} to="/login" className={styles.authButton}>Вход</LinkButton>
      <LinkButton {...buttonProps} to="/signup" className={styles.signupButton}>Регистрация</LinkButton>
    </div>
  }

  renderDesktopHeaderHavigationWhenLogin = () => {
    return _.map(data, ({ title, url, isCommingSoon }) => {
      return <LinkButton to={url} key={title} color="inherit" variant="text" className={styles.NavigationButton}>
        <div>{title}</div>
        {isCommingSoon && <div className={styles.comingSoonCircle} />}
      </LinkButton>
    })
  }

  render() {
    console.count('header render')
    const user = this.props.store.user
    const checkMenuRender = user ? this.renderMobileHeaderNavigationWhenLogin() : this.renderMobileHeaderNavigationWhenNoLogin()

    return <AppBar position="static" className={cn(styles.header, 'global pl pr')}>
      <MenuIcon className={styles.menuIcon} onClick={this.onMenuChange} />
      {checkMenuRender}
      <div className={styles.navbarWrapper}>
        <Link to={user ? "/dashboard" : "/"} className={styles.logoLink}>
          <div className={styles.logoWrapper}>
            <LogoIcon className={styles.logo} />
            <div className={styles.logoText}> Clear English </div>
          </div>
        </Link>
        <div className={styles.navigation}>
          {user && this.renderDesktopHeaderHavigationWhenLogin()}
        </div>
      </div>
      <Link to={user ? "/dashboard/account/payment" : "/signup"} className={cn(styles.headerTrainsLockIcon, { [styles.headerTrainsLockIconHidden]: !user })}>
        <IconButton data-tip={"Разблокируй премиум доступ и получи безлимитные возможности"}>
          <LockIcon className="trainsLockIcon" />
        </IconButton>
      </Link>
      {this.renderDesktopHeaderUserSectionWhenLogin()}
      {this.renderDesktopHeaderUserSectionWhenNoLogin()}
    </AppBar>
  }
}

export default connect("_ALL")(Header)
