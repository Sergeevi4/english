import React from 'react'

import styles from './ErrorBoundary.module.scss'

class ErrorBoundary extends React.Component{
    state = {
        hasError: false
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    render(){
        if(this.state.hasError){
            return <div className={styles.errorBoundary}>
                <div className={styles.firstText}>
                    Упс... Что-то пошло не так :(
                </div>
                <div className={styles.secondText}>
                    Попробуйте перезагрузить страницу.
                </div>
            </div>
        }
        return this.props.children
    }
}

export default ErrorBoundary