import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Vocabulary } from './Vocabulary';
import { Card, IconButton, Button } from '@material-ui/core';
import _ from 'lodash';
import cn from 'classnames'
import style from './index.module.scss'
import { Link, RouteComponentProps } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';

import * as packsData from '../Training/trainingWord'

import PlusIcon from '@material-ui/icons/AddRounded';
import LockIcon from '@material-ui/icons/LockRounded';
import DeleteIcon from '@material-ui/icons/DeleteRounded';
import ArrowBackRounded from '@material-ui/icons/ArrowBackRounded';

ArrowBackRounded

import { connect } from '../../Store';
import { IPropsStore, Store } from '../../interface';
import { LinkButton } from '../UI/LinkButton';
import history from '../../history';
import { packsInfo } from '../../Infos';
import { API } from '../../API';
import { useStore } from '../../Utilities';
import { packsStuff } from '../../storeHelpers';

const togglePack = (store: Store, packId: string) => {
    const packs = _.get(store, 'userInfo.packs', {})

    const userWords = _.get(store, 'userInfo.userWords', {})
    const objectForUpdate = {}
    _.forEach(packsData[packId], word => {
        objectForUpdate[word] = {
            ..._.cloneDeep(userWords[word]),
            added: Boolean(!packs[packId])
        }
    })

    API.myUser().pack(packId).set(!packs[packId]);
    API.myUser().userWords().update(objectForUpdate)
}

export const Pack = (props:  RouteComponentProps<{id: string}>) => {
    const store = useStore();
    const {isOpen} = packsStuff(props.match.params.id)

    return <div className="page">
        <LinkButton to="/dashboard/vocabulary" color="inherit"> 
            <ArrowBackRounded fontSize="small" />
            Все наборы
        </LinkButton>
        <div className={style.packSubHeader}>
            <h1 className={style.packTitle}>Это набор слов <b>{props.match.params.id}</b></h1>
            <Button variant="contained" color={isOpen ? "secondary" : "primary" } onClick={() => togglePack(store, props.match.params.id)}>
                {isOpen ? "Удалить" : "Добавить"}
            </Button>
        </div>
        <ul className={style.packsWord}>
            {_.map(packsData[props.match.params.id], word => {
                return <li key={word}>{word}</li>
            })}
        </ul>
    </div>
}

export const Packs = connect("_ALL")(class extends React.Component<IPropsStore> {

    componentDidMount () {
        ReactTooltip.rebuild();
        ReactTooltip.hide();
    }
    componentDidUpdate () {
        ReactTooltip.rebuild();
        ReactTooltip.hide();
    }

    componentWillUnmount () {
        ReactTooltip.hide();
    }

    togglePack = (packId: string) => (e) => {
        e.preventDefault();
        togglePack(this.props.store, packId)
    }

    redirectToPayment = (e) => {
        e.preventDefault();
        history.push("/dashboard/account/payment")
    }

    render() {
        const {canOpenPack, packs} = packsStuff()

        const cards = _.map(packsInfo, (info, index) => {
            const isOpen = packs[info.url]
            return <Link to={`/dashboard/pack/${info.url}`} key={index} className={style.packWrapper}>
                <Card className={style.pack}>
                    {info.title}
                    {!isOpen && (canOpenPack
                        ? <IconButton onClick={this.togglePack(info.url)} className={style.packAdd} data-tip={"Добавить в словарь"}><PlusIcon /></IconButton>
                        : <IconButton onClick={this.redirectToPayment} className={style.packAdd} data-tip={"Добавлено максимальное количество наборов в словарь, нажмите чтобы разблокировать больше!"} ><LockIcon/></IconButton>)
                    }
                    {isOpen && <IconButton onClick={this.togglePack(info.url)} className={style.packRemove} data-tip={"Удалить"}><DeleteIcon /></IconButton>}
                </Card>
            </Link>
        })
        return <div className={cn(style.packBackground, 'global pl pr')}>
            {cards}
        </div>
    }
})

export const VocabularyTabs = () => {
    const [tabIndex, setTabIndex] = React.useState(0);
    const handleChange = (event, newValue) => setTabIndex(newValue)

    return <div className={style.tabsWrapper}>
        <Tabs value={tabIndex} onChange={handleChange} className={cn(style.tabs, 'global pl')}>
            <Tab label="Наборы слов" />
            <Tab label="Мой словарь" />
        </Tabs>
        {tabIndex === 0 && <Packs />}
        {tabIndex === 1 && <Vocabulary />}
    </div>
}