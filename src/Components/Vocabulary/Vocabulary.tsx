import MUIDataTable from "mui-datatables";
import React from 'react';
import { pasreWord } from "../../Utilities";
import _ from "lodash";
import cn from 'classnames'
import style from './Vocabulary.module.scss'
import { Link } from "react-router-dom";

import { ReactComponent as StashVoc } from '../icons/stashVoc.svg'
import { ReactComponent as VolumeVoc } from '../icons/volumeVoc.svg'
import { ReactComponent as HelpVoc } from '../icons/helpVoc.svg'
import { Card } from "@material-ui/core";
import { connect } from "../../Store";
import { API } from "../../API";

type IState = {
  wordsInfo: any
}

export const Vocabulary = connect("_ALL")(class extends React.Component {
  state = {
    wordsInfo: {}
  };

  wordsInfo = {}

  getWordIfNeeded = async (word: string) => {
    if (!this.wordsInfo[word]) {
      this.wordsInfo[word] = true
      const wordInfo = await API.word(word).once()
      this.setState((oldState: IState) => ({
        ...oldState,
        wordsInfo: {
          ...oldState.wordsInfo,
          [word]: wordInfo
        }
      }))
    }
  }

  render() {
    const columns = [
      "mobile",
      {
        name: "Слово",
        options: {
          filter: true,
          sort: false,
        },
      },
      "Транскрипция", "Перевод(ы)", ""];


    const userWords = _.keys(
      _.pickBy(
        _.get(this.props, 'store.userInfo.userWords', {}), 
        (word) => word.added
      )
    )
    const data = _.map(userWords, word => {
      // this.getWordIfNeeded(word)
      const wordInfo = this.state.wordsInfo[word] || {}
      return [
        word,
        wordInfo.transcription,
        pasreWord(wordInfo).showMeFirst,
        <Link key={word} to={'/word/' + word} target="_blank" className={style.vocDetail}>Подробнее</Link>,
      ]
    })

    const options = {
      // responsive: "scroll",
      selectableRows: 'none',
      sort: false, // Remove top section
      filter: false,
      search: false,
      print: false,
      download: false,
      viewColumns: false,
      rowsPerPageOptions: [10, 15],
      textLabels: {
        body: {
          noMatch: 'Здесь пусто :( Добавьте набор слов'
        },
        pagination: {
          rowsPerPage: 'Слов на странице'
        }
      },

      customRowRender: (...all) => {
        const word = all[0][0];
        this.getWordIfNeeded(word)
        const wordInfo = this.state.wordsInfo[word] || {}
        return <tr className={style.tr} key={word}>
          <td>
            <Card className={style.vocMobileWrapper}>
              <div className={style.vocMobileLeft}>
                <div className={style.volumeVocIconWrapper}>
                  <VolumeVoc className={style.volumeVocIcon} />
                </div>
                <div className={style.vocWordInfoWrapper}>
                  <div className={style.vocMobileWord}>
                    {word}
                  </div>
                  <div className={style.vocTranslateMobile}>
                    {pasreWord(wordInfo).showMeFirst}
                  </div>
                  <Link to={'/word/' + word} target="_blank" className={style.vocDetail}>Подробнее</Link>
                </div>
              </div>
              <div className={style.vocMobileRight}>
                <div className={style.vocMobileRightTop}>
                  <div className={style.vocMobileTranscription}>
                    {wordInfo.transcription}
                  </div>
                  <HelpVoc className={style.helpVocIcon} />
                </div>
                <div className={style.vocMobileRightBottom}>
                  <div>
                    90%
                  </div>
                  <StashVoc className={style.trashVoc} />
                </div>
              </div>
            </Card>
          </td>
          {_.map(_.initial(all[0]), (item, index) => <td key={index} className={style.td}>{item}</td>)}
        </tr>
      }
    };

    return (
      <div className={cn(style.vocWrapper, 'global pl pr')}>
        <MUIDataTable
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    );
  }
})