import React from 'react'
import { connect } from '../../Store';

import { ReactComponent as PremiumIcon } from '../icons/premiumIcon.svg'
import { ReactComponent as StandardIcon } from '../icons/standardIcon.svg'

import './PaymentIcons.scss'

class PaymentIcons extends React.Component<any, any>{

    renderIcon = () => {
        if(!this.props.store.userInfo) {return null}
        const paymentStatus = this.props.store.paymentStatus;
        if(paymentStatus === 'basic'){
            return
        }else if(paymentStatus === 'premium'){
            return <PremiumIcon className={this.props.type}/>
        }else if(paymentStatus === 'standard'){
            return <StandardIcon className={this.props.type}/>
        }   
    }

    render(){
        return(
            <div className="helpClass">{this.renderIcon()}</div>
        )
    }
}

export default connect('_ALL')(PaymentIcons)