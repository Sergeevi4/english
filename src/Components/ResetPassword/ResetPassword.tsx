import React, {useState, useCallback} from 'react';
import { auth } from 'firebase/app';
import Alert from 'react-s-alert';
import { LinkButton } from '../UI/LinkButton';

import { Button, TextField } from '@material-ui/core';

import 'react-s-alert/dist/s-alert-default.css'
import 'react-s-alert/dist/s-alert-css-effects/scale.css'
import styles from "./ResetPassword.module.scss"
import Helmet from 'react-helmet';

export const ResetPassword = () => {

    const [value, setValue] = useState('')

    const onChange = useCallback((e: any) => {
        setValue(e.target.value)
    }, [])

    const requestForReset = () => {
        auth().sendPasswordResetEmail(value).then(function () {
            Alert.success('Check your E-mail', {
                position: 'bottom',
                effect: 'scale',
                timeout: 'none'
            })
        }).catch(function (error) {
            Alert.error(error, {
                position: 'bottom',
                effect: 'scale',
                timeout: 'none'
            }) 
        });
    }

    return (
        <div className={styles.wrapper}>
            <div className={styles.resetpassword}>
                <h3>
                    Ссылка на восстановление пароля будет отправлена вам на почту :
                </h3>
                <TextField 
                    InputProps={{ classes: { underline: styles.underline } }}
                    className={styles.input}
                    placeholder = 'Email'
                    onChange = {onChange}
                    value={value}
                />
                <Button
                    color="primary"
                    variant="contained"
                    className={styles.button} 
                    onClick={requestForReset}
                    size="small"
                >
                    Восстановить пароль
                </Button>
                <div className={styles.row}>
                    <h4>Вспомнили пароль?</h4>   
                    <LinkButton to={"/login"}
                        className={styles.buttonLogin}
                        classes={{label: styles.buttonText}}
                        variant="contained"
                        size="small"
                    >
                        Войти
                    </LinkButton>
                </div>
            </div>
            <Helmet>
                <link rel="canonical" href="https://clearenglish.io/reset-password" />
            </Helmet>
        </div>
    );
}
