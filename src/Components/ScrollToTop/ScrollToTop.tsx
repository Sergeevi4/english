import React, { useCallback } from 'react'

import './ScrollToTop.scss'

import { ReactComponent as Arrow } from '../icons/right-arrow.svg'

export const ScrollToTop = () => {

    const Scroll = useCallback(() => {
        const hiddenElement = document.querySelector('header')
        hiddenElement.scrollIntoView({block: "center", behavior: "smooth"});
    }, []) 
    return(
        <div className="scroll" onClick={Scroll}>
            <Arrow className="scrollArrow"/>
        </div>
    )
}