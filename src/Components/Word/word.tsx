import * as React from 'react';
import * as _ from 'lodash';
import firebase from '../../Firebase';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Alert from 'react-s-alert'

import 'react-s-alert/dist/s-alert-default.css'
import 'react-s-alert/dist/s-alert-css-effects/scale.css'

import HelpIcon from '@material-ui/icons/Help';
import SpeakIcon from '@material-ui/icons/VolumeUpRounded';
import CloseIcon from '@material-ui/icons/CloseRounded';
import { ReactComponent as CopyIcon } from '../icons/copy-content.svg'

import styles from './word.module.scss';
import { useState, useEffect } from 'react';
import Helmet from 'react-helmet';
import { speakText, mapReplace } from '../../Utilities';
import { Fab, DialogTitle } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import { API } from '../../API';

const nlp = require('compromise');

const sounds = {
    'ʌ': {
        sound: 'Open-mid_back_unrounded_vowel.mp3',
        info: 'в русском нет эквивалентного звука, ближайший аналог А, но будьте осторожны, звуки æ, a также звучат похоже на русский а'
    },
    'æ': {
        sound: 'Near-open_front_unrounded_vowel.mp3',
        info: 'в русском нет эквивалентного звука, ближайший аналог А или Э, но будьте осторожны, звуки æ, ʌ также звучат похоже на русский а'
    },
    'a': {
        sound: 'Open_front_unrounded_vowel.mp3',
        info: 'в русском нет эквивалентного звука, ближайший аналог А, но будьте осторожны, звуки æ, ʌ также звучат похоже на русский а'
    },
    'ɛ': {
        sound: 'Open-mid_front_unrounded_vowel.mp3',
        info: 'в русском нет эквивалентного звука, ближайший аналог э, но будьте осторожны, звуки æ, e, ɘ также звучат похоже на русский э'
    },
    'ɜ': {
        sound: 'Open-mid_central_unrounded_vowel.mp3',
        info: 'в русском нет эквивалентного звука, ближайший аналог э, но будьте осторожны, звуки æ, e, ɘ также звучат похоже на русский э'
    },



    // 'ai'
    'aʊ': {
        sound: ''
    },
    // 'aiə, aʊə ]'
    'i': {
        sound: ''
    },
    // 'i:': {
    //     sound: ''
    // },
    'ɪ': {
        info: 'приближенно к русскому Ы',
        sound: 'Near-close_near-front_unrounded_vowel.mp3'
    },
    'e': {
        sound: ''
    },
    'ei': {
        sound: ''
    },


    'ɘ': {
        sound: 'Close-mid_central_unrounded_vowel.mp3',
        info: 'в русском нет эквивалентного звука, ближайший аналог э, но будьте осторожны, звуки æ, e, ɛ также звучат похоже на русский э'
    },



    'g': {
        sound: 'g.mp3',
        info: 'почти полный аналог русского звука г, и украинского ґ'
    },

    't': {
        sound: 't.mp3',
        info: 'приближенно к русскому Т, но есть существенные отличия в положении языка'
    },
    'b': {
        sound: 'b.mp3',
        info: 'аналог русскому Б'
    },
    'j': {
        sound: 'j.mp3',
        info: 'приближен к русскому Й'
    },
    'n': {
        sound: 'n.mp3',
        info: 'приближен к русскому Н, звук '
    },
    'd': {
        sound: 'd.mp3',
        info: 'приближенно к русскому Д, но есть существенные отличия в положении языка '
    },
    'ˈ': {
        sound: null,
        info: 'ударение, ставится ПЕРЕД ударным слогом'
    },

    'f': {
        sound: 'f.mp3',
        info: 'приближенно к русскому Ф'
    },
    ':': {
        sound: null,
        info: 'удлинение предыдущего звука'
    },
    'w': {
        sound: 'w.mp3',
        info: 'в русском нет эквивалентного звука, что-то среднее между В и У'
    },
    'l': {
        sound: 'l.mp3',
        info: 'приближенной к русскому Л'
    },

    'p': {
        sound: 'p.mp3', // todo
        info: 'приближенно к русскому П'
    },

    'ʒ': {
        sound: 'ʒ.mp3', // todo
        info: 'приближенно к русскому Ж'
    }
}


// class InteractiveTranscription2 extends React.Component<{ transcription: string, word: string }> {
//     state = {
//         selected: null,
//     }

//     play = () => {
//         const utterance = new SpeechSynthesisUtterance(this.props.word);
//         window.speechSynthesis.speak(utterance);
//     }

//     renderSelected = () => {
//         if (!this.state.selected) {
//             return null;
//         }
//         return <div>
//             <h3>{this.state.selected}</h3>
//             {sounds[this.state.selected].info}
//         </div>
//     }

//     render() {
//         const letters = this.props.transcription.split('').map(letter => {
//             return letter
//             // return <b onClick={() => {
//             //     this.setState({selected: letter})
//             //     new Audio(`/audio/${sounds[letter].sound}`).play()
//             // }} style={{ border: 'solid 1px black', margin: '2px' }}>{letter}</b>;
//         })
//         return <div className={styles.wrapper}>
//             <div className={styles.center} >
//                 <SpeakIcon onClick={this.play} />
//                 <div className={styles.trans}>
//                     {letters}
//                 </div>
//                 <HelpIcon />
//             </div>
//             {this.renderSelected()}

//         </div>
//     }
// }

const InteractiveTranscription = (props: { transcription: string, word: string }) => {
    const [isOpen, open] = useState(false);
    const [isErrorHappens, setErrorState] = useState(false)
    const play = () => {

        speakText({ text: props.word, onError: () => setErrorState(true) })
    }
    const letters = props.transcription.split('').map(letter => {
        return letter
    })
    const renderFull = () => {
        if (!isOpen) return null;
        const infos = mapReplace(props.transcription, { 'ː': ':', ' ': '' }).split('').filter(letter => {
            return ![']', '['].includes(letter);
        }).map((letter, index) => {
            return <li key={index}>
                <h3>
                    {letter}
                    {_.get(sounds, `[${letter}].sound`) && <SpeakIcon onClick={() => {
                        new Audio(`/audio/${sounds[letter].sound}`).play()
                    }} />}
                </h3>
                {_.get(sounds, `[${letter}].info`)}
            </li>
        })
        return <ul className={styles.symbols}>{infos}</ul>
    }
    return <div className={styles.wrapper}>
        <Dialog open={isErrorHappens} classes={{ root: styles.errorDialog, paper: styles.errorDialogWrapper }}>
            <div className={styles.closeDialog} onClick={() => setErrorState(false)}>&#10006;</div>
            <DialogTitle id="simple-dialog-title" className={styles.title}>Упс</DialogTitle>
            <div className={styles.items}>Данный браузер не поддерживает произношение</div>
            <div className={styles.items + ' ' + styles.secondText}>Скопируйте ссылку и откройте в Google Chrome</div>
            <div className={styles.urlWrapper}>
                <div className={styles.url}>
                    <div className={styles.urlBlock}>{window.location.href}</div>
                    <CopyToClipboard text={window.location.href}
                        onCopy={() => {
                            Alert.success('Скопировано', {
                                position: 'bottom',
                                effect: 'scale',
                                timeout: 3000
                            })
                            setErrorState(false)
                        }}>
                        <CopyIcon className={styles.copyIcon} />
                    </CopyToClipboard>
                </div>
            </div>
        </Dialog>
        <div className={styles.center} >
            <Fab className={styles.iconWrapper} size="small" onClick={play}><SpeakIcon /></Fab>
            <div className={styles.trans}>
                {letters}
            </div>
        </div>
        <div className={styles.innerWrapper + " " + (isOpen ? styles.open : '')} onClick={!isOpen ? (() => open(!isOpen)) : undefined}>
            {!isOpen && <Button size="large" className={styles.button} variant="contained" color="primary" >Как читать транскрипцию</Button>}
            {isOpen && <div className={styles.close} onClick={() => open(false)}>Свернуть <CloseIcon /></div>}
            {renderFull()}
        </div>
    </div>
}

const normalazeWord = (word) => {
    let a = word.replace(/[^a-zA-Z ]/g, "")
    let b = a.toLowerCase()
    return b;
}

export const WordInfoDummy = ({ word, withAuth, addToDictionaryFrom }: { word: string, withAuth?: boolean, addToDictionaryFrom?: any }) => {



    const [wordInfo, setWordInfo] = useState(undefined)

    const loadWord = async () => {

        let normalezedWord = normalazeWord(word.trim());
        let wordInfo = await API.word(normalezedWord).once()
        if (wordInfo) {
            setWordInfo(wordInfo)
            return;
        } 

        let maybeWord = nlp(normalezedWord).normalize({ plurals: true, verbs: true }).out('text');
        wordInfo = await API.word(maybeWord).once()
        if (wordInfo) {
            setWordInfo(wordInfo)
            return;
        }
        const verbs = nlp(normalezedWord).verbs().conjugate();
        if (verbs.length == 1) {
            maybeWord = verbs[0].Infinitive
        }
        wordInfo = await API.word(maybeWord).once()
        setWordInfo(wordInfo)
    }

    useEffect(() => {
        loadWord();
    }, [])




    if (wordInfo === undefined) {
        return <h2 style={{ padding: '20px' }}>Загружаем</h2>;
    }

    if (!wordInfo) {
        return <h2 style={{ padding: '20px' }}>Нет информации</h2>;
    }


    const addWord = async () => {
        // In case we haven't translate for plural or V2,V3 we will try to show for V1 or singular
        const changedWord = wordInfo.word || word; 

        await API.myUser().userWords().update({
            [changedWord]: {
                added: true,
                addedAt: firebase.database.ServerValue.TIMESTAMP as number, // TODO, in case we add this word second time, we overwrite addedAt time, is it okay ? 
                addedFrom: addToDictionaryFrom
            }
        })

        Alert.info(`Слово <b>${changedWord}</b> было добавленно в ваш словарь`, {
            position: 'bottom',
            effect: 'scale',
        });
    }

    const speeches = _.filter(_.map(wordInfo.speeches, (speech, key) => {
        if (_.every(speech, translate => !translate.ru)) return undefined;
        return <div key={key}>
            <h2 className={styles["part-of-speech"]}>{key}</h2>
            <ol>
                {_.map(_.filter(speech, translate => translate.ru), (translate: any, index) => {
                    return <li key={index}>{translate.ru}</li>
                })}
            </ol>
            {/* <code>
                {JSON.stringify(speech)}
            </code> */}
        </div>
    }))


    const examples = _.filter(_.map(wordInfo.speeches, (speech, key) => {
        if (_.every(speech, it => !it.examples)) return undefined;
        return <div key={key}>
            <h2 className={styles["part-of-speech"]}>{key}</h2>
            <ul className={styles.exampleList}>
                {_.map(_.filter(speech, 'examples'), (translate, index) => {
                    return <li key={index} className={styles.example}>
                        <div>
                            <span className={styles.lang}>en</span>
                            <span>{translate.examples[0].eng}</span>
                        </div>
                        <div>
                            <span className={styles.lang}>ru</span>
                            <span>{translate.examples[0].ru}</span>
                        </div>
                    </li>
                })}
            </ul>
            {/* <code>
                {JSON.stringify(speech)}
            </code> */}
        </div>
    }))

    const AuthLink = props => <Link to="/signup" {...props} />
    return <div style={{ padding: '10px 10px 20px 10px' }}>
        <h1 className={styles.title}>  {wordInfo.word}</h1>
        <InteractiveTranscription word={wordInfo.word} transcription={wordInfo.transcription} />
        <div className={styles.papers}>
            <Paper className={styles.paper}>
                <span className={styles.text} >Перевод слова <span className={styles.word}>{wordInfo.word}</span></span>
                {speeches}
            </Paper>
            {withAuth && <Button component={AuthLink} size="large" className={styles.button} variant="contained" color="primary" >Зарегистрироваться</Button>}
            <Paper className={styles.paper}>
                <span className={styles.text} >Примеры употребления слова <span className={styles.word}>{wordInfo.word}</span></span>
                {examples}
            </Paper>
        </div>
        {withAuth && <Button component={AuthLink} size="large" className={styles.button} variant="contained" color="primary" >Зарегистрироваться</Button>}
        {addToDictionaryFrom && <Button onClick={addWord} size="large" className={styles.button} variant="contained" color="primary" >Добавить в мой словарь</Button>}
    </div>
}

export class WordInfo extends React.Component<any> {
    state = { wordInfo: null }
    async componentDidMount() {
        const word = await firebase.database().ref("words/" + this.props.match.params.id).once('value')
        this.setState({
            wordInfo: word.val()
        })
    }
    render() {
        if (!this.state.wordInfo) {
            return null;
        }

        const speeches = _.filter(_.map(this.state.wordInfo.speeches, (speech, key) => {
            if (_.every(speech, translate => !translate.ru)) return undefined;
            return <div key={key}>
                <h2 className={styles["part-of-speech"]}>{key}</h2>
                <ol>
                    {_.map(_.filter(speech, translate => translate.ru), (translate: any, index) => {
                        return <li key={index}>{translate.ru}</li>
                    })}
                </ol>
                {/* <code>
                    {JSON.stringify(speech)}
                </code> */}
            </div>
        }))


        const examples = _.filter(_.map(this.state.wordInfo.speeches, (speech, key) => {
            if (_.every(speech, it => !it.examples)) return undefined;
            return <div key={key}>
                <h2 className={styles["part-of-speech"]}>{key}</h2>
                <ul className={styles.exampleList}>
                    {_.map(_.filter(speech, 'examples'), (translate, index) => {
                        return <li key={index} className={styles.example}>
                            <div>
                                <span className={styles.lang}>en</span>
                                <span>{translate.examples[0].eng}</span>
                            </div>
                            <div>
                                <span className={styles.lang}>ru</span>
                                <span>{translate.examples[0].ru}</span>
                            </div>
                        </li>
                    })}
                </ul>
                {/* <code>
                    {JSON.stringify(speech)}
                </code> */}
            </div>
        }))

        const AuthLink = props => <Link to="/signup" {...props} />
        return <div style={{ padding: '10px 10px 20px 10px' }}>
            <Helmet defer={false}>
                <title>{"Значение слова " + this.props.match.params.id + ' | Clearenglish'}</title>
                <link rel="canonical" href={"http://clearenglish.io/word/" + this.props.match.params.id} />
            </Helmet>
            <h1 className={styles.title}>  {this.state.wordInfo.word}</h1>
            <InteractiveTranscription word={this.state.wordInfo.word} transcription={this.state.wordInfo.transcription} />
            <div className={styles.papers}>
                <Paper className={styles.paper}>
                    <span className={styles.text} >Перевод слова <span className={styles.word}>{this.state.wordInfo.word}</span></span>
                    {speeches}
                </Paper>
                <Button component={AuthLink} size="large" className={styles.button} variant="contained" color="primary" >Зарегистрироваться</Button>
                <Paper className={styles.paper}>
                    <span className={styles.text} >Примеры употребления слова <span className={styles.word}>{this.state.wordInfo.word}</span></span>
                    {examples}
                </Paper>
            </div>
            <Button component={AuthLink} size="large" className={styles.button} variant="contained" color="primary" >Зарегистрироваться</Button>
        </div>
    }
}


