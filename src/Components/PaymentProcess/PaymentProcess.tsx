import React from 'react';
import _ from 'lodash';
import { LinkButton } from '../UI/LinkButton';
import styles from './PaymentProcess.module.scss'

export const PaymentProcess =() => {
    return(
        <div className={styles.wrapper}>
            <h1>Спасибо за покупку</h1>
            <h2>Оплата пройдет в течении суток </h2>
            <LinkButton to='/dashboard'>Вернуться домой</LinkButton>
        </div>
    )
}