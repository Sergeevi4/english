import * as React from 'react';
import * as _ from 'lodash';
import firebase from '../../Firebase';
import { connect, set as storeSet } from "../../Store";
import { Store } from "../../interface";

import { speakText, ObjectOf } from '../../Utilities'

import Button from '@material-ui/core/Button';
import { Theme, Fab, Input, Checkbox, Paper } from '@material-ui/core';
import QrReader from 'react-qr-reader'

import 'rc-slider/assets/index.css';
import style from './Watch.module.scss'
import Spinner from '../Spinner/Spinner';
import cn from 'classnames';
import Slider from 'rc-slider';

import { parse } from 'subtitle'
import ComingSoon from '../ComingSoon/ComingSoon';

interface IProps {
  store: Store;
}

const defaultSharedState = {
  // global setting 
  showEnSubtitle: false,
  showRuSubtitle: false,
  playbackRate: 1,
  volume: 0.5, // between 0 and 1
  isFullScreen: false,

  // current video setting
  startPlayTime: 0, // second
  startDate: Date.now(),
  isPlay: false,
  duration: 42 * 60, // wrong, just for test
}

const sharedStateType: ObjectOf<'checkbox' | 'slider' | 'none'> = {
  showEnSubtitle: 'checkbox',
  showRuSubtitle: 'checkbox',
  playbackRate: 'slider',
  volume: 'slider',
  isFullScreen: 'checkbox',
  startPlayTime: 'slider',
  startDate: 'none',
  isPlay: 'checkbox',
  duration: 'none',
}

const Watch = () => <ComingSoon type="watch"/>

class WatchDev extends React.Component<IProps> {
  state = {
    key: '',
    videoInfo: null,
    sharedState: defaultSharedState,
    videoTime: 0,
    result: 'No result'
  }


  // componentDidMount() {
  //   setInterval(() => {
  //     this.forceUpdate();
  //   }, 2000)
  // }

  // componentWillUnmount () {
  //   clearInterval(this.timer)
  // }

  
  videoTimeInterval = null

  updateInfo = () => {
    firebase.database().ref('watch/' + this.state.key).once('value', (res) => {
      this.setState({videoInfo: res.val()})
    })
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value }, this.updateInfo);
  };

  updateVideo = () => {
    firebase.database().ref('watch/' + this.state.key + '/control').update({
      ...this.state.sharedState,
      // isPlay: this.state.isPlay,
      lastWhoChange: this.props.store.user.uid
    })

    // clearInterval(this.videoTimeInterval);
    // if (this.state.isPlay) {
    //   this.videoTimeInterval = setInterval(() => {
    //     this.setState({videoTime: this.state.videoTime + 1000})
    //   }, 1000) // <-- shit code :(
    // }
  }

  onSharedControlChange = (name: string) => (e) => {
    const value = e.currentTarget 
      ? e.currentTarget.checked || e.currentTarget.value 
      : e

    const additionInfo = name === 'startPlayTime' 
      ? {startDate: Date.now()}
      : {}

    this.setState({
      sharedState: {
        ...this.state.sharedState,
        ...additionInfo,
        [name]: value
      }
    }, this.updateVideo)
  }

  // onCheckboxChange = (name: string) => (e) => {
  //   this.setState({ [name]: e.currentTarget.checked }, this.updateVideo);
  // }


  getCurrentSubtitle = () => {
    if (!_.get(this.state, 'videoInfo.subtitle.ru')) return 'sub not loaded'
    const sub = parse(this.state.videoInfo.subtitle.ru).find(item => item.start <= this.state.videoTime && item.end > this.state.videoTime )
    return sub ? sub.text : 'nothing'
  }

  renderControls = () => {
    const controls = _.map(sharedStateType, (type, name) => {
      let Control = null;
      if (type === 'checkbox') {
        Control = <Checkbox checked={this.state.sharedState[name]} onChange={this.onSharedControlChange(name)} />
      }

      if (type === 'slider') {
        if (name === 'startPlayTime') {
          Control = <Slider value={this.state.sharedState[name]} step={0.00001} max={this.state.sharedState.duration} onChange={this.onSharedControlChange(name)} />
        } else {
          Control = <Slider value={this.state.sharedState[name]} step={0.00001} max={1} onChange={this.onSharedControlChange(name)} />
        }
        
      }

      if (type === 'none') {
        Control = '[ReadOnly]: ' + this.state.sharedState[name]
      }


      return <div>
          {name}:
          {Control}
        </div>

    })
    return <Paper className={style.controls}>
      <h1>Control video</h1>
      {controls}
    </Paper>
  }
  handleScan = data => {
    if (data) {
      this.setState({
        result: data
      })
    }
  }
  handleError = err => {
    console.error(err)
  }

  render() {
    return <div className={cn(style.page, "page")}>
      <h1>Введите ваш код ниже</h1>

      <QrReader
          delay={300}
          onError={this.handleError}
          onScan={this.handleScan}
          className={style.qrCode}
      />
      <Input value={this.state.key} onChange={this.handleChange('key')} />
      
      {this.renderControls()}
      <h1>subtitle</h1>
      <h2>{this.getCurrentSubtitle()}</h2>
      <div>
        {JSON.stringify(this.state.videoInfo)}
      </div>
    </div>
  }
}

export const watchDev = connect("_ALL")(WatchDev);
export default connect("_ALL")(Watch);
