import React from 'react'
import firebase from '../../Firebase';
import { connect } from '../../Store';
import { Store } from '../../interface';

import { Paper } from '@material-ui/core';

import styles from './ComingSoon.module.scss';
import { API } from '../../API';

class ComingSoon extends React.Component<{store: Store, type: 'book' | 'voc' | 'watch'}> {
    state = {
        comingSoons: {},
    }

    componentDidMount() {
        API.comingSoons().on(comingSoons => {
            this.setState({comingSoons})
        })
    }

    saveEmailInDb = () => {
        const { email, uid, displayName } = this.props.store.user
        const data = { email, displayName }

        API.comingSoon(this.props.type, uid).update(data)
    }

    renderText = {
        book: () => {
            return <div className={styles.text}>
                Но пока еще не сделали финальную версию.
                <br />
                Тут вы сможете читать книги на английском языке, кликая на незнакомые слова смотреть их перевод, добавлять их себе в словарь и учить.
            </div>
        },
        watch: () => {
            return <div className={styles.text}>
                Но пока еще не сделали финальную версию.
                <br />
                Тут вы сможете скачать программу для просмотра любых фильмов на вашем компьютере на английском языке, а в субтитрах вы сможете кликая на незнакомые слова смотреть их перевод, добавлять их себе в словарь и учить.
            </div>
        },
        voc: () => {
            return <div className={styles.text}>
                Но пока еще не сделали финальную версию.
                <br />
                Тут вы сможете просматривать свой словарь, видеть прогресс изучения каждого слова, добавлять себе в словарь новые слова и наборы слов, например "спорт", "путешествия".
            </div>
        }
    }

    render() {
        const type = this.props.type
        const isAlready = this.state.comingSoons[type] && this.state.comingSoons[type][this.props.store.user.uid]
        return (
            <div className={styles.wrapper}>
                <Paper className={styles.comingSoon} elevation={10}>
                    <div className={styles.title}>
                        Мы усердно работаем
                    </div>
                    {this.renderText[type]()}
                    <div 
                        className={styles.button + ' ' + (isAlready ? styles.disable : '')} 
                        onClick={this.saveEmailInDb}
                    >
                        <span className={styles.buttonText}>
                            {isAlready ? 'Вы уже подписались' : 'Получить уведомление о запуске'}
                        </span>
                    </div>
                </Paper>
            </div>
        )
    }
}

export default connect("user")(ComingSoon)