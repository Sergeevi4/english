import React from 'react'
import _ from 'lodash'

import './UserDailyRecomendation.scss'
import { Card, Stepper, Step, StepLabel } from '@material-ui/core';
import { dailyTasks } from '../../Infos';

export default class UserDailyRecomendation extends React.Component {

    state = {
        activeStep: 0,
    }

    renderDailyRecomendations = () => {
        const result = _.map(dailyTasks, (value, index) => {
            return <div className='UserProgressDay' key={index}>
                <div className='progressDay'>{index + 1}</div>
                {_.map(value, (trainProgress, key) => {
                    return <div className='progressLineWrapper' key={key}>
                        <div className='progressLabel'>
                            {key}
                        </div>
                        <div className='progressValue'>
                            3/{trainProgress} раз
                        </div>
                    </div>
                })}
            </div>
        })

        return <Card className="userDailyRecomendation">
            <div className="recomendationText">Выполняй задания 5 дней подряд и получи премиум аккаунт на 5 дней!</div>
            <Stepper activeStep={this.state.activeStep} alternativeLabel className="Stepper">
                {_.map(result, (text) => (
                    <Step key={9}>
                        <StepLabel StepIconProps={{ classes: { root: 'stepIcon' } }}>{text}</StepLabel>
                    </Step>
                ))}
            </Stepper>
        </Card>
    }

    render() {
        return this.renderDailyRecomendations()
    }
}