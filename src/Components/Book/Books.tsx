import React from 'react';
import firebase from '../../Firebase';
import { connect } from "../../Store";
import BookPreview from './BookPreview';
import _ from 'lodash';
import Spinner from '../Spinner/Spinner';
import { useStore } from '../../Utilities';
import { API } from '../../API';
import cn from 'classnames'

import styles from './Books.module.scss'

interface IState {
    uploadPopupIsShow: boolean,
    textName: string,
    text: string,
    allUserBooks: any,
    richText: string,
    audioFile: File[],
    bookFile: File | null
}

export const Books = () => {
    const store = useStore();
    const myBooks = _.get(store, 'userInfo.booksPreview')
    const libraryBooks = API.booksPreview().useOnce();

    // if (!myBooks) return <div>Loading</div>
    return <div>
        <h1 className={cn(styles.booksTitle, "global pl")}>Мои книги</h1>
        <div className="books-wrapper">
            {_.map(myBooks, book => {
                return <BookPreview
                    {...book}
                    key={book.bookId}
                />
            })}
        </div>
        <h1 className={cn(styles.booksTitle, "global pl")}>Библиотека</h1>
        <div className="books-wrapper">
            {_.map(_.filter(libraryBooks, book => !book.hide), book => {
                return <BookPreview
                    {...book}
                    key={book.bookId}
                />
            })}
        </div>
    </div>
}

class BooksOld extends React.Component<any, IState> {

    state = {
        uploadPopupIsShow: false,
        textName: '',
        text: '',
        allUserBooks: null,
        richText: '',
        audioFile: [],
        bookFile: null
    }

    async componentDidMount() {
        const books = await firebase
            .database()
            .ref("users/" + this.props.store.user.uid + "/books")
            .once('value')
        this.setState({
            allUserBooks: books.val() || {}
        })
    }

    renderBlankPage = () => {
        return <div className="books__blankPage">
            <span className="blankPage__text">
                У вас нет избранных книг :(
            </span>
        </div>
    }

    renderBooks = () => {
        return _.map(this.state.allUserBooks, ({ textName }, key) => <BookPreview
            id={key}
            key={key}
            textName={textName}
            author={textName}
        />)
    }

    render() {
        if (!this.state.allUserBooks) return <Spinner />
        return (
            <>
                { _.isEmpty(this.state.allUserBooks) ? this.renderBlankPage() : <div className="books-wrapper">
                    {this.renderBooks()}
                </div>}
            </>
        );
    }
}

export default connect("user")(Books);
