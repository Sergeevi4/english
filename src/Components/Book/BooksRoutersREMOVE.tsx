import * as React from 'react';
import firebase from '../../Firebase';
import * as _ from 'lodash';
import axios from 'axios';
import BookPreview from './BookPreview';
import { connect } from "../../Store";
import { Store } from '../../interface'
import { FileButton } from '../UI/FileButton';

import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Theme, WithStyles } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import './BooksRouters.scss'
import { Link, Switch, Route, NavLink } from 'react-router-dom';
import { Books } from './Books';
import ComingSoon from '../ComingSoon/ComingSoon';

function getModalStyle() {
  const top = 50
  const left = 50

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = (theme: Theme) => ({
  paper: {
    position: "absolute" as "absolute",
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
  input: {
    margin: theme.spacing.unit,
  },
  inputUpload: {
    display: 'none',
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  uploadButtons: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  uploadButtonsOnPage: {
    position: 'fixed' as 'fixed',
    top: 0,
    left: 0,
    zIndex: 9999
  }
});

interface IProps extends WithStyles<typeof styles> {
  store: Store
}

const removeStyleFromHTML = (html: string) => {
  const div = document.createElement('div');
  div.innerHTML = html;
  _.forEach(div.querySelectorAll('*'), el => el.removeAttribute('style'))
  return div.innerHTML
}

interface IState {
  uploadPopupIsShow: boolean,
  textName: string,
  text: string,
  allUserBooks: any,
  richText: string,
  audioFile: File[],
  bookFile: File | null
}

class BooksRouters extends React.Component<IProps, IState> {

  state = {} as IState;
  uploadText = () => {
    const currentUser = firebase.auth().currentUser;
    if (currentUser) {
      currentUser.getIdToken(true).then((idToken) => {
        const formData = new FormData();
        _.forEach(this.state.audioFile, file => {
          formData.append('audio', file)
        })
        formData.append('book', this.state.bookFile)
        formData.append('textName', this.state.textName)
        formData.append('text', this.state.text)
        formData.append('richText', this.state.richText)
        formData.append('idToken', idToken)
        axios.post('http://localhost:5000/checkToken', formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
        this.setState({
          uploadPopupIsShow: false,
          textName: '',
          text: '',
        })
      })
    }
  }

  componentDidMount() {
    firebase
      .database()
      .ref("users/" + this.props.store.user.uid + "/books")
      .on('value', (snapshot: firebase.database.DataSnapshot) => {
        this.setState({
          allUserBooks: snapshot.val()
        })
      })
  }

  handleOpen = () => {
    this.setState({ uploadPopupIsShow: true });
  };

  handleClose = () => {
    this.setState({ uploadPopupIsShow: false });
  };

  onTextNameChange = (e: any) => {
    this.setState({
      textName: e.target.value
    })
  }

  onTextChange = (e: any) => {
    this.setState({
      text: e.target.value
    })
  }

  onAudioFileChange = (e: any) => {
    this.setState({
      audioFile: e.target.files
    })
  }

  onBookFileChange = (e: any) => {
    this.setState({
      bookFile: e.target.files[0]
    })
  }

  renderModal = () => {
    const { classes } = this.props;
    return <Modal
      open={this.state.uploadPopupIsShow}
      onClose={this.handleClose}
    >
      <div style={getModalStyle()} className={classes.paper}>
        <Input
          placeholder="Введите имя файла"
          className={classes.input}
          onChange={this.onTextNameChange}
          value={this.state.textName}
        />
        <br />
        <div className={classes.uploadButtons}>
          <FileButton onChange={this.onAudioFileChange} >
            <Button variant="contained" component="span">
              <CloudUploadIcon className={classes.rightIcon} />
              audio file
            </Button>
          </FileButton>
          <FileButton onChange={this.onBookFileChange} >
            <Button variant="contained" component="span">
              <CloudUploadIcon className={classes.rightIcon} />
              books file
            </Button>
          </FileButton>
        </div>
        <TextField
          id="outlined-multiline-flexible"
          label="Multiline"
          multiline={true}
          rowsMax="4"
          value={this.state.text}
          onChange={this.onTextChange}
          margin="normal"
          variant="outlined"
        />
        <div className="upload-buttons">
          <Button
            className="reg-button"
            variant="contained"
            color="primary"
            onClick={this.uploadText}
          >
            Create
          </Button>
          <Button
            className="reg-button"
            variant="contained"
            color="secondary"
            onClick={this.handleClose}
          >
            Cancel
        </Button>
        </div>
      </div>
    </Modal>
  }

  render() {
    const { classes } = this.props;
    const dev = process.env.NODE_ENV === 'development'
    return (
      <div className={dev ? "Books" : "Books-dev"}>
        {this.renderModal()}
        {dev ? [<Button
          className={classes.uploadButtonsOnPage}
          variant="contained"
          onClick={this.handleOpen}
        >
          Upload
        </Button>,
        // <div className="Books__header">
        //   {/* <Switch>
        //     <Route path="/dashboard/books" exact={true} render={() => <div className="Books__pageName">Избранное</div>} />
        //     <Route path="/dashboard/books/library" render={() => <div className="Books__pageName">Библиотека</div>} />
        //     <Route path="/dashboard/books/add" render={() => <div className="Books__pageName">Добавить книгу</div>} />
        //   </Switch> */}
        //   <div className="Books__navBar navBar">
        //     <NavLink to="/dashboard/books" exact className="navBar__item" activeClassName="active">
        //       Избранное
        //     </NavLink>
        //     <NavLink to="/dashboard/books/library" className="navBar__item" activeClassName="active">
        //       Библиотека
        //     </NavLink>
        //     <NavLink to="/dashboard/books/add" className="navBar__item" activeClassName="active">
        //       Добавить книгу
        //     </NavLink>
        //   </div>
        // </div>,
        <div className="Books__mainContent">
          <Switch>
            <Route path='/dashboard/books' exact={true} render={() => <Books />}/>
            {/* <Route path='/dashboard/books/library' render={() => <Books filter={"library"}/>}/>
            <Route path='/dashboard/books/add' render={() => <h1>231312</h1>}/>}/> */}
          </Switch>
        </div>]
        : 
        <ComingSoon type="book"/>  
      }
      </div>
    );
  }
}

export default connect("user")(withStyles(styles)(BooksRouters));
