import * as React from 'react';
import firebase from '../../Firebase';
import * as _ from 'lodash';
import Slider from 'rc-slider';
import Alert from 'react-s-alert';
import ControlsPopup from '../UI/ControlsPopup';
import { connect } from "../../Store";
// import words from '../../word.json'
import { sortByKey } from '../../Utilities';
import cn from 'classnames'

const words = {}; // TODO

const ReactAudioPlayer = require('react-audio-player').default
const spline = require('cubic-spline');
const nlp = require('compromise');

import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import { withStyles } from '@material-ui/core/styles';

import { ReactComponent as PlayerSpeed } from '../icons/player-speed.svg'
import { ReactComponent as PlayerSetting } from '../icons/player-setting.svg'
import { ReactComponent as DarkMode } from '../icons/footerSeting-item1.svg'
import { ReactComponent as WhiteMode } from '../icons/footerSeting-item2.svg'
import { ReactComponent as YellowMode } from '../icons/footerSeting-item3.svg'
import { ReactComponent as LoupeMinus } from '../icons/loupeMinus.svg'
import { ReactComponent as LoupePlus } from '../icons/loupePlus.svg'
import { ReactComponent as PaginationArrow } from '../icons/paginationArrow.svg'
import { ReactComponent as PlayIcon } from '../icons/playIcon.svg'
import { ReactComponent as ComplexityIcon } from '../icons/complexity-icon.svg'

import style from "./Book.module.scss"
import 'rc-slider/assets/index.css';
import Helmet from 'react-helmet';
import { API } from '../../API';

import { StylesProvider } from '@material-ui/styles';
import { WordInfoDummy } from '../Word/word';
import { Dialog } from '@material-ui/core';
import { Link } from 'react-router-dom';

const normalazeWord = (word) => {
  let a = word.replace(/[^a-zA-Z ]/g, "")
  let b = a.toLowerCase()
  return b;
}

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap' as 'wrap',
  },
  chip: {
    margin: theme.spacing.unit,
    border: 'none',
    height: 20,
    color: "#b2b2b2",
    boxShadow: "0 0 5px grey",
    '&:hover': {
      background: 'linear-gradient(to right, #0cebeb 0%, #5b86e5 100%)',
      color: '#fff'
    }
  },
});

const VocabularyHistory = connect("user")(class extends React.Component<any, any> {
  state = {
    history: {}
  }

  componentDidMount() {
    firebase.database().ref(`users/${this.props.store.user.uid}/userWord`).once('value', (snapshot: firebase.database.DataSnapshot) => {
      this.setState({
        history: snapshot.val()
      })
    })
  }

  render() {
    return <ul>
      {_.map(this.state.history, (item, key) => {
        return <li>{key}</li>
      })}
    </ul>
  }
})

class Book extends React.Component<any, {}> {
  state = {
    showWordPopup: false,
    selectedWord: null,
    bookInfo: {
      text: '',
      textName: '',
      richText: '',
      audioURL: '',
      audioToText: [],
      pagesAsArray: [],
      pagesWordIndex: [],
      previewText: ''
    },
    bookPreview: {
      picture: ''
    },
    userBookInfo: {
      currentAudioChapter: 0
    },
    wordsInfo: {},
    parsedText: [],
    centerTextIndex: 0,
    progress: 0,
    readMode: "WhiteMode",
    IsPlay: false,
  }



  showWordWithoutTranslate = () => {
    // _.forEach(document.querySelectorAll(`#richText [data-num]`), (el: any) => {
    //   let word = el.innerHTML;
    //   let wordFromDict = this.getTranslate(word);
    //   if (!wordFromDict) {
    //     el.style.background = '#eee';
    //   }
    // })
  }

  getCurrentAudioChapter = () => {
    return Math.min(this.state.userBookInfo.currentAudioChapter || 0, this.state.bookInfo.audioToText.length - 1)
  }
  getTextFromAudioTime = (seconds: number) => {
    const audioToText = this.state.bookInfo.audioToText[this.getCurrentAudioChapter()];

    const low = _.findLast(audioToText, item => item.time < seconds)
    const end = _.find(audioToText, item => item.time > seconds)

    const fragmentDuration = end.time - low.time;
    const currentWordIndex = Math.floor(low.wordIndex + (end.wordIndex - low.wordIndex) * (seconds - low.time) / fragmentDuration)

    _.forEach(document.querySelectorAll(`#richText [data-num]`), el => {
      el.classList.remove('highlight')
    })

    const gap = 14;
    const renderDelay = 2; // We need move highlight slightly right, because audio play first, then render happen 
    _.times(gap, (index) => {
      const el = document.querySelector(`#richText [data-num="${currentWordIndex - (gap / 2) + index + renderDelay}"]`);
      if (el) el.classList.add('highlight')
    })
    // Follow part
    // TODO refactor to find
    let page = -1;
    _.forEach(this.state.bookInfo.pagesWordIndex, ([begin, end], index) => {
      if (begin < currentWordIndex && currentWordIndex <= end) {
        page = index;
      }
    })
    // debugger
    this.setState({ centerTextIndex: page })
  }

  wordClick = (event) => {
    var target = event.target as HTMLElement;
    if (target.hasAttribute('data-num')) {
      const word = target.innerHTML.trim();
      this.setState({ showWordPopup: true, selectedWord: word });
    }
  }



  renderWordPopup = () => {
    if (!this.state.showWordPopup) {
      return;
    }



    const addFrom = {
      type: 'BOOK',
      id: this.props.match.params.id
    }

    return <Dialog open={this.state.showWordPopup} onClose={() => this.setState({showWordPopup: false})}>
      <WordInfoDummy word={this.state.selectedWord.trim()} addToDictionaryFrom={addFrom} />
    </Dialog>


  }

  componentDidUpdate() {
    this.showWordWithoutTranslate();
  }

  async componentDidMount() {
    
    document.addEventListener('click', this.wordClick)
    const book = await API.book(this.props.match.params.id).once();
    const bookPreview = await API.bookPreview(this.props.match.params.id).once() || {}
    const userBookInfo = await API.myUser().bookPreview(this.props.match.params.id).once() || {}
    API.myUser().bookPreview(this.props.match.params.id).update(bookPreview)
    this.setState({
      bookInfo: {
        ...book,
        richText: _.map(sortByKey(book.pages, key => +key.split("_")[1]), item => '<div class="english-page">' + item + '</div>').join(""),
        pagesAsArray: _.map(sortByKey(book.pages, key => +key.split("_")[1])),
        pagesWordIndex: _.map(sortByKey(book.pagesWord, key => +key.split("_")[1]))
      },
      userBookInfo: {
        ...this.state.userBookInfo,
        ...userBookInfo
      },
      bookPreview: {
        ...this.state.bookPreview,
        ...bookPreview
      }
    })
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.wordClick)
  }

  audioDOM: any
  onListen = () => {
    this.getTextFromAudioTime(this.audioDOM.audioEl.currentTime);
    API.myUser().bookPreview(this.props.match.params.id).update({
      audioProgressTime: this.audioDOM.audioEl.currentTime
    })
    this.setState({
      progress: (this.audioDOM.audioEl.currentTime / this.audioDOM.audioEl.duration) * 100
    })
  }

  renderAudioPlayer = () => {
    const urls = sortByKey(this.state.bookInfo.audioURL)
    const url = urls[this.getCurrentAudioChapter()]
    return <ReactAudioPlayer
      onEnded={() => {
        const nextChapter = (this.getCurrentAudioChapter()) + 1;
        API.myUser().bookPreview(this.props.match.params.id).update({
          currentAudioChapter: nextChapter,
          audioProgressTime: 0
        })
        this.setState({
          userBookInfo: {
            ...this.state.userBookInfo,
            currentAudioChapter: nextChapter
          }
        }, () => {
          this.onPlayButtonClick();
        });
      }}
      src={url}
      autoPlay={false}
      controls={true}
      ref={(element: any) => { this.audioDOM = element; }}
      listenInterval={1000}
      onListen={this.onListen}
    />
  }

  onPlayButtonClick = () => {
    const promise = this.audioDOM.audioEl.play();
    if (promise !== undefined) {
      promise.catch(error => {
        // Auto-play was prevented
        // Show a UI element to let the user manually start playback
      }).then(() => {
        //alert('THEN IN PLAY PROMISE')
        // Auto-play started
      });
    }
  }

  pauseButton = () => {
    this.audioDOM.audioEl.pause()
  }

  reductionPlus = (time) => {
    this.audioDOM.audioEl.currentTime += time
  }

  switchPageToRight = () => {
    this.setState({
      centerTextIndex: this.state.centerTextIndex + 1
    })
  }

  switchPageToLeft = () => {
    this.setState({
      centerTextIndex: this.state.centerTextIndex - 1
    })
  }

  speedUpTime = (time) => {
    this.audioDOM.audioEl.playbackRate = time
  }

  pageControl = (e) => {
    this.setState({
      centerTextIndex: (+e.target.value) - 1
    })
  }

  renderSettings = () => {
    const modes = [DarkMode, YellowMode, WhiteMode];
    const modesName = ["DarkMode", "YellowMode", "WhiteMode"]
    const modeComponent = modes.map((Mode, index) => <Mode
      key={index}
      className={style.settingIcon}
      onClick={() => this.setState({ readMode: modesName[index] })}
    />)
    return <div className={style.settingWrapper}>
      <div className={style.settingControlPopup}>
        {modeComponent}
      </div>
      <PlayerSetting className={style.playerSetting} />
    </div>
  }
 
  render() {
    const { classes } = this.props;
    const timeArrayMinus = [
      { text: "-5s", value: -5 },
      { text: "-10s", value: -10 },
      { text: "-20s", value: -20 },
      { text: "-30s", value: -30 },
      { text: "-1m", value: -60 },
      { text: "-5m", value: -300 }
    ]
    const timeArrayPlus = [
      { text: "+5s", value: +5 },
      { text: "+10s", value: +10 },
      { text: "+20s", value: +20 },
      { text: "+30s", value: +30 },
      { text: "+1m", value: +60 },
      { text: "+5m", value: +300 }
    ]
    const speedArray = [
      { text: "x0.25", value: 0.25 },
      { text: "x0.5", value: 0.5 },
      { text: "x0.75", value: 0.75 },
      { text: "x0.9", value: 0.9 },
      { text: "x1", value: 1 },
      { text: "x1.25", value: 1.25 },
      { text: "x1.5", value: 1.5 },
      { text: "x1.75", value: 1.75 },
      { text: "x1.9", value: 1.9 }
    ]

    const renderLabels = () => {
      const data = ['Гарри Поттер', 'ИИ', 'Хогвартс', 'Фанфик', 'Магия']
      return _.map(data, (label) => {
        return <Chip
          label={label}
          clickable
          className={classes.chip}
          color="primary"
          variant="outlined"
        />
      })
    }
    return (
      <div className={style.Book}>
        {this.renderWordPopup()}
        <button
          className={style.paginationMobile}
          onClick={this.switchPageToLeft}
        >
          <PaginationArrow className={style.paginationArrow} />
        </button>
        <button
          className={cn(style.paginationMobile, style.isRight)}
          onClick={this.switchPageToRight}
        >
          <PaginationArrow className={cn(style.paginationArrow, style.paginationArrowRotate)}/>
        </button>
        <div className={style.bookInfo}>
          {this.renderAudioPlayer()}
          <div>
            {/* <div className={style.paginationButton}>
              <PaginationArrow className={style.paginationArrow} />
            </div> */}
            <div className={style.breadCrumbs}>
              <Link to="/dashboard/books" className={style.breadCrumbsItem}>
                {"<"} Все книги
              </Link>
              {/* <div className={style.breadCrumbsItem}>
                >
              </div>
              <div className={style.breadCrumbsItem}>
                Гарри Поттер
              </div> */}
            </div>
          </div>
          <div className={style.bookInfoMain}>
            <div>
              <div className={style.bookInfoPicture} style={{ backgroundImage: `url(../../${this.state.bookPreview.picture})` }} />
            </div>
            <div>
              <div className={style.mainInfoName}>
                {this.state.bookInfo.textName}
              </div>
              <div className={style.mainInfoDescription}>
                {this.state.bookInfo.previewText}
              </div>
              {/* <div className={style.mainInfoButtonsWrapper}>
                <div className={style.mainInfoAddButton}>
                  добавить в библиотеку
                </div>
                <div className={style.playButton}>
                  <PlayIcon className={style.playIcon} />
                </div>
              </div> */}
            </div>
          </div>
          <div className={style.labels}>
            <div className={style.complexity}>
              <div>
                <ComplexityIcon />
              </div>
              <div>
                Идеальная сложность!
              </div>
            </div>
            <div className={style.pages}>
              <div className={style.pagesCurrentPage}>
                {this.state.centerTextIndex + 1}
              </div>
              <div>
                /
              </div>
              <div className={style.pagesLeftPage}>
                {this.state.bookInfo.pagesAsArray.length} страниц
              </div>
            </div>
            <div className={style.chip}>
              {renderLabels()}
            </div>
          </div>
        </div>
        {/* <div className={style.bookName}>{this.state.bookInfo.textName}</div> */}
        <div className={style.footer}>
            <Slider value={this.state.progress} step={0.00001} onChange={(progress) => {
              this.setState({
                progress
              })
              this.audioDOM.audioEl.currentTime = (this.audioDOM.audioEl.duration * progress) / 100
            }} />
          
          <div className={style.controls}>
            <div className={style.playerControls}>
              <ControlsPopup
                className={style.previouslyButton}
                renderButton={({ onClick, text }) => <button
                  onClick={onClick}
                  className={style.playerSpeed}
                >
                  {text}
                </button>}
                options={timeArrayMinus}
                defaultValue={-30}
                onSelect={this.reductionPlus}
              />
              <div className={style.playButton} onClick={(e: any) => {
                this.state.IsPlay ? this.pauseButton() : this.onPlayButtonClick()
                this.setState({ IsPlay: !this.state.IsPlay })
              }}>
                {this.state.IsPlay ? <div>||</div> : <PlayIcon className={style.playIcon} />}
              </div>
              <ControlsPopup
                className={style.previouslyButton}
                renderButton={({ onClick, text }) => <button
                  onClick={onClick}
                  className={style.playerSpeed}
                >
                  {text}
                </button>}
                options={timeArrayPlus}
                defaultValue={30}
                onSelect={this.reductionPlus}
              />
              <ControlsPopup
                renderButton={() => null}
                renderIcon={() => <PlayerSpeed className={style.playerSpeedIcon} />}
                options={speedArray}
                onSelect={this.speedUpTime}
                className={style.speedUp}
              />
            </div>
            <div className={style.bookControls}>
              <div className={style.pageControls}>
                <input
                  type='number'
                  value={this.state.centerTextIndex + 1}
                  onChange={this.pageControl}
                  className={style.paginationInput}
                />
                <div className={style.pageLeft}> из {this.state.bookInfo.pagesAsArray.length} </div>
              </div>
              <div className={style.paginationControls}>
                <button
                  className={style.paginationButton}
                  onClick={this.switchPageToLeft}
                >
                  <PaginationArrow className={style.paginationArrow} />
                </button>
                <div
                  className={style.paginationButton}
                  onClick={this.switchPageToRight}
                >
                  <PaginationArrow className={cn(style.paginationArrow, style.paginationArrowRotate)} />
                </div>
              </div>
              <div className={style.sizeControls}>
                <LoupeMinus className={style.loupeIcon} />
                <LoupePlus className={style.loupeIcon} />
              </div>
            </div>
            {this.renderSettings()}
          </div>
        </div>
        {/* <div
          className={"secondaryText textLeft " + this.state.readMode}
          dangerouslySetInnerHTML={{ __html: this.state.bookInfo.pagesAsArray[this.state.centerTextIndex - 1] }}
        /> */}
        <div
          className={cn(style.bookText, this.state.readMode)}
          id="richText"
          dangerouslySetInnerHTML={{ __html: this.state.bookInfo.pagesAsArray[this.state.centerTextIndex] }}
        />
        {/* <div
          className={"secondaryText textRight " + this.state.readMode}
          dangerouslySetInnerHTML={{ __html: this.state.bookInfo.pagesAsArray[this.state.centerTextIndex + 1] }}
        /> */}
        <Helmet>
          <body className="footer-none" />
        </Helmet>
      </div>
    );
  }
}

export default connect("user")(withStyles(styles)(Book));
