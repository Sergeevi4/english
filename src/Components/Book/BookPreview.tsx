import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "../../Store";

import styles from './BookPreview.module.scss';
import { Card } from '@material-ui/core';
import _ from 'lodash';

class BookPreview extends React.Component<any, any>{

  render() {
    console.log('props', this.props)
    return (
      <Card className={styles.BookPreview}>
        <div className={styles.mainBlock} style={{backgroundImage: `url(${this.props.picture})`}}>
          <div className={styles.author}>
            {this.props.author}
          </div>
          <div className={styles.bookName}>
            {this.props.bookName}
          </div>
        </div>
        <div className={styles.footer}>
        <Link to={'/dashboard/book/' + this.props.bookId} className={styles.readButton}>
          Читать
        </Link>
        </div>
      </Card>
    );
  }
}

export default connect("user")(BookPreview);
