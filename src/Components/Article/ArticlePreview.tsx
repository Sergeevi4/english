import React from 'react';
import ReactMarkdown from 'react-markdown';
import { Link } from 'react-router-dom';
import { getDateString } from '../../Utilities';

import { ReactComponent as AdminIcon } from '../icons/adminIcon.svg'

import styles from './ArticlePreview.module.scss'

interface IProps {
    url: string,
    title: string,
    textPreview: string,
    date: number,
    author: string
}

class ArticlePreview extends React.Component<IProps, any> {

    render() {
        return (
            <div className={styles.Article}>
                <div className="Article__articleBlock articleBlock">
                    <Link to={"/article/" + this.props.url}>
                        <h2 className={styles.title}>
                            {this.props.title}
                        </h2>
                    </Link>
                    <div className={styles.text}>
                        <div className={styles.markdown}>
                            <ReactMarkdown source={this.props.textPreview + ' ' + `[Читать дальше...](/article/${this.props.url})`} />
                        </div>
                    </div>
                    <div className={styles.footer}>
                        <div className={styles.calendar}>
                            <div className={styles.calendarText}>
                                {getDateString(new Date(+this.props.date))}
                            </div>
                        </div>
                        <div className={styles.author}>
                            <AdminIcon className={styles.icon}/>
                            <div className={styles.authorText}>
                                {this.props.author}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ArticlePreview;
