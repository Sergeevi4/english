import React from 'react';
import { auth } from 'firebase/app';
import ReactMarkdown from 'react-markdown';
import { Helmet } from "react-helmet";
import _ from 'lodash';
import cn from 'classnames';
import { ScrollToTopOnMount } from '../RouteHelpers/ScrollToTop';
import { getPageState, getDateString } from '../../Utilities';

import { ReactComponent as Arrow } from '../icons/right-arrow.svg'
import { ReactComponent as AdminIcon } from '../icons/adminIcon.svg'

import { Link } from 'react-router-dom';
import styles from './Article.module.scss'
import { ScrollToTop } from '../ScrollToTop/ScrollToTop';
import { API } from '../../API';

import {
    FacebookShareButton,
    LinkedinShareButton,
    TwitterShareButton,
    TelegramShareButton,
    WhatsappShareButton,
    PinterestShareButton,
    VKShareButton,
    OKShareButton,
    RedditShareButton,
    TumblrShareButton,
    LivejournalShareButton,
    MailruShareButton,
    ViberShareButton,
    WorkplaceShareButton,
    LineShareButton,
    PocketShareButton,
    InstapaperShareButton,
    EmailShareButton,
} from 'react-share';

import {
    FacebookIcon,
    TwitterIcon,
    TelegramIcon,
    WhatsappIcon,
    LinkedinIcon,
    PinterestIcon,
    VKIcon,
    OKIcon,
    RedditIcon,
    TumblrIcon,
    LivejournalIcon,
    MailruIcon,
    ViberIcon,
    WorkplaceIcon,
    LineIcon,
    PocketIcon,
    InstapaperIcon,
    EmailIcon,
} from 'react-share';
import { Button } from '@material-ui/core';
import {QuestionInArticle} from '../QuestionInArticle/QuestionInArticle';

interface IArticle { // or Props for dumb component
    text?: string,
    title?: string,
    date?: number,
    author?: string,
    description?: string,
    url?: string,
}

const shareButtons = [
    { Wrapper: FacebookShareButton, Icon: FacebookIcon },
    { Wrapper: TwitterShareButton, Icon: TwitterIcon },
    { Wrapper: VKShareButton, Icon: VKIcon },
    { Wrapper: TelegramShareButton, Icon: TelegramIcon },
    { Wrapper: LinkedinShareButton, Icon: LinkedinIcon },
    { Wrapper: WhatsappShareButton, Icon: WhatsappIcon },
]

export class ArticleDumb extends React.Component<IArticle> {

    renderShare = () => {
        return <div className={styles.share}>
            {_.map(shareButtons, ({ Wrapper, Icon }, index) => {
                return <Wrapper key={index} url={"https://clearenglish.io/article/" + this.props.url} >
                    <Icon size={40} round={true} />
                </Wrapper>
            })}
        </div>
    }
    
    render() {
        if (!this.props) {
            return <div className={styles.ArticleNotFound}>
                Произошла ошибка, статьи по такому адресу нет. :(
                <br />
                <Link to={'/'}>Вернутся на главную</Link>
            </div>
        }
        return (
            <div className="Article-wrapper">
                <Link to={'/articles'}><div className={styles.backAbsoluteButton}><Arrow className={styles.arrowIcon} /><div className={styles.backButtonText}>Назад</div></div></Link>
                <ScrollToTopOnMount />
                <ScrollToTop />
                <div className={styles.Articles}>
                    <div className={styles.Article}>
                        <div className="Article__articleBlock articleBlock">
                            <h1 className={styles.title}>
                                {this.props.title}
                            </h1>
                            <div className={styles.text}>
                                <div className={styles.markdown}>
                                    <ReactMarkdown source={this.props.text} />
                                </div>
                            </div>
                            <div className={styles.footer}>
                                {this.renderShare()}
                                <div className={styles.calendar}>
                                    <div className={styles.calendarText}>
                                        {getDateString(new Date(+this.props.date))}
                                    </div>
                                </div>
                                <div className={styles.author}>
                                    <AdminIcon className={styles.icon} />
                                    <div className={styles.authorText}>
                                        {this.props.author}
                                    </div>
                                </div>
                            </div>

                            {this.props.url === 'all-whole-entire' &&
                                <QuestionInArticle />
                            }

                        </div>
                    </div>
                </div>
                <Helmet defer={false}>
                    <title>{(this.props.title || 'статья') + ' | Clearenglish'}</title>
                    <meta name="description" content={this.props.description}></meta>
                    <link rel="canonical" href={"https://clearenglish.io/article/" + this.props.url} />
                </Helmet>
            </div>
        );
    }
}

type IState = { article: IArticle, haveSeenArticle: any };

export default class Article extends React.Component<any, IState> {
    state: IState = {
        article: getPageState() || {},
        haveSeenArticle: null
    }

    startTime: number = Date.now();
    updateData = null
    componentDidMount() {
        const articleId = this.props.match.params.id
        API.article(articleId).once(article => {
            this.setState({
                article
            }, () => window.infoForSeoRendered && window.infoForSeoRendered())
            window.PAGE_STATE = JSON.stringify(article)
        })

        const onUserLoad = _.once(user => {
            if (!user) return;

            const key = API.myUser().stat('articles').value.push({
                id: articleId,
                timeForRead: 0,
                date: Date.now()
            })
            this.updateData = setInterval(() => {
                const readTime = Date.now() - this.startTime
                key.update({ timeForRead: readTime })
                API.myUser().haveSeenArticle(articleId).update({
                    time: Math.max(readTime, this.state.haveSeenArticle.time)
                })
            }, 5000)

            API.myUser().haveSeenArticle(articleId).once(haveSeenArticle => {
                this.setState(
                    {
                        haveSeenArticle: haveSeenArticle || { time: 0 }
                    })
            })
        })
        auth().onAuthStateChanged(onUserLoad)
    }

    componentWillUnmount() {
        clearInterval(this.updateData)
        window.PAGE_STATE = null
    }

    render() {
        return <ArticleDumb {...this.state.article} />
    }
}
