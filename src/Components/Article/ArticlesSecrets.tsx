import React from 'react';
import _ from 'lodash';
import Spinner from '../Spinner/Spinner';
import { ReactComponent as CheckIcon } from '../icons/subCheckIcon.svg'

import styles from './ArticlesSecret.module.scss'
import { API } from '../../API';
import { ArticleDumb } from './Article';
import { escapeHtml } from '../../Utilities';

export class ArticlesSecrets extends React.Component {

    state = {
        allArticles: null,
        filter: {
            idioms: false,
            times: false,
            modalVerb: false,
            phrasalVerb: false,
            grammar: false,
            partOfSpeech: false,
            other: false
        }
    }

    async componentDidMount() {
        const allArticles = await API.articlesSecrets().once();
        this.setState({ allArticles })
        window.PAGE_STATE_ARTICLE = allArticles
    }

    articlesRender = () => {
        if (!this.state.allArticles) return <Spinner />

        const sortdate = _.sortBy(this.state.allArticles, (article) => -article.date)
        return _.map(sortdate, (info, key) => {
            return <div 
                key={key} 
                data-type="article" 
                data-url={info.url} 
                data-title={info.title + ' | Clearenglish'}
                data-head={`
                    <title>${escapeHtml((info.title || 'статья') + ' | Clearenglish')}</title>
                    <meta name="description" content="${escapeHtml(info.description)}"></meta>
                    <link rel="canonical" href="${escapeHtml("https://clearenglish.io/article/" + info.url)}" />
                `}
            >
                <ArticleDumb key={key} {...info} />
            </div>;
        })
    }

    renderFilter = () => {
        const date = [
            { label: 'Времена', key: 'times' },
            { label: 'Идиомы', key: 'idioms' },
            { label: 'Модальные глаголы', key: 'modalVerb' },
            { label: 'Фразовые глаголы', key: 'phrasalVerb' },
            { label: 'Грамматика', key: 'grammar' },
            { label: 'Части речи', key: 'partOfSpeech' },
            { label: 'Остальное', key: 'other' }
        ]
        return _.map(date, ({ label, key }) => {
            return <div key={key} className={styles.filterLabel + " " + (this.state.filter[key] && styles.isChecked)} onClick={() => {
                this.setState({
                    filter: {
                        ...this.state.filter,
                        [key]: !this.state.filter[key]
                    }
                })
            }}>
                <div>{label}</div>
                {this.state.filter[key] && <CheckIcon className={styles.filterCheckIcon} />}
            </div>
        })
    }

    render() {
        return <div data-type="secretParent" >{this.articlesRender()}</div>
    }
}
