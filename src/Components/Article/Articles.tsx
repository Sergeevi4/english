import React from 'react';
import _ from 'lodash';
import Spinner from '../Spinner/Spinner';
import cn from 'classnames'
import ArticlePreview from './ArticlePreview'
import { ReactComponent as CheckIcon } from '../icons/subCheckIcon.svg'

import styles from './Articles.module.scss'
import { ScrollToTop } from '../ScrollToTop/ScrollToTop';
import { API } from '../../API';
import Helmet from 'react-helmet';

export default class Articles extends React.Component {

    state = {
        allArticles: null,
        filter: {
            idioms: false,
            times: false,
            modalVerb: false,
            phrasalVerb: false,
            grammar: false,
            partOfSpeech: false,
            other: false,
            difference: false
        }
    }

    async componentDidMount() {
        const allArticles = await API.articlePreview().once();
        this.setState({ allArticles })
    }

    articlesRender = () => {
        if (!this.state.allArticles) return <Spinner />
        const filteredData = _.filter(this.state.allArticles, (article) => {
            if (_.every(this.state.filter, item => !item)) return true
            let needShowCurrentArticle = _.some(article.tags, tag => {
                return this.state.filter[tag];
            })
            return needShowCurrentArticle;
        })
        const sortdate = _.sortBy(filteredData, (article) => -article.date)
        return _.map(sortdate, ({ title, preview, date, author, url }, key) => <ArticlePreview
            textPreview={preview}
            key={key}
            title={title}
            date={date}
            author={author}
            url={url}
        />
        )
    }

    renderFilter = () => {
        const date = [
            { label: 'Времена', key: 'times' },
            { label: 'Идиомы', key: 'idioms' },
            { label: 'Модальные глаголы', key: 'modalVerb' },
            { label: 'Фразовые глаголы', key: 'phrasalVerb' },
            { label: 'Грамматика', key: 'grammar' },
            { label: 'Части речи', key: 'partOfSpeech' },
            { label: 'Разница между', key: 'difference' },
            { label: 'Остальное', key: 'other' }
        ]
        return _.map(date, ({ label, key }) => {
            return <div key={key} className={styles.filterLabel + " " + (this.state.filter[key] && styles.isChecked)} onClick={() => {
                this.setState({
                    filter: {
                        ...this.state.filter,
                        [key]: !this.state.filter[key]
                    }
                })
            }}>
                <div>{label}</div>
                {this.state.filter[key] && <CheckIcon className={styles.filterCheckIcon} />}
            </div>
        })
    }

    render() {
        return (
            <div className={cn(styles.Articles, 'global pl pr')}>
                <ScrollToTop />
                <div className={cn(styles.filterWrapper, 'global al')}>{this.renderFilter()}</div>
                {this.articlesRender()}
                <Helmet>
                    <link rel="canonical" href="https://clearenglish.io/articles" />
                </Helmet>
            </div>
        );
    }
}
