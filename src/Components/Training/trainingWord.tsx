export const top150 = [
    {
        "rank": "1",
        "word": "the",
        "count": "56271872"
    },
    {
        "rank": "2",
        "word": "of",
        "count": "33950064"
    },
    {
        "rank": "3",
        "word": "and",
        "count": "29944184"
    },
    {
        "rank": "4",
        "word": "to",
        "count": "25956096"
    },
    {
        "rank": "5",
        "word": "in",
        "count": "17420636"
    },
    {
        "rank": "6",
        "word": "i",
        "count": "11764797"
    },
    {
        "rank": "7",
        "word": "that",
        "count": "11073318"
    },
    {
        "rank": "8",
        "word": "was",
        "count": "10078245"
    },
    {
        "rank": "9",
        "word": "his",
        "count": "8799755"
    },
    {
        "rank": "10",
        "word": "he",
        "count": "8397205"
    },
    {
        "rank": "11",
        "word": "it",
        "count": "8058110"
    },
    {
        "rank": "12",
        "word": "with",
        "count": "7725512"
    },
    {
        "rank": "13",
        "word": "is",
        "count": "7557477"
    },
    {
        "rank": "14",
        "word": "for",
        "count": "7097981"
    },
    {
        "rank": "15",
        "word": "as",
        "count": "7037543"
    },
    {
        "rank": "16",
        "word": "had",
        "count": "6139336"
    },
    {
        "rank": "17",
        "word": "you",
        "count": "6048903"
    },
    {
        "rank": "18",
        "word": "not",
        "count": "5741803"
    },
    {
        "rank": "19",
        "word": "be",
        "count": "5662527"
    },
    {
        "rank": "20",
        "word": "her",
        "count": "5202501"
    },
    {
        "rank": "21",
        "word": "on",
        "count": "5113263"
    },
    {
        "rank": "22",
        "word": "at",
        "count": "5091841"
    },
    {
        "rank": "23",
        "word": "by",
        "count": "5061050"
    },
    {
        "rank": "24",
        "word": "which",
        "count": "4580906"
    },
    {
        "rank": "25",
        "word": "have",
        "count": "4346500"
    },
    {
        "rank": "26",
        "word": "or",
        "count": "4228287"
    },
    {
        "rank": "27",
        "word": "from",
        "count": "4108111"
    },
    {
        "rank": "28",
        "word": "this",
        "count": "4015425"
    },
    {
        "rank": "29",
        "word": "him",
        "count": "3971997"
    },
    {
        "rank": "30",
        "word": "but",
        "count": "3894211"
    },
    {
        "rank": "31",
        "word": "all",
        "count": "3703342"
    },
    {
        "rank": "32",
        "word": "she",
        "count": "3415846"
    },
    {
        "rank": "33",
        "word": "they",
        "count": "3340398"
    },
    {
        "rank": "34",
        "word": "were",
        "count": "3323884"
    },
    {
        "rank": "35",
        "word": "my",
        "count": "3277699"
    },
    {
        "rank": "36",
        "word": "are",
        "count": "3224178"
    },
    {
        "rank": "37",
        "word": "me",
        "count": "3027134"
    },
    {
        "rank": "38",
        "word": "one",
        "count": "2832569"
    },
    {
        "rank": "39",
        "word": "their",
        "count": "2820265"
    },
    {
        "rank": "40",
        "word": "so",
        "count": "2802481"
    },
    {
        "rank": "41",
        "word": "an",
        "count": "2641417"
    },
    {
        "rank": "42",
        "word": "said",
        "count": "2637136"
    },
    {
        "rank": "43",
        "word": "them",
        "count": "2509917"
    },
    {
        "rank": "44",
        "word": "we",
        "count": "2491655"
    },
    {
        "rank": "45",
        "word": "who",
        "count": "2472663"
    },
    {
        "rank": "46",
        "word": "would",
        "count": "2400858"
    },
    {
        "rank": "47",
        "word": "been",
        "count": "2357654"
    },
    {
        "rank": "48",
        "word": "will",
        "count": "2320022"
    },
    {
        "rank": "49",
        "word": "no",
        "count": "2241145"
    },
    {
        "rank": "50",
        "word": "when",
        "count": "1980046"
    },
    {
        "rank": "51",
        "word": "there",
        "count": "1961200"
    },
    {
        "rank": "52",
        "word": "if",
        "count": "1951102"
    },
    {
        "rank": "53",
        "word": "more",
        "count": "1899787"
    },
    {
        "rank": "54",
        "word": "out",
        "count": "1875351"
    },
    {
        "rank": "55",
        "word": "up",
        "count": "1792712"
    },
    {
        "rank": "56",
        "word": "into",
        "count": "1703963"
    },
    {
        "rank": "57",
        "word": "do",
        "count": "1680164"
    },
    {
        "rank": "58",
        "word": "any",
        "count": "1665366"
    },
    {
        "rank": "59",
        "word": "your",
        "count": "1658553"
    },
    {
        "rank": "60",
        "word": "what",
        "count": "1605908"
    },
    {
        "rank": "61",
        "word": "has",
        "count": "1602329"
    },
    {
        "rank": "62",
        "word": "man",
        "count": "1573117"
    },
    {
        "rank": "63",
        "word": "could",
        "count": "1571110"
    },
    {
        "rank": "64",
        "word": "other",
        "count": "1533530"
    },
    {
        "rank": "65",
        "word": "than",
        "count": "1508779"
    },
    {
        "rank": "66",
        "word": "our",
        "count": "1498473"
    },
    {
        "rank": "67",
        "word": "some",
        "count": "1476767"
    },
    {
        "rank": "68",
        "word": "very",
        "count": "1462382"
    },
    {
        "rank": "69",
        "word": "time",
        "count": "1449681"
    },
    {
        "rank": "70",
        "word": "upon",
        "count": "1424595"
    },
    {
        "rank": "71",
        "word": "about",
        "count": "1414687"
    },
    {
        "rank": "72",
        "word": "may",
        "count": "1400642"
    },
    {
        "rank": "73",
        "word": "its",
        "count": "1373270"
    },
    {
        "rank": "74",
        "word": "only",
        "count": "1318367"
    },
    {
        "rank": "75",
        "word": "now",
        "count": "1317723"
    },
    {
        "rank": "76",
        "word": "like",
        "count": "1280625"
    },
    {
        "rank": "77",
        "word": "little",
        "count": "1273589"
    },
    {
        "rank": "78",
        "word": "then",
        "count": "1255636"
    },
    {
        "rank": "79",
        "word": "can",
        "count": "1210074"
    },
    {
        "rank": "80",
        "word": "should",
        "count": "1192154"
    },
    {
        "rank": "81",
        "word": "made",
        "count": "1188501"
    },
    {
        "rank": "82",
        "word": "did",
        "count": "1185720"
    },
    {
        "rank": "83",
        "word": "us",
        "count": "1171742"
    },
    {
        "rank": "84",
        "word": "such",
        "count": "1136757"
    },
    {
        "rank": "85",
        "word": "a",
        "count": "1135294"
    },
    {
        "rank": "86",
        "word": "great",
        "count": "1120163"
    },
    {
        "rank": "87",
        "word": "before",
        "count": "1117089"
    },
    {
        "rank": "88",
        "word": "must",
        "count": "1108116"
    },
    {
        "rank": "89",
        "word": "two",
        "count": "1093366"
    },
    {
        "rank": "90",
        "word": "these",
        "count": "1090510"
    },
    {
        "rank": "91",
        "word": "see",
        "count": "1084286"
    },
    {
        "rank": "92",
        "word": "know",
        "count": "1075612"
    },
    {
        "rank": "93",
        "word": "over",
        "count": "1056659"
    },
    {
        "rank": "94",
        "word": "much",
        "count": "1021822"
    },
    {
        "rank": "95",
        "word": "down",
        "count": "989808"
    },
    {
        "rank": "96",
        "word": "after",
        "count": "978575"
    },
    {
        "rank": "97",
        "word": "first",
        "count": "978196"
    },
    {
        "rank": "98",
        "word": "Mr",
        "count": "974419"
    },
    {
        "rank": "99",
        "word": "good",
        "count": "966602"
    },
    {
        "rank": "100",
        "word": "men",
        "count": "923053"
    },
    {
        "rank": "101",
        "word": "own",
        "count": "922130"
    },
    {
        "rank": "102",
        "word": "never",
        "count": "899673"
    },
    {
        "rank": "103",
        "word": "most",
        "count": "889691"
    },
    {
        "rank": "104",
        "word": "old",
        "count": "887917"
    },
    {
        "rank": "105",
        "word": "shall",
        "count": "883846"
    },
    {
        "rank": "106",
        "word": "day",
        "count": "882331"
    },
    {
        "rank": "107",
        "word": "where",
        "count": "881975"
    },
    {
        "rank": "108",
        "word": "those",
        "count": "878621"
    },
    {
        "rank": "109",
        "word": "came",
        "count": "873144"
    },
    {
        "rank": "110",
        "word": "come",
        "count": "873007"
    },
    {
        "rank": "111",
        "word": "himself",
        "count": "863478"
    },
    {
        "rank": "112",
        "word": "way",
        "count": "860027"
    },
    {
        "rank": "113",
        "word": "work",
        "count": "829823"
    },
    {
        "rank": "114",
        "word": "life",
        "count": "825485"
    },
    {
        "rank": "115",
        "word": "without",
        "count": "819684"
    },
    {
        "rank": "116",
        "word": "go",
        "count": "816536"
    },
    {
        "rank": "117",
        "word": "make",
        "count": "807600"
    },
    {
        "rank": "118",
        "word": "well",
        "count": "799596"
    },
    {
        "rank": "119",
        "word": "through",
        "count": "792925"
    },
    {
        "rank": "120",
        "word": "being",
        "count": "792220"
    },
    {
        "rank": "121",
        "word": "long",
        "count": "791686"
    },
    {
        "rank": "122",
        "word": "say",
        "count": "788124"
    },
    {
        "rank": "123",
        "word": "might",
        "count": "787455"
    },
    {
        "rank": "124",
        "word": "how",
        "count": "770603"
    },
    {
        "rank": "125",
        "word": "am",
        "count": "761957"
    },
    {
        "rank": "126",
        "word": "too",
        "count": "758856"
    },
    {
        "rank": "127",
        "word": "even",
        "count": "750750"
    },
    {
        "rank": "129",
        "word": "again",
        "count": "745230"
    },
    {
        "rank": "130",
        "word": "many",
        "count": "744168"
    },
    {
        "rank": "131",
        "word": "back",
        "count": "740270"
    },
    {
        "rank": "132",
        "word": "here",
        "count": "729829"
    },
    {
        "rank": "133",
        "word": "think",
        "count": "715780"
    },
    {
        "rank": "134",
        "word": "every",
        "count": "704444"
    },
    {
        "rank": "135",
        "word": "people",
        "count": "701741"
    },
    {
        "rank": "136",
        "word": "went",
        "count": "690186"
    },
    {
        "rank": "137",
        "word": "same",
        "count": "689376"
    },
    {
        "rank": "138",
        "word": "last",
        "count": "680833"
    },
    {
        "rank": "139",
        "word": "thought",
        "count": "674623"
    },
    {
        "rank": "140",
        "word": "away",
        "count": "673810"
    },
    {
        "rank": "141",
        "word": "under",
        "count": "671168"
    },
    {
        "rank": "142",
        "word": "take",
        "count": "656486"
    },
    {
        "rank": "143",
        "word": "found",
        "count": "654512"
    },
    {
        "rank": "144",
        "word": "hand",
        "count": "648227"
    },
    {
        "rank": "145",
        "word": "eyes",
        "count": "647788"
    },
    {
        "rank": "146",
        "word": "still",
        "count": "640067"
    },
    {
        "rank": "147",
        "word": "place",
        "count": "621773"
    },
    {
        "rank": "148",
        "word": "while",
        "count": "613918"
    },
    {
        "rank": "149",
        "word": "just",
        "count": "610168"
    },
    {
        "rank": "150",
        "word": "also",
        "count": "608042"
    }
].map(({ word }) => word)

export const dota2 = [
    'tower',
    'building',
    'fountain',
    'shrine',
    'bounty',
    'courier',
    'scan',
    'rune',
    'invisibility',
    'illusion',
    'talent',
    'barrack',
    'range',
    'melee',
    'damage',
    'ability',
    'protection',
    'stun',
    'line',
    'glyph',
    'arcane',
    'dire',
    'radient',
    'neutral',
    'blood',
    'tron',
    'ward',
    'observer',
    'health',
    'sentry',
    'shop',
    'gold',
    'secret',
    'mana',
    'agility',
    'intelligence',
    'strength',
    'level',
    'experience',
    'aura',
    'movement',
    'physical',
    'magical',
    'attack',
    'river',
    'boot',
    'clarity',
    'smoke',
    'enchanted',
    'salve',
    'knowledge',
    'dust',
    'animal',
    'bottle',
    'branch',
    'iron',
    'gauntlet',
    'slipper',
    'mantle',
    'circlet',
    'belt',
    'robe',
    'crown',
    'axe',
    'blade',
    'alacrity',
    'stuff',
    'wizardry',
    'ring',
    'raindrop',
    'venom',
    'orb',
    'blight',
    'stone',
    'chainmail',
    'javelin',
    'hammer',
    'wind',
    'lace',
    'mask',
    'speed',
    'haste',
    'glove',
    'cloak',
    'void',
    'gem',
    'sight',
    'morbid',
    'shadow',
    'ghost',
    'scepter',
    'blink',
    'dagger',
    'energy',
    'booster',
    'vitality',
    'point',
    'evasion',
    'mystic',
    'reaver',
    'eagle',
    'song',
    'sacred',
    'relic',
    'wraith',
    'bracer',
    'soul',
    'oblivion',
    'perseverance',
    'madness',
    'hand',
    'travel',
    'moon',
    'shard',
    'buckler',
    'urn',
    'tranquil',
    'medallion',
    'courage',
    'endurance',
    'drum',
    'offering',
    'holy',
    'spirit',
    'vessel',
    'pipe',
    'guardian',
    'glimmer',
    'cape',
    'veil',
    'discord',
    'lens',
    'divinity',
    'rod',
    'solar',
    'crest',
    'orchid',
    'malevolence',
    'refresh',
    'scythe',
    'core',
    'hood',
    'defiance',
    'vanguard',
    'disk',
    'crimson',
    'black', 
    'king', 
    'bar',
    'hurricane',
    'pike',
    'style',
    'sphere',
    'heart',
    'assault',
    'cuirass',
    'armlet',
    'skull',
    'monkey',
    'battle',
    'fury',
    'ethereal',
    'radiance',
    'butterfly',
    'silver',
    'divine', 
    'abyssal',
    'thorn',
    'dragon', 
    'echo',
    'sabre',
    'maelstrom',
    'heaven',
    'halberd',
    'satanic',
    'eye',
]

export const csgo = [
    'behind',
    'under', 
    'bomb', 
    'flashbang',
    'blind',
    'hear',
    'defuse',
    'rush',
    'defend', 
    'rotation', 
    'drop', 
    'quick',
    'buy',
    'kit', 
    'close', 
    'smoke', 
    'burning', 
    'fake', 
    'ninja', 
    'one-way', 
    'step', 
    'enemy', 
    'clear', 
    'health', 
    'point', 
    'headshot', 
    'boost', 
    'take', 
    'radar', 
    'map', 
    'hide', 
    'hostage', 
    'armor', 
    'knife', 
    'graffiti', 
    'ammo', 
    'scope', 
    'jump', 
    'kill', 
    'side', 
    'lurk', 
    'aim', 
    'helmet', 
    'buy', 
    'ruin', 
    'split', 
    'force', 
    'clutch', 
    'ace', 
    'device', 
    'save', 
    'dust', 
    'office', 
    'mirage', 
    'lake',
    'box', 
    'door', 
    'outside', 
    'inside', 
    'middle', 
    'pit', 
    'long', 
    'short', 
    'ladder', 
    'column', 
    'balcony', 
    'car', 
    'goose', 
    'window', 
    'tunnel', 
    'palace', 
    'sandwich', 
    'van', 
    'ventilation', 
    'stair', 
    'spawn', 
    'kitchen', 
    'wall', 
    'parapet', 
    'hangar', 
    'garage', 
    'toxic', 
    'barrel', 
    'trash', 
    'library', 
    'jungle', 
    'roof', 
    'suicide', 
    'tree', 
    'bench', 
    'ballon', 
    'garden', 
    'toilet', 
]

export const cssHtml = [
    'input',
    'form',
    'select',
    'button',
    'shadow',
    'border',
    'background',
    'height',
    'width',
    'size',
    'align',
    'padding',
    'margin',
    'grid',
    'column',
    'row',
    'font',
    'direction',
    'bottom',
    'top',
    'hover',
    'active',
    'inherit',
    'position',
    'relative',
    'absolute',
    'fixed',
    'child',
    'color',
    'important',
    'transparent',
    'box',
    'weight',
    'bold',
    'cover',
    'overflow',
    'hidden',
    'collapse',
    'opacity',
    'body',
    'caption',
    'header',
    'main',
    'link',
    'mark',
    'strong',
    'small',
    'section',
    'source',
    'table',
    'article',
    'divide',
    'span',
    'head',
    'type',
    'style',
    'script',
]

export const beach = [
    'sand',
    'beach',
    'hotel',
    'temperature',
    'weather',
    'gravel',
    'sea',
    'ocean',
    'palm',
    'reef',
    'coast',
    'wave',
    'flow',
    'quay',
    'breakwater',
    'medusa',
    'turtle',
    'shark',
    'gull',
    'toilet',
    'wharf',
    'mussel',
    'oyster',
    'shrimp',
    'lobster',
    'octopus',
    'crab',
    'squid',
    'swimsuit',
    'cap',
    'catamaran',
    'sunbathe',
    'badminton',
    'frisbee',
    'shell',
    'buoy',
    'kayak',
    'bikini',
    'ship',
    'boat'
]

export const interfacePack = [
    'menu',
    'file',
    'folder',
    'recent',
    'save',
    'auto',
    'preference',
    'revert',
    'project',
    'editor',
    'edit',
    'undo',
    'redo',
    'cut',
    'copy',
    'paste',
    'find',
    'replace',
    'toggle',
    'switch',
    'search',
    'output',
    'console',
    'terminal',
    'control',
    'definition',
    'configuration',
    'documentation',
    'tip',
    'trick',
    'report', 
    'issue',
    'extension',
    'snippet',
    'theme',
    'appearence',
    'split',
    'group',
    'view',
    'license',
    'collaps'
] 


export const reddit = [
    'sort',
    'popular',
    'community',
    'hour',
    'minute',
    'day',
    'ago',
    'card',
    'hot',
    'notification',
    'all',
    'share',
    'career',
    'award',
    'search',
    'hide',
    'report',
    'help',
    'comment',
    'settings',
    'profile',
    'stuff',
    'create',
    'save',
    'content',
    'members',
    'relevance',
    'cake',
    'unread',
    'digest',
    'receive',
    'replies',
    'agreement',
    'push',
    'give',
    'turned',
    'administrative',
    'mentions',
    'mark'
]