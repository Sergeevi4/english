import * as React from 'react';
import * as _ from 'lodash';
import firebase from '../../Firebase';
import { connect, set as storeSet } from "../../Store";
import { Store } from "../../interface";

import { pasreWord, speakText, sleep, isUserAndUserInfoLoaded, shuffleWithPriority } from '../../Utilities'

import Button from '@material-ui/core/Button';
import { Fab, Input } from '@material-ui/core';

import { ReactComponent as CheckIconTrain } from '../icons/CheckIconTrain.svg'
import { ReactComponent as BlankCircle } from '../icons/blankCircle.svg';
import { ReactComponent as Arrow } from '../icons/right-arrow.svg'
import SpeakIcon from '@material-ui/icons/VolumeUpRounded';

import style from './Training.module.scss'
import Spinner from '../Spinner/Spinner';
import cn from 'classnames';
import history from '../../history';
import { API, UserWord } from '../../API';
import wordRank from '../../wordRank.json';
import { database } from 'firebase/app';

import * as packsInfo from './trainingWord'

import KeyboardEventHandler from 'react-keyboard-event-handler';
import { getUserLevelWordPriority, spacedRepetitionGap, getDefaultSpaceRepetitionForUser } from '../../Infos';


const contextTrainSentences = {
  'noun': [
    {
      ru: (word) => `можешь мне дать ${word}`,
      eng: (word) => `can you give me a ${word}`
    },
    {
      ru: (word) => `Я потерял ${word}`,
      eng: (word) => `I lost a ${word}`
    }
  ]
}

interface IProps {
  store: Store;
  match: {
    params: {
      type: 'translate-eng-ru' | 'translate-for-time' | 'translate-from-audio' | 'write' | 'context'
    }
  }
}

// interface IState {
//   wordsInfo: string,
//   isAnswersShown: boolean,
//   clickedIndex: number,
//   progress: number,
//   fullOpenButton: number,
//   timeLeft: number,
//   inCorrectAnswersCounter: number,
//   currentWordIndex: number,
//   inputWord: string,
// }

class Training extends React.Component<IProps, any> {
  state = {
    wordsInfo: null,
    isAnswersShown: false,
    clickedIndex: null,
    progress: 0,
    fullOpenButton: -1,
    timeLeft: 30,
    inCorrectAnswersCounter: 0,
    currentWordIndex: 0,
    inputWord: '',
  }

  timer = null;

  startTime = Date.now()
  trainTasks = [] as { word: string, answers: string[], correctIndex: number, randomNumber: number }[]
  lastLoadedWordIndex = 0;


  loadWord = () => {
    const loadGap = 5;
    const loadWord = _.flatMap(
      _.slice(this.trainTasks, this.lastLoadedWordIndex, this.state.currentWordIndex + loadGap),
      ({ answers }) => answers.map(word => API.word(word).once())
    )
    this.lastLoadedWordIndex = this.state.currentWordIndex + loadGap

    console.log({loadWord})
    Promise.all(loadWord).then(wordsInfo => {
      this.setState({
        wordsInfo: {
          ...this.state.wordsInfo,
          ..._.keyBy(wordsInfo, 'word')
        }
      })
      if (this.trainTasks[this.state.currentWordIndex]) {
        speakText({text: this.trainTasks[this.state.currentWordIndex].word, utteranceOptions: { volume: 0.65 }, onError: _.noop})
      }
    })
  }

  selectTrainingWords = async () => {
    await isUserAndUserInfoLoaded;
    let trainPacksSetting = _.get(this.props.store, 'userInfo.trainPacksSetting')
  
    // TODO Need refactor from hero to --->
    let trainWords = _.keys(
      _.pickBy(this.props.store.userInfo.userWords, (word) => word.added)
    )

    if (trainPacksSetting && !_.some(trainPacksSetting, {value: 'ALL'})) {
      trainWords = _.uniq(_.flatMap(trainPacksSetting, ({value}) => {
        return packsInfo[value]
      }))
    }

    if (_.some(trainPacksSetting, {value: 'ALL_BOOK'})) {
      trainWords.push(..._.keys(
        _.pickBy(this.props.store.userInfo.userWords, (word) => word.added && _.get(word, 'addedFrom.type') === 'BOOK')
      ))
    }

    trainWords = trainWords.filter(item => item);
    if (trainWords.length === 0) {
      // If user delete all pack
      trainWords = packsInfo.top150
    }

    // TODO -----> TO here

    const shuffledTrainWords = shuffleWithPriority(trainWords.map(word => {
      // const dateNow = Date.now() + 35*24*60*60*1000 // Change it variable to something like Date.now() + 24*60*60*1000 for easy testing
      const dateNow = Date.now()
      const wordIndex = (wordRank.indexOf(word) === -1) ? 2001 : wordRank.indexOf(word)
      const userLevelFactor = getUserLevelWordPriority(this.props.store.userInfo.choosenLevel, wordIndex)
      let spacedRepetitionFactor = 0.01; // If user never train this word

      const wordProgress = _.get(this.props.store, `userInfo.userWords[${word}].progress[${this.props.match.params.type}]`) as UserWord['progress']['anyTrain']
      if (wordProgress) {
        const dayFromLastTrain = dateNow - _.get(_.last(wordProgress.results), 'date', 0)
        const idealTime = spacedRepetitionGap[wordProgress.spacedRepetitionGroup]

        spacedRepetitionFactor = 0.2; // Пора тренировать
        if (dayFromLastTrain < idealTime) {
          spacedRepetitionFactor = 0.03; // Еще не пора, но уже можно
        }
        if (dayFromLastTrain * 1.1 < idealTime) {
          spacedRepetitionFactor = 0.003; // Вообще нельзя
        }
      }
      
      

      
      return {
        el: word,
        priority: spacedRepetitionFactor * userLevelFactor,
        // meta for easy testing (look result in console.log)
        spacedRepetitionFactor, 
        userLevelFactor
      }
    }))

    console.log({shuffledTrainWords})

    this.trainTasks = _.map(shuffledTrainWords, (word, index) => {
      if (shuffledTrainWords.length < 15) {

      }
      const incorrectWords = shuffledTrainWords.length < 15
        ? _.take(_.without(_.union(shuffledTrainWords, packsInfo.top150), word), index + 10)
        : _.take(_.without(shuffledTrainWords, word), index + 10)

      const answers = _.shuffle([
        word,
        ..._.sampleSize(incorrectWords, 4)
      ]);

      return {
        word,
        answers: answers,
        correctIndex: _.indexOf(answers, word),
        randomNumber: Math.floor(Math.random() * 1000*1000) // We need this for context train, and maybe something else in future
      }
    })
    this.loadWord();
  }

  componentDidMount() {
    this.selectTrainingWords();
    // this.getAnswers();
    if (this.props.match.params.type !== 'translate-for-time') return;
    this.timer = setInterval(() => {
      if (this.state.timeLeft < 0) {
        this.saveAchivements()
        alert(`Awesome! Вы набрали ${this.state.progress} правильных ответов`)
        this.setState({ timeLeft: 30, progress: 0 })
      } else {
        this.setState({ timeLeft: this.state.timeLeft - 1 }) // TODO inaccurate timer!!!
      }
    }, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  renderProgress = () => {
    const progressItems = _.times(10, i => i < this.state.progress
      ? <CheckIconTrain key={i} className={style.icons} />
      : <BlankCircle key={i} className={style.icons} />
    )

    return <div className={style.progress}>
      {progressItems}
    </div>
  }

  renderTimeLeft = () => {
    if (this.props.match.params.type !== 'translate-for-time') return null;
    return <div className={style.timeLeft}>
      <div style={{ width: this.state.timeLeft / 30 * 100 + '%', transition: '0.2' }} />
    </div>
  }

  renderWordInfo = () => {
    const trainTask = this.trainTasks[this.state.currentWordIndex];

    const engWord =  this.trainTasks[this.state.currentWordIndex].word;
    const answerEng = this.props.match.params.type === 'context'
      ? contextTrainSentences.noun[trainTask.randomNumber % contextTrainSentences.noun.length].eng(engWord)
      : engWord

    const transcription = _.get(this.state.wordsInfo, `[${answerEng}].transcription`)
    const showFullInfo = ['translate-eng-ru', 'translate-for-time', 'context'].includes(this.props.match.params.type);
    const ruWord = pasreWord(this.state.wordsInfo[answerEng]).showMeFirst || "Load"
    return <div className={style.wordInfo}>
      <Fab onClick={() => {
        speakText({text: answerEng});
      }}><SpeakIcon /></Fab>
      {showFullInfo && <div className="train-wordInfoOriginal">{answerEng}</div>}
      {this.props.match.params.type === 'write' && <div>{ruWord}</div>}
      {showFullInfo && <div className={style.transcription}>{transcription}</div>}
    </div>
  }

  goToNextQuestion  = () => {
    // TODO we need add some code to check, do we have next question ?


    this.setState({
      currentWordIndex: this.state.currentWordIndex + 1,
      isAnswersShown: false,
      fullOpenButton: -1,
      inputWord: '',
    }, this.loadWord)
    if (this.answerInput) {
      this.answerInput.focus();
    }
  }

  renderAnswers = () => {
    const isAnswersShown = this.state.isAnswersShown;
    const trainTask = this.trainTasks[this.state.currentWordIndex]

    // const answers = this.trainTasks[this.state.currentWordIndex].answers;

    // console.log({ answers })
    const answersComponents = _.map(trainTask.answers, (word, index) => {
      const correctIndex = this.trainTasks[this.state.currentWordIndex].correctIndex;
      const isCorrect = this.state.clickedIndex === correctIndex

      const ruWord = pasreWord(this.state.wordsInfo[word]).showMeFirst
      const buttonText = this.props.match.params.type === 'context' 
        ? contextTrainSentences.noun[trainTask.randomNumber % contextTrainSentences.noun.length].ru(ruWord)
        : ruWord

      const btnClass = cn(style.answerButton, {
        [style.correct]: this.state.isAnswersShown && correctIndex === index,
        [style.notCorrect]: this.state.isAnswersShown && !isCorrect && this.state.clickedIndex === index,
        [style.isOpen]: this.state.fullOpenButton === index
      })

      const btnOnClick = () => {
        if (isAnswersShown) {
          this.setState({ fullOpenButton: index })
        } else {
          const isCorrectAnswer = correctIndex === index
          const answerEng = this.trainTasks[this.state.currentWordIndex].word;
          this.changeWordProgress(answerEng, isCorrectAnswer)
          this.setState({
            fullOpenButton: index,
            isAnswersShown: true,
            clickedIndex: index,
            progress: this.state.progress + (isCorrectAnswer ? 1 : 0),
            inCorrectAnswersCounter: this.state.inCorrectAnswersCounter + (isCorrectAnswer ? 0 : 1)
          })
        }
      }

      return <Button
        component='div'
        className={btnClass}
        onClick={btnOnClick}
        variant="contained"
        key={index}
      >
        <span className={style.number}>{index + 1}</span>
        {buttonText || "Load"}
        <div className={style.eachAnswer}>{word}</div>
        {this.state.isAnswersShown && <Arrow className={style.arrowIcon} />}
      </Button>
    })
    return <div className={cn(style.answers, { [style.isAnswersShown]: isAnswersShown })}>{answersComponents}</div>
  }

  handleChange = (name: any) => event => {
    const answer = this.trainTasks[this.state.currentWordIndex].word.toLowerCase();
    this.setState({ [name]: event.target.value.substr(0, answer.length).toLowerCase() });

    if (event.target.value.length >= answer.length) {
      const nothingChange = this.state.inputWord === event.target.value.substr(0, answer.length).toLowerCase()
      if (!nothingChange) {
        const isCorrect = event.target.value.toLowerCase() === answer
        this.changeWordProgress(answer, isCorrect)
        this.setState({
          isAnswersShown: true,
          progress: this.state.progress + (isCorrect ? 1 : 0),
          inCorrectAnswersCounter: this.state.inCorrectAnswersCounter + (isCorrect ? 0 : 1)
        }) 
      } 
    }
    this.setState({ [name]: event.target.value.substr(0, answer.length).toLowerCase() });
  };

  changeWordProgress = (word, isCorrectAnswer) => {
    try {
      const wordIndex = (wordRank.indexOf(word) === -1) ? 2001 : wordRank.indexOf(word)
      const defaultProgress = {
        results: [],
        spacedRepetitionGroup: getDefaultSpaceRepetitionForUser(wordIndex, this.props.store.userInfo.choosenLevel)
      }
      const oldObj = _.get(this.props.store.userInfo, `userWords[${word}].progress[${this.props.match.params.type}]`, defaultProgress)
      API.myUser().userWords().value.child(`${word}/progress/${this.props.match.params.type}`).update({
        results: [...(_.isArray(oldObj.results) ? oldObj.results : []), {
          date: database.ServerValue.TIMESTAMP as number,
          isCorrectAnswer
        }],
        spacedRepetitionGroup: Math.max(0, oldObj.spacedRepetitionGroup + (isCorrectAnswer ? 1 : -1))
      }) 
    } catch (e) {
      console.error('something wrong in changeWordProgress', e)
    }
  }

  answerInput: HTMLInputElement = null;
  renderAnswerInput = () => {
    const answerEng = this.trainTasks[this.state.currentWordIndex].word.toLowerCase();
    return <>
      <Input
        inputRef={(node) => this.answerInput = node}
        value={this.state.inputWord}
        onChange={this.handleChange('inputWord')}
        placeholder={_.repeat('*', answerEng.length)}
        classes={{input: style.input, root: style.inputRoot}}
        inputProps={{
          style: {width: answerEng.length * 30 + 'px'}
        }}
      />
      {this.state.isAnswersShown && <span className={style.engAnswer}>
        {_.map(answerEng, (letter, index) => {
          const isCorrect = letter === this.state.inputWord[index]
          return <span key={index} className={cn(style.letter, {[style.isNotCorrect]: !isCorrect})}>{letter}</span>;
        })}
      </span>}
    </>
  }

  saveAchivements = () => {
    firebase.database().ref('users/' + this.props.store.user.uid + "/stats/" + this.props.match.params.type).push({
      date: Date.now(),
      inCorrectAnswers: this.state.inCorrectAnswersCounter,
      timeForPass: Date.now() - this.startTime,
      correctAnswers: this.state.progress
    })
  }

  handleKeyboard = (key, e: Event) => {
    console.log(key, e)
    if (key === "enter") {
      e.stopPropagation();
      e.preventDefault();
      this.goToNextQuestion ();
    }

    if (this.props.match.params.type === 'write') {return}
    const keyNumber = parseInt(key) - 1
    if (keyNumber >= 0 && keyNumber <= 4) {
      
      const isCorrect = this.trainTasks[this.state.currentWordIndex].correctIndex === keyNumber;
      this.setState({
        fullOpenButton: keyNumber,
        isAnswersShown: true,
        clickedIndex: keyNumber,
        progress: this.state.progress + (isCorrect ? 1 : 0),
        inCorrectAnswersCounter: this.state.inCorrectAnswersCounter + (isCorrect ? 0 : 1)
      })
    }
  }

  checkIsTrainingFinish = () => {
    if (this.state.progress === 10 || this.state.currentWordIndex >= this.trainTasks.length) {
      this.saveAchivements()
      this.setState({
        progress: 0,
        timeLeft: 30
      })
      alert("Awesome!");
      history.push('/dashboard')
      return true;
    }
  }

  render() {
    const wordsInfo = this.state.wordsInfo;

    if (!wordsInfo || this.trainTasks.length === 0) return <Spinner />

    if (this.checkIsTrainingFinish()) {
      return null;
    }

    return (
      <div className={cn(style.page, "page")}>
        <KeyboardEventHandler
          handleFocusableElements={true}
          handleKeys={['enter', '1', '2', '3', '4', '5']}
          onKeyEvent={this.handleKeyboard} 
        />
        {this.renderTimeLeft()}
        {this.renderProgress()}
        {this.renderWordInfo()}
        {this.props.match.params.type === 'write'
          ? this.renderAnswerInput()
          : this.renderAnswers()
        }
        <Button className={style.button} variant="contained" color="primary" onClick={this.goToNextQuestion } >Следующее ></Button>
      </div>

    );
  }
}

export default connect("_ALL")(Training);
