import React from 'react'
import Select from 'react-select';
import _ from 'lodash'

import styles from './StartTrainScreen.module.scss'
import { LinkButton } from '../UI/LinkButton';
import { API } from '../../API';
import { RouteComponentProps } from 'react-router';
import { useStore } from '../../Utilities';
import cn from 'classnames'

export const StartTrainScreen = (props: RouteComponentProps<{type: string}>) => {
    const packs = API.myUser().packs().useOnce();
    const trainPacksSetting = _.get(useStore(), 'userInfo.trainPacksSetting');
    const userWords = _.get(useStore(), 'userInfo.userWords');
    const isUserHaveBooksWord = _.some(userWords, (word) => word.added && _.get(word, 'addedFrom.type') === 'BOOK')
    const packsForSelect = _.map(_.pickBy(packs), (item, key) => {
        return {value: key, label: "Набор: " + key}
    }).concat({value: 'ALL', label: 'Все слова'})
    .concat(isUserHaveBooksWord ? {value: 'ALL_BOOK', label: 'Все слова из книг'} : [])

    const handleChange = selectedOption => {
        API.myUser().trainPacksSetting().set(selectedOption)
    };

    return <div className={cn(styles.wrapper, 'page')}>
            <div className={styles.topSection}>
                <div className={styles.text}>
                    Я хочу тренировать
                </div>
                <Select
                    value={trainPacksSetting}
                    isMulti
                    placeholder='Все слова'
                    options={packsForSelect}
                    className={styles.startTrainSelect}
                    classNamePrefix="select"
                    onChange={handleChange}
                />
            </div>
            <LinkButton to={'/dashboard/training/' + props.match.params.type} className={styles.startButton}>
                Поехали
            </LinkButton>
        </div>
}

