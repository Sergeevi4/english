import * as React from 'react';

interface IProps {
    onChange: any,
    children: any
}

export class FileButton extends React.Component<IProps, any> {
    render() {
        const uid = "" + Math.random();
        return <label htmlFor={uid}>
            <input
                // accept="audio/*"
                style={{ display: 'none' }}
                id={uid}
                type="file"
                onChange={this.props.onChange}
                multiple={true}
            />
            {this.props.children}
        </label>
    }
}
