import * as React from 'react';
import * as _ from 'lodash';

import cn from 'classnames'
import  './ControlsPopup.scss'

interface IState {
  selectedValue: number
}

interface IProps {
  options:  Array<{text: string, value: number}>,
  onSelect(value: any): any,
  className: string,
  renderButton?: ({onClick, text}: {onClick: (...args: any[]) => any, text: string}) => JSX.Element,
  renderIcon?: () => JSX.Element,
  defaultValue?: any
}

class ControlsPopup extends React.Component<IProps, IState> {

  state:IState = {
    selectedValue: null,
  }

  renderDropDownList = () => {
    return _.map(this.props.options, ({text, value}) => <div className="footerControl__item" key={value} onClick={() => {
      this.setState({
        selectedValue: value
      })
      this.props.onSelect(value)
    }}>{text}</div>)
  }

  renderButton = () => {
    const defaultElement = _.find(this.props.options, {value: this.state.selectedValue || this.props.defaultValue}) || this.props.options[0]
    return this.props.renderButton
      ? this.props.renderButton({onClick: () => this.props.onSelect(defaultElement.value), text: defaultElement.text})
      : <div>test</div>
  }

  render() {
    return <div className={cn("dropdown", this.props.className)}>
      {this.renderButton()}
    </div>
  }
}


export default ControlsPopup;
