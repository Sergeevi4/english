import * as React from 'react';

import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import _ from 'lodash';

const MyLink = _.memoize((to) => props => <Link to={to} {...props} />)


export class LinkButton extends React.Component<any, any> {
    render() {
        return <Button component={MyLink(this.props.to)} variant="contained" color="primary" {...this.props}>{this.props.children}</Button>
    }
}
