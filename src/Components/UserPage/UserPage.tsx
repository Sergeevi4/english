import React from 'react';
import { connect } from '../../Store';
import { auth } from 'firebase/app';
import _ from 'lodash';
import cn from 'classnames'

import styles from './UserPage.module.scss'
import { Button, Tabs, Tab } from '@material-ui/core';
import UserStatsCard from './UserStatsCard';

import userIcon from '../icons/headerUser.svg'
import { getDateString, useStore } from '../../Utilities';
import Achive, { allAchives } from '../Achive/Achive';
import {Subscription} from '../Subscription/Subscription';
import { withRouter } from 'react-router';
import PaymentIcons from '../PaymentIcons/PaymentIcons';
import { API } from '../../API';
import { UserComponent } from '../Header/User';
import { allTrainingsInfo } from '../../Infos';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { ReactComponent as CopyIcon } from '../icons/copy-content.svg'
import { withSnackbar } from 'notistack';
import GetUserLevel from '../UserLevel/GetUserLevel';
import { Link } from 'react-router-dom';

const InviteFriends = withSnackbar((props) => {
    const store = useStore()
    return <div className={styles.copyWrapper}>
        <div className={styles.copyLink}>clearenglish.io/invite/{store.user.uid}</div>
        <CopyToClipboard onCopy={() => props.enqueueSnackbar('Скопировано')} text={`https://clearenglish.io/invite/${store.user.uid}`}>
            <CopyIcon className={styles.copyIcon} />
        </CopyToClipboard>
    </div>
})

const UserPageKeyValue = (props) => {
    return <div className={styles.keyValue}>
        <div className={styles.title}>
            {props.keyElement}
        </div>
        <div className={styles.value}>
            {props.value}
        </div>
    </div>
}

class UserPage extends React.Component<any, any>{

    state = {
        userStats: undefined,
        value: 0
    }

    logOut = () => {
        auth().signOut()
    }

    async componentDidMount() {
        const userStats = await API.myUser().stats().once()
        this.setState({
            userStats: userStats || {}
        })
    }

    renderTrainCards = () => {
        const userStats = this.state.userStats
        if (userStats === undefined) return
        return _.map(allTrainingsInfo, ({ url, name }) => {
            const currentStats = _.map(userStats[url])
            const date = (_.maxBy(currentStats, 'date') || {} as any).date
            const InCorrectCounter = _.sumBy(currentStats, 'inCorrectAnswers')
            const correctCounter = _.sumBy(currentStats, 'correctAnswers')
            const finishCounter = _.size(currentStats)
            const SuccessPercent = (correctCounter / (correctCounter + InCorrectCounter) * 100).toFixed(1)
            return <UserStatsCard
                key={url}
                date={date}
                title={name}
                finishCounter={finishCounter}
                percent={SuccessPercent}
            >
                <Achive
                    userData={userStats[url]}
                    achiveData={allAchives['trains']}
                />
                <Achive
                    userData={userStats[url]}
                    achiveData={allAchives['trainsMistake']}
                />
            </UserStatsCard>
        })
    }

    renderArticlesCard = () => {
        const userStats = this.state.userStats
        if (userStats === undefined) return
        const date = (_.maxBy(_.map(userStats.articles), 'date') || {} as any).date
        const haveReadCounter = _.uniqBy(_.filter(userStats.articles, ({ timeForRead }) => timeForRead > 20000), 'id').length
        return <UserStatsCard
            type={'articles'}
            date={date}
            title={"Статьи"}
            haveReadCounter={haveReadCounter}
        >
            <Achive
                userData={userStats.articles}
                achiveData={allAchives.articles}
            />
        </UserStatsCard>
    }

    calcTrainAchievsWithoutMistakes = () => {
        const userStats = this.state.userStats
        if (userStats === undefined) return

        const trains = [
            { key: 'translate-eng-ru', name: 'перевод слова' },
            { key: 'translate-for-time', name: 'перевод на время' },
            { key: 'translate-from-audio', name: 'перевод на слух' }
        ]

        const trainAchivement = _.map(trains, ({ key, name }) => {
            const countBestTrain = _.filter(userStats[key], (train) => {
                return train.inCorrectAnswers == 0
            }).length
            return <li>{name + (countBestTrain === 5 ? " Разблокировано" : " Не разблокировано")}</li>
        })
        return <ul>{trainAchivement}</ul>
    }

    getLastActivity = () => {
        const userStats = this.state.userStats
        if (!userStats) return {}
        const result = _.maxBy(_.flatMap(this.state.userStats, _.toArray), 'date') as any || {}
        return result
    }

    handleCallToRouter = (_, url) => {
        this.props.history.push(url);
    }

    renderPaymentStatus = () => {
        if (!this.props.store.userInfo || !this.props.store.user) { return null }
        const paymentStatus = {
            basic: "Базовый",
            standard: "Стандарт",
            premium: "Премиум"
        }

        return <UserPageKeyValue keyElement="Тарифный план" value={<>
            <div className={styles.valuePayment}>{paymentStatus[this.props.store.paymentStatus]}</div>
            <PaymentIcons type='userInfo' />
        </>}/>
    }

    UserIconWithPaymentStatus = () => {
        if (!this.props.store.userInfo || !this.props.store.user) { return null }
        return <div className={styles.helperForIcon}>
            <img className={styles.userPhoto} src={this.props.store.user.photoURL || userIcon} alt='User photo' />
            <PaymentIcons type='userIcon' />
        </div>
    }

    render() {
        return (
            <div className={styles.userPage}>
                <div className={cn(styles.userSection, 'global pl pr')}>
                    <div className={styles.userWrapper}>
                        {this.UserIconWithPaymentStatus()}
                        <div className={styles.userInfo}>
                            <UserComponent />
                            <div className={styles.bottomSection}>
                                <UserPageKeyValue keyElement="Уровень" value={<Link to={'/user-level'} className={styles.userLevelLink}><GetUserLevel /></Link>} />
                                <UserPageKeyValue keyElement="Последняя активность" value={getDateString(new Date(this.getLastActivity().date)) || 'Нет данных'} />
                                {this.renderPaymentStatus()}
                                <UserPageKeyValue keyElement="Пригласи друга и получи премиум" value={<InviteFriends />} />
                            </div>
                        </div>
                    </div>
                    <Tabs
                        className={styles.tabsFooter}
                        value={this.props.history.location.pathname}
                        onChange={this.handleCallToRouter} >
                        <Tab label="Достижения" value='/dashboard/account' />
                        <Tab label="Подписка" value='/dashboard/account/payment' />
                    </Tabs>
                </div>
                <div className={cn(styles.userStatsPaymentBlock, 'global pl pr')}>
                    {this.props.history.location.pathname === '/dashboard/account' && <div className={styles.userStatsWrapper}>
                        {this.renderTrainCards()}
                        {this.renderArticlesCard()}
                    </div>}
                    {this.props.history.location.pathname === '/dashboard/account/payment'
                        && <div className={styles.subUserWrapper}>
                            <div className={styles.userSubText}>Ускорь изучение английского языка вместе с нашими тарифными планами</div>
                            <Subscription type={'userPageSub'} />
                        </div>}
                </div>
            </div>
        )
    }
}

export default connect('_ALL')(withRouter(UserPage))
