import React from 'react'
import _ from 'lodash'
import './UserStatsCard.scss'
import { getDateString } from '../../Utilities';
import { Card } from '@material-ui/core';

interface IProps {
    percent?: any,
    title: string,
    finishCounter?: number,
    type?: string,
    date: number,
    children: any,
    haveReadCounter?: number
}

class UserStatsCard extends React.Component<IProps, any>{

    render() {
        let percentColor = ''
        if (this.props.percent >= 0 && this.props.percent <= 60) {
            percentColor = 'red'
        } else if (this.props.percent >= 61 && this.props.percent <= 80) {
            percentColor = 'orange'
        } else {
            percentColor = 'green'
        }
        return (
            <Card className="UserStatsCard">
                <div className="first-section">
                    <div className="first-section-title">
                        {this.props.title}
                    </div>
                    <div className="info">
                        <div className="items">
                            <div className="items-title">
                                {this.props.type != 'articles' ? "Пройдено" : 'Прочитано'}
                            </div>
                            <div className="items-value">
                                {this.props.type != 'articles' ? ((this.props.finishCounter || 0) + ' раз(а)') : this.props.haveReadCounter || 0}
                            </div>
                        </div>
                        {this.props.type != 'articles' && <div className="items">
                            <div className="items-title">
                                Корректность
                            </div>
                            <div className={"items-value " + percentColor}>
                                {isNaN(this.props.percent) ? 0 : this.props.percent}%
                            </div>
                        </div>}
                        <div className="items">
                            <div className="items-title">
                                Активность
                            </div>
                            <div className="items-value">
                                {getDateString(new Date(+this.props.date)) || 'Нет данных'}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="second-section">
                    <div className="aсhive-title">
                        Награды
                    </div>
                    <div className="achive-items">
                        {this.props.children}
                    </div>
                </div>
            </Card>
        )
    }
}

export default UserStatsCard