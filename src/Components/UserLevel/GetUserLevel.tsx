import React from 'react'
import { connect } from '../../Store';
import { Store } from '../../interface';

interface IProps {
    store: Store
}

class GetUserLevel extends React.Component<IProps, any>{

    userLevel = () => {
        if (!this.props.store.userInfo) {
            return 'Loading'
        }
        const levels = [
            'Beginner', 'Pre-intermediate', 'Intermediate', 'Upper-intermediate', 'Advanced'
        ]
        return levels[this.props.store.userInfo.choosenLevel] || 'beginner'
    }

    render(){
        return this.userLevel()
    }
}

export default connect('_ALL')(GetUserLevel)