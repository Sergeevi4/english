import * as React from 'react';
import { Link } from 'react-router-dom';
import { Store } from '../../interface';
import { connect } from "../../Store";
import cn from 'classnames'

import Button from '@material-ui/core/Button';
import { API } from '../../API';
import Helmet from 'react-helmet';

import { ReactComponent as CheckIcon } from '../icons/CheckIconTrain.svg'

import styles from './UserLevel.module.scss'

interface IProps {
  store: Store
}

class UserLevel extends React.Component<IProps, {}> {

  state = { index: -1 }

  addDataToDB = () => {
    API.myUser().update({ choosenLevel: this.state.index })
  }

  chooseLevel = (index) => () => {
    this.setState({
      index
    })
  }

  renderLevels = () => {
    const levels = [
      'Beginner', 'Pre-intermediate', 'Intermediate', 'Upper-intermediate', 'Advanced'
    ]
    return levels.map((level, index) => {
      const checkStyleForButton = this.state.index !== index
      return <Button
        variant="contained"
        key={index}
        className={cn(((this.state.index != -1 && checkStyleForButton) ? styles.UserLevelButtonWithOpacity : ''), styles.UserLevelButton)}
        onClick={this.chooseLevel(index)}>
          {level}
          {this.state.index === index && <div className={styles.buttonCheckIconWrapper}><CheckIcon className={styles.buttonCheckIcon}/></div>}
        </Button>
    })
  }

  render() {
    return (
      <div className={styles.UserLevel}>
        <div className={styles.UserLevelText}>
          <div className={styles.UserLevelTextTop}>Добро пожаловать, друг!</div>
          <div className={styles.UserLevelTextBottom}>Выбери свой уровень английского, это поможет настроить наиболее эффективный для тебя курс</div>
        </div>
        <div className={styles.levelsWrapper}>
          {this.renderLevels()}
        </div>
        <Link to={'/dashboard'} className={this.state.index === -1 ? styles.UserLevelNextButtonHidden : ''}>
          <Button variant="contained" className={styles.UserLevelNextButton} component="span" onClick={this.addDataToDB}>
            Продолжить
          </Button>
        </Link>
        <Helmet>
          <body className="footer-none header-noneBackground" />
        </Helmet>
      </div>
    );
  }
}

export default connect("user")(UserLevel);
