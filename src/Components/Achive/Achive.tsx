import React from 'react'
import ReactTooltip from 'react-tooltip';
import _ from 'lodash'

import { ReactComponent as Achive5 } from '../icons/achive5.svg'
import { ReactComponent as Achive4 } from '../icons/achive4.svg'
import { ReactComponent as Achive3 } from '../icons/achive3.svg'
import { ReactComponent as Achive2 } from '../icons/achive2.svg'
import { ReactComponent as Achive1 } from '../icons/achive1.svg'
import { ReactComponent as AchiveBlocked } from '../icons/achiveBlocked.svg'

const AchiveComponents = [Achive1, Achive2, Achive3, Achive4, Achive5]

export const allAchives = {
    articles: {
        calc: (stats) => _.uniqBy(_.filter(stats, ({timeForRead}) => timeForRead > 20000), 'id').length,
        checkPoints: [50, 20, 10, 5, 1],
        tooltipText: ({ from, to }) => `Прочитано ${from} из ${to}`
    },
    trainsMistake:{
        calc: (stats) => _.filter(stats, (train) => {
            return train.inCorrectAnswers == 0
        }).length, 
        checkPoints: [50, 20, 10, 5, 1],
        tooltipText: ({ from, to }) => `Без ошибок ${from} из ${to}`
    },
    trains: {
        calc: (stats) => _.map(stats).length, 
        checkPoints: [50, 20, 10, 5, 1],
        tooltipText: ({ from, to }) => `Пройдено ${from} из ${to}`
    }
}

interface IProps {
    userData: {},
    achiveData: {
        checkPoints: Array<Number>,
        calc: any,
        tooltipText: any
    },
    id?: string
}

class Achive extends React.Component<IProps, any>{
    
    render() {
        const { userData, achiveData } = this.props
        const count = achiveData.calc(userData)
        const nextLevel = _.findLast(achiveData.checkPoints, (point) => point > count)
        const tooltip = achiveData.tooltipText({ from: count, to: nextLevel })
        const AchiveComponent = _.find(AchiveComponents, (c, index) => achiveData.checkPoints[index] <= count) || AchiveBlocked
        return (
            <div data-tip={tooltip} data-for={this.props.id}>
                <ReactTooltip id={this.props.id} effect="solid" />
                <AchiveComponent className="achivements-icon"/>
            </div>
        )
    }
}

export default Achive