import React from 'react';
import { Dialog, Card } from '@material-ui/core';
import { LinkButton } from '../UI/LinkButton';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import Alert from 'react-s-alert'
import { useStore, getEnergyInfo, useIntervalRerender } from '../../Utilities';

import { ReactComponent as CopyIcon } from '../icons/copy-content.svg'
import { ReactComponent as EnegryIcon } from '../icons/energyIcon.svg'

import styles from './LockPopup.module.scss'

export const LockPopup = (props) => {

    const { formattedTimeToRestoreOneEnergy } = getEnergyInfo();
    useIntervalRerender();
    const store = useStore()
    return <Dialog open={props.open} className={styles.dialog}>
        <div className={styles.helpClass}>
            <div className={styles.timeBlock}>
                <EnegryIcon className={styles.enegryIcon}/>
                <Card className={styles.card}>{formattedTimeToRestoreOneEnergy}</Card>
            </div>
            <div className={styles.firstText}>
                Только не это! Твоя энергия на сегодня закончилась :(
            </div>
            <div className={styles.secondText}>Не расстраивайся, ты можешь подключить Премиум</div>
            <div className={styles.thirdText}>Или пригласить друга по ссылке и получить бесплатный премиум на 7 дней</div>
            <div className={styles.inviteBlock}>
                <div className={styles.inviteText}>
                    clearenglish.io/invite/{store.user.uid}
                </div>
                <CopyToClipboard text={'clearenglish.io/invite/' + store.user.uid}
                        onCopy={() => {
                            Alert.success('Скопировано', {
                                position: 'bottom',
                                effect: 'scale',
                                timeout: 3000
                            })
                        }}>
                        <CopyIcon  className={styles.copyIcon}/>
                    </CopyToClipboard>
            </div>
            <LinkButton to="/dashboard/account/payment" className={styles.premiumButton}>Премиум</LinkButton>
            <div onClick={props.onClose} className={styles.bottomText}>Не сейчас</div>
            <div onClick={props.onClose} className={styles.crossButton}>✕</div>
        </div>
    </Dialog>
}
