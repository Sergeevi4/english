import React from 'react';
import { ReactComponent as Icon404 } from '../icons/404icon.svg'
import { Button } from '@material-ui/core';
import Helmet from 'react-helmet';

import styles from './Page404.module.scss'
import { Link } from 'react-router-dom';

export const Page404 = () => {
    return (
        <div className={styles.notFoundPage}>
            <div className={styles.wrapper404}>
                <Icon404 className={styles.icon404} />
                <div className={styles.text404}>
                    Упс... Страница, которую вы ищете, не найдена :(
                </div>
            </div>
            <Link to={'/'}><Button classes={{ label: styles.button404, root: styles.button404Back }} variant="contained">Вернуться домой</Button></Link>
            <Helmet>
                <body className="footer-none header-none" />
            </Helmet>
        </div>
    )
}