import React, { useState } from 'react';
import './Spinner.scss';

export default class Spinner extends React.Component {
    render() {
        return (
            <div className="lds-css ng-scope">
                <div className="lds-eclipse"><div></div></div>
            </div>
        );
    }
}
