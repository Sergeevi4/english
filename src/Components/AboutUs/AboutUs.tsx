import React from 'react'

import styles from './AboutUs.module.scss'
import Helmet from 'react-helmet';

export const AboutUs = () => {
    return(
        <div className={styles.AboutUs}>
            <div className={styles.AboutUsWrapper}>
                <div className={styles.title}>
                    Кто мы?
                </div>
                <div className={styles.value}>
                    ClearEnglish.io - это платформа для тех, кто хочет выучить или усовершенствовать свой английский!
                    Наша цель - говорить о сложных вещах максимально просто и превращать процесс изучения английского в удовольствие. Мы подготовили для наших пользователей ряд обучающих материалов, база которых постоянно пополняется: статьи, слова, подборки книг и фильмов на английском языке, которые помогают закрепить пройденный материал и выучить новый.
                    Кроме того, ClearEnglish - это интерактивная платформа, на которой можно проходить тренировки своих знаний: распознавать тексты на слух, переводить слова и делать это на время. Все, что нужно для обучения на одном сайте!
                    Мы будем рады, если Вы поделитесь с нами Вашими идеями или замечаниями, используя контакты, описанные ниже.
                </div>
                <div className={styles.title}>
                    Контакты
                </div>
                <div className={styles.value}>
                    E-mail: clearenglishio@gmail.com
                </div>
                <div className={styles.value}>
                    Telegram / Viber: +380636468436
                </div>
            </div>
            <Helmet>
                <link rel="canonical" href="https://clearenglish.io/about-us" />
            </Helmet>
        </div>
    )
}