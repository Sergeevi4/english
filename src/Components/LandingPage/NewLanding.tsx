import React from 'react'

import styles from './NewLanding.module.scss'
import Helmet from 'react-helmet';
import cn from 'classnames'
import AuthComponent from '../Auth/AuthComponent';
import { LinkButton } from '../UI/LinkButton';
import { Subscription } from '../Subscription/Subscription';

import { ReactComponent as EnergyIcon } from '../icons/energyIcon.svg'

export const NewLanding = () => {
    return (
        <div className={styles.newLandingPage}>
            <div className={cn(styles.registrationBlock, 'global pl pr')}>
                <div>
                    <h1 className={styles.introTextTitle}>
                        Изучай английский с ClearEnglish
                    </h1>
                    <div className={styles.introText}>
                        Повысь свой уровень английского с обучающими статьями и практическими тренировками от ClearEnglish
                    </div>
                </div>
                <div className={styles.authSection}>
                    <div className={styles.freeSignUpText}>
                        Бесплатная регистрация
                    </div>
                    <div className={styles.AuthBlock}>
                        <AuthComponent type={'landing'} match={{ params: { type: 'signup' } }} />
                    </div>
                </div>
            </div>
            <div className={styles.trainingsBlock}>
                <div className={styles.trainingSvg} />
                <div className={styles.sectionDescription}>
                    <div className={styles.descriptionBlueTitle}>
                        Практические тренировки
                    </div>
                    <div className={styles.descriptionBlueText}>
                        Оттачивай свои знания используя индивидуальные практические тренировки
                    </div>
                    <LinkButton to={'/signup'} className={styles.descriptionBlueButton}>Начать изучение</LinkButton>
                </div>
            </div>
            <div className={styles.articlesBlock}>
                <div className={styles.sectionDescriptionWithoutBack}>
                    <div className={styles.descriptionWhiteTitle}>
                        Обучающие статьи
                    </div>
                    <div className={styles.descriptionWhiteText}>
                        Изучай грамматику начального и продвинутого уровня с интересными обучающими статьями.
                    </div>
                    <LinkButton to={'/signup'} className={styles.descriptionWhiteButton}>Начать изучение</LinkButton>
                </div>
                <div className={styles.articlesSvg} />
            </div>
            <div className={styles.packsBlock}>
                <div className={styles.packsSvg} />
                <div className={styles.sectionDescription}>
                    <div className={styles.descriptionBlueTitle}>
                        Наборы слов
                    </div>
                    <div className={styles.descriptionBlueText}>
                        Выбирай из множества уже готовых наборов слов, таких как Dota 2, CS:GO, CSS + HTML и подборок самых популярный слов
                    </div>
                    <LinkButton to={'/signup'} className={styles.descriptionBlueButton}>Начать изучение</LinkButton>
                </div>
            </div>
            <div className={styles.progressBlock}>
                <div className={styles.sectionDescriptionWithoutBack}>
                    <div className={styles.descriptionWhiteTitle}>
                        Контроль результатов
                    </div>
                    <div className={styles.descriptionWhiteText}>
                        Отслеживай свой прогресс в личном кабинете мотивируя себя наградами
                    </div>
                    <LinkButton to={'/signup'} className={styles.descriptionWhiteButton}>Начать изучение</LinkButton>
                </div>
                <div className={styles.progressSvg} />
            </div>
            <div className={styles.booksBlock}>
                <div className={styles.booksSvg} />
                <div className={styles.sectionDescription}>
                    <div className={styles.descriptionBlueTitle}>
                        Книги
                    </div>
                    <div className={styles.descriptionBlueText}>
                        Слушай и читай свои любимые книги на английском языке изучая новые слова
                    </div>
                    <LinkButton to={'/signup'} className={styles.descriptionBlueButton}>Начать изучение</LinkButton>
                </div>
            </div>
            <div className={styles.moviesBlock}>
                <div className={styles.sectionDescriptionWithoutBack}>
                    <div className={styles.descriptionWhiteTitle}>
                        Фильмы
                    </div>
                    <div className={styles.descriptionWhiteText}>
                        Смотри фильмы с интерактивнми субтитрами управляя плеером с телефона
                    </div>
                    <LinkButton to={'/signup'} className={styles.descriptionWhiteButton}>Начать изучение</LinkButton>
                </div>
                <div className={styles.moviesSvg} />
            </div>
            <div className={styles.englishForBlock}>
                <div className={styles.englishForTop}>
                    <EnergyIcon className={styles.landingEnerhyIcon}/>
                    <div className={styles.englishForTitle}>
                        Зачем нужно учить английский?
                    </div>
                </div>
                <div className={styles.englishForBottom}>
                    <div>
                        Новые знакомства
                    </div>
                    <div>
                        Путешествия по всему миру
                    </div>
                    <div>
                        Рост заработной платы
                    </div>
                </div>
            </div>
            <div className={styles.paymentBlock}>
                <div className={styles.subText}>
                    Ускорь изучение английского языка вместе с нашими тарифными планами
                </div>
                <div className={styles.subParent}>
                    <Subscription type={'landingSub'} />
                </div>
            </div>
            <Helmet>
                <body className="header-none" />
                <title>ClearEnglish - Результативное изучение английского</title>
                <meta name="description" content="Английский бесплатно онлайн! Учить английский язык еще не было так приятно и легко, как с ClearEnglish, присоединяйся скорее!"></meta>
                <link rel="canonical" href="https://clearenglish.io" />
            </Helmet>
        </div>
    )
}