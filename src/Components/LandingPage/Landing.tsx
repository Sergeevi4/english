import React from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import _ from 'lodash'

import { ReactComponent as LandingTop } from '../icons/landing-top.svg'
import { ReactComponent as LandingMid } from '../icons/landing-mid.svg'
import { ReactComponent as LandingBottom } from '../icons/landing-bottom.svg'
import { ReactComponent as Check } from '../icons/landing-check.svg'

import styles from './Landing.module.scss'
import { Button } from '@material-ui/core';
import AuthComponent from '../Auth/AuthComponent';
import { absoluteToFixed } from '../../Utilities';
import {Subscription} from '../Subscription/Subscription';
import cn from 'classnames'

const ButtonWithLink = (props: any) => {
    const MyLink = propsInner => <Link to={props.to} {...propsInner} />
    return <Button component={MyLink} {...props}>{props.children}</Button>
}

class Landing extends React.Component {

    state = {
        shakeAuth: false
    }

    el = null

    componentDidMount(){
        absoluteToFixed({minTop: 0, minBottom: 900, defaultFixedTop: 120, element: this.el})
    }

    renderMidSectionDesktop = () => {
        const data = [
            {title: 'Практические тренировки', value: 'Оттачивай свои знания используя индивидуальные практические тренировки'},
            {title: 'Обучающие статьи', value: 'Изучай грамматику начального и продвинутого уровня с интересными обучающими статьями', isLink: true},
            {title: "Контроль результатов", value: 'Отслеживай свой прогресс в личном кабинете мотивируя себя наградами'}
        ]

        return _.map(data, ({title, value, isLink}) => {
            return <div className={styles.itemsWrapper} key={title}>
                {isLink 
                    ? <Link to="/articles" className={styles.itemTitle}>{title}</Link>
                    : <div className={styles.itemTitle} onClick={this.onAuthShake}>{title}</div>}
                <div className={styles.itemValue}>
                    {value}
                </div>
            </div>
        })
    }

    onAuthShake = () => {
        this.setState({shakeAuth: true})
        setTimeout(() => {
            this.setState({shakeAuth: false})
        }, 1000)
    }

    render() {
        return (
            <div className={styles.landingPage}>
                <div className={cn(styles.authText, 'global ar')}>Бесплатная регистрация</div>
                <AuthComponent myRef={(el) => this.el = el} shake={this.state.shakeAuth} type={'landing'} match={{params: {type: 'signup'}}}/>
                <div className={cn(styles.wrapper, 'global pl pr')}>
                    <LandingTop className={styles.svg} />
                    <div className={styles.text}>
                        <h1 className={styles.title}>
                            Изучай английский с ClearEnglish
                        </h1>
                        <div className={cn(styles.description, 'global pl pr')}>
                            Повысь свой уровень английского с обучающими статьями и практическими тренировками от ClearEnglish
                        </div>
                        <ButtonWithLink to={'/signup'} variant="contained" className={styles.signup} {...{fortest: 'landing-mobile-signup-link'}}>
                            Начать изучение бесплатно
                        </ButtonWithLink>
                        <Link to={'/login'} className={styles.login}>
                            Уже есть аккаунт
                        </Link>
                    </div>
                </div>
                <div className={cn(styles.midSectionDesktop, 'global pl')}>
                    {this.renderMidSectionDesktop()}
                </div>
                <div className={styles.secondBlock}>
                    <div className={styles.secondBLockTitle}>
                        Практические тренировки
                    </div>
                    <div className={styles.secondBlockDescription}>
                        Оттачивай свои знания используя индивидуальные практические тренировки
                    </div>
                </div>
                <div className={styles.midSection}>
                    <LandingMid className={styles.midSvg} />
                    <div className={styles.midSectionWrapper}>
                        <Link to="/articles" className={styles.midSectionTitle}>
                            Обучающие статьи
                        </Link>
                        <div className={styles.midSectionDescription}>
                            Изучай грамматику начального и продвинутого уровня с интересными обучающими статьями
                        </div>
                    </div>
                </div>
                <div className={styles.secondBlock}>
                    <div className={styles.secondBLockTitle}>
                        Контроль результатов
                    </div>
                    <div className={styles.secondBlockDescription}>
                        Отслеживай свой прогресс в личном кабинете мотивируя себя наградами
                    </div>
                </div>
                <div className={styles.botSection}>
                    <div className={styles.childForBack} />
                    <LandingBottom className={styles.botSvg} />
                    <div className={cn(styles.botSectionWrapper, 'global pl')}>
                        <div className={styles.botSectionTitle}>
                            Зачем учить английский?
                        </div>
                        <div className={styles.botSectionDescription}>
                            <div className={styles.iconWrapper}>
                                <Check className={styles.checkIcon} /> Путешествия по всему миру
                            </div>
                            <div className={styles.iconWrapper}>
                                <Check className={styles.checkIcon} /> Новые знакомства
                            </div>
                            <div className={styles.iconWrapper}>
                                <Check className={styles.checkIcon} /> Рост заработной платы
                            </div>
                        </div>
                    </div>
                </div>
                <div className={cn(styles.BottomWrapperDesktop, 'global pl')}>Начинай увлекательное изучение прямо сейчас!</div>
                <div className={cn(styles.BottomWrapper, 'global pl pr')}>
                    <ButtonWithLink to={'/signup'} variant="contained" className={styles.BottomSignup}>
                        Начать изучение бесплатно
                    </ButtonWithLink>
                    <Link to={'/login'} className={styles.BottomLogin}>
                        Уже есть аккаунт
                    </Link>
                </div>
                <div className={styles.subDesktop + " " + styles.landingSubWrapper}>
                    <div className={styles.subText}>
                        Ускорь изучение английского языка вместе с нашими тарифными планами
                    </div>
                    <div className={styles.subParent}>
                        <Subscription type={'landingSub'}/>
                    </div>
                </div>
                <Helmet>
                    <body className="header-none footerWithoutBakground" />
                    <title>ClearEnglish - Результативное изучение английского</title>
                    <meta name="description" content="Английский бесплатно онлайн! Учить английский язык еще не было так приятно и легко, как с ClearEnglish, присоединяйся скорее!"></meta>
                    <link rel="canonical" href="https://clearenglish.io" />
                </Helmet>
            </div>
        )
    }
}

export default Landing