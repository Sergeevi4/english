import React, {useState} from 'react'
import _ from 'lodash';
import cn from 'classnames';
import styles from './QuestionInArticle.module.scss'
import { Button } from '@material-ui/core'
import { ReactComponent as CorrectAnswerIcon } from '../icons/trainInArticleCorrect.svg'
import { ReactComponent as IncorrectAnswerIcon } from '../icons/trainInArticleWrong.svg'
import { LinkButton } from '../UI/LinkButton';

const correctAnswerIcon = <CorrectAnswerIcon className={styles.buttonCheckIcon} />
const incorrectAnswerIcon = <IncorrectAnswerIcon className={styles.buttonCheckIcon}/>
const questions = [
    {
        answers: [<span>I ate <strong>the whole</strong> banana</span>, <span>I ate <strong> all the</strong> banana</span> ],
        correctIndex: 0,
    },
    {
        answers: [<span>I ate <strong>all the</strong> pasta</span>, <span>I ate <strong>the whole</strong> pasta</span>],
        correctIndex: 0
    },
    {
        answers: [<span>I rested <strong>all the</strong> night</span>, <span>I rested <strong>all</strong> night</span>],
        correctIndex: 1
    },
    {
        answers: [<span>I spent <strong>all</strong> my life learning languages </span>, <span>I spent my <strong>entire</strong> life learning languages</span>],
        correctIndex: 1
    }
]

export const QuestionInArticle = () => {

    const [questionIndex, changeQuestionIndex] = useState(0)
    const [userSelectIndex, changeUserSelectIndex] = useState(-1)

    const chooseQuestion = (index) => () => {
        changeUserSelectIndex(index)
    }

    const changeQuestion = () => {
        changeQuestionIndex(questionIndex + 1),
        changeUserSelectIndex(-1)
    }

    const renderFinishTrainScreen = () => {
        return <div className={styles.finishTrainBlock}>
            <div className={styles.finishTrainTitle}>Молодец! У тебя отличные результаты</div>
            <div className={styles.finishTrainText}>Зарегистрируйся и получи доступ к множеству тренировок и статьям</div>
            <LinkButton className={styles.signUpButton} to={'/login'}>Начать изучение</LinkButton>
        </div> 
    }

    const renderTrainQuestions = () => {
        const stylesForNextButton = cn(styles.nextButton, {[styles.activeNextButton]: userSelectIndex != -1})        
        return <>
            <div className={styles.trainTitle}>
                Проверь себя
            </div>
            <div className={styles.trainSubTitle}>
                Выбери правильный вариант
            </div> {_.map(questions[questionIndex].answers, (answer, index) => {
                const isButtonSelectedByUser = userSelectIndex === index
                const isThisButtonForCorrectAnswer = index === questions[questionIndex].correctIndex
                return <Button
                    variant="contained"
                    onClick={chooseQuestion(index)}
                    className={cn(styles.answerButton, {[styles.unselectedAnswerButton]: userSelectIndex != -1 && !isButtonSelectedByUser})}
                >
                    {answer}
                    {userSelectIndex != -1 ? (isThisButtonForCorrectAnswer ? correctAnswerIcon : incorrectAnswerIcon) : null}
                </Button>
            })}
            <Button variant='contained' className={stylesForNextButton} onClick={changeQuestion}>Дальше</Button>
        </>
    }

    return <div className={styles.trainCard}>
        {questionIndex === questions.length ? renderFinishTrainScreen() : renderTrainQuestions()}
    </div>
}