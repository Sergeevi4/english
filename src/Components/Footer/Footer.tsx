import React from 'react'
import { Link } from 'react-router-dom';
import cn from 'classnames'

import styles from './Footer.module.scss'
import MastercardSrc  from '../icons/mastercardIcon.svg'
import VisaSrc from '../icons/visaIcon.svg'

const Footer = () => {
    return(
        <div className={cn(styles.footer, 'global pl pr')}>
            <div className={styles.footerLeftWrapper}>
                <Link to={'/privacy-policy'} className={styles.policy}>
                    Privacy Policy
                </Link>
                <Link to={'/about-us'} className={styles.email}>
                    О компании
                </Link>
                <div className={styles.copywrite}>
                    &#169; Copyright 2018-2019 ClearEnglish.io
                </div>
            </div>
            <div className={styles.footerRightWrapper}>
                <img src={MastercardSrc} alt="Master card" className={styles.mastercardIcon}/>
                <img src={VisaSrc} alt="Visa" className={styles.visaIcon}/>
            </div>
        </div>
    )
}
export default Footer