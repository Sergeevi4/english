import * as React from 'react';
import {auth} from 'firebase/app';
import Alert from 'react-s-alert'
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import { Theme, Checkbox, FormControlLabel, TextField, Dialog } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';

import { ReactComponent as FaceBook } from '../icons/faceBookIcon.svg'
import { ReactComponent as GoogleIcon } from '../icons/googleIcon.svg'

import 'react-s-alert/dist/s-alert-default.css'
import 'react-s-alert/dist/s-alert-css-effects/scale.css'
import style from './AuthComponent.module.scss'
import { ScrollToTopOnMount } from '../RouteHelpers/ScrollToTop';

const styles = (theme: Theme) => ({
    authButton: {
        background: 'linear-gradient(118deg, #35b6e8 0%, #5b86e5 100%)',
        width: "190px",
        '&:hover': {
            boxShadow: "0 0 10px rgba(0,0,0,0.5)",
        }
    },
    bottomButton: {
        background: 'linear-gradient(118deg, #35b6e8 0%, #5b86e5 100%)',
        color: "white",
        '&:hover': {
            boxShadow: "0 0 10px rgba(0,0,0,0.5)",
        }
    },
    hideNameInput: {
        display: "none"
    },
});

interface IState {
    email: string,
    password: string,
    name: string,
    showPassword: boolean,
    checkedPrivacy: boolean,
    isAuthing: boolean
}

class Auth extends React.Component<any, IState> {
    state = {
        email: '',
        password: '',
        name: '',
        showPassword: false,
        checkedPrivacy: false,
        isAuthing: false
    }

    loginWithGoogle = () => {
        let provider = new auth.GoogleAuthProvider();
        auth().signInWithPopup(provider)
        this.setState({isAuthing: true})
    }

    loginWithFacebook = () => {
        let provider = new auth.FacebookAuthProvider();
        auth().signInWithPopup(provider)
        this.setState({isAuthing: true})
    }

    // handleChange = <T extends keyof IState>(prop: T) => event => {
    //     // this.setState({ [prop]: '5' as any as boolean | string } );
    //     this.setState<T>({ [prop]: event.target.value } );
    // };

    handleChange = (prop: any) => (event: any) => {
        // this.setState({ [prop]: '5' as any as boolean | string } );
        (this as any).setState({ [prop]: event.target.value } );
    };

    handleClickShowPassword = () => {
        this.setState(state => ({ showPassword: !state.showPassword }));
    };

    emailSignIn = (email, password) => {
        return auth().signInWithEmailAndPassword(email, password)
    }

    emailSignUp = async (email, password) => {
        // Before doing signup we try login with login/pass
        // We need this because we have seen in hotjar (analitycs) that
        // some user try login thru signup form.
        
        try {
            const loginResult = await this.emailSignIn(email, password)
            if (loginResult) return loginResult
        } catch {
            if (!this.state.name) {
                return Promise.reject({code: 'authFront/invalid-name', message: "Invalid name"})
            }else {
                return auth().createUserWithEmailAndPassword(email, password).then(() => {
                    auth().currentUser.updateProfile({
                        displayName: this.state.name
                    })
                })
            }
        }
    }

    loginSubmit = () => {
        const { email, password } = this.state
        const authAction = this.props.authType
            ? this.emailSignIn(email, password)
            : this.emailSignUp(email, password)
            
        authAction.catch((error) => {
            console.error(error)
            const messageMap = {
                'auth/invalid-email': 'Вы ввели некоректный email',
                'auth/wrong-password': 'Логин или пароль неправильный',
                'authFront/invalid-name': 'Введите имя',
                'auth/weak-password': 'Пароль должен быть 6 или больше символов',
                'auth/email-already-in-use': 'Этот email уже используется'
            }
            let message = messageMap[error.code]
            if (!message) {
                message = `Что-то пошло не так, [${error.message}]`
            }
            Alert.error(message, {
                position: 'bottom',
                effect: 'scale',
                timeout: 3500
            })
        })
    }

    onAuthmodeChange = () => {
        this.setState({
            email: '',
            password: '', // TODO Is it good UX????
        })
    }

    MyLink = (props) => {
        return <Link to={this.props.authType ? '/signup' : '/login'} {...props} />
    }

    render() {
        const { classes } = this.props;
        return (
            <div ref={this.props.myRef} className={style.rightSide + " global ar " + (this.props.shake ? ' ' + style.shake : '') + (this.props.type === 'landing' ? (' ' + classNames(style.landing)) : '')}>
                <Dialog open={this.state.isAuthing} onClose={() => this.setState({isAuthing: false})} classes={{paper: style.authLoading}}>Авторизируем...</Dialog>
                <ScrollToTopOnMount />
                <div className={style.inputs}>
                    <TextField
                        name="email"
                        label="E-mail"
                        className={classes.input}
                        inputProps={{
                            'aria-label': 'Description',
                        }}
                        onChange={this.handleChange('email')}
                        value={this.state.email}
                    />
                    <FormControl className={classNames(classes.margin, classes.textField)}>
                        <InputLabel htmlFor="adornment-password">Password</InputLabel>
                        <Input
                            name="password"
                            id="adornment-password"
                            type={this.state.showPassword ? 'text' : 'password'}
                            value={this.state.password}
                            onChange={this.handleChange('password')}
                            endAdornment={
                                <InputAdornment position="end" className="input__eye">
                                    <IconButton
                                        aria-label="Toggle password visibility"
                                        onClick={this.handleClickShowPassword}
                                    >
                                        {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                    <TextField
                        name="name"
                        label="Имя"
                        required
                        value={this.state.name}
                        onChange={this.handleChange('name')}
                        className={classNames(classes.input, (this.props.authType ? classes.hideNameInput : ''))}
                        inputProps={{
                            'aria-label': 'Description',
                        }}
                    />
                </div>
                
                    {this.props.authType 
                        ? <Link to={"/reset-password"}><div className={style.forgotPassword}>Забыли пароль?</div></Link>
                        : <div className={style.privacyPolicyWrapper}>
                            Регистрируясь Вы соглашаетесь с <Link to='/privacy-policy' target="_blank" >Privacy Policy </Link> 
                            <br />
                            и <Link to='/terms-and-conditions' target="_blank" >Условиями использования </Link>
                        </div>
                    }
                
                <Button
                    name="signButton"
                    {...{ fortest: "signUp or logIn" }}
                    variant="contained"
                    color="primary"
                    className={classes.authButton}
                    onClick={this.loginSubmit}
                >
                    {this.props.authType ? "Вход" : "Зарегистрироваться"}
                </Button>
                <div className={style.socialButtons}>
                    <div className={style.line} />
                    <div className={style.button}>
                        <FaceBook onClick={this.loginWithFacebook} />
                    </div>
                    <div className={style.button}>
                        <GoogleIcon onClick={this.loginWithGoogle} />
                    </div>
                    <div className={style.line} />
                </div>
                <div className={style.bottom}>
                    <div className={style.bottomText}>
                        {this.props.authType ? "Нет аккаунта?" : "Уже зарегистрированы?"}
                    </div>
                    <Button
                        component={this.MyLink}
                        variant="contained"
                        color="default"
                        className={style.bottomButton}
                        onClick={this.onAuthmodeChange}
                    >
                        {this.props.authType ? "регистрация" : "войти"}
                    </Button>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Auth);
