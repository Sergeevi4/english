import React from 'react';
import ReactDOM from 'react-dom';
import Auth  from './Auth';
import _ from 'lodash';

import { shallow } from 'enzyme';

import Enzyme, { mount } from 'enzyme';

import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() })

it('check left-side-title logIn', () => {
    const data = mount(<Auth />)
    data.setState({authModeIsLogin: true}), () => {
        const getElement = data.find('.left-side__title')
        expect(getElement.text()).toEqual('Вход')
    }
})

it('check left-side-title signUp', () => {
    const data = mount(<Auth />)
    data.setState({authModeIsLogin: false}), () => {
        data.update()
        const getElement = data.find('.left-side__title')
        expect(getElement.text()).toEqual('Регистрация')
    }
})

it('check text under inputs logIn', () => {
    const data = mount(<Auth />)
    data.setState({authModeIsLogin: true}), () => {
        data.update()
        const getElement = data.find('.right-side__checkbox')
        expect(getElement.text()).toEqual('Забыли пароль?')
    }
})

it('check text under inputs signUp', () => {
    const data = mount(<Auth />)
    data.setState({authModeIsLogin: false}), () => {
        data.update()
        const getElement = data.find('.right-side__checkbox')
        expect(getElement.text()).toEqual('blabla agree with bla bla')
    }
})

it('check text in enter button logIn', () => {
    const data = mount(<Auth />)
    data.setState({authModeIsLogin: false})
        data.update()
        const getElement = data.find({fortest: "signUp or logIn"})
        console.log("text = ", getElement.debug())
        expect(getElement.text()).toEqual('Вход')
})
