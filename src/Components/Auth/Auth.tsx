import * as React from 'react';
import { Link } from 'react-router-dom';
import AuthComponent from './AuthComponent';

import { ReactComponent as Logo } from '../icons/Logo.svg'

import 'react-s-alert/dist/s-alert-default.css'
import 'react-s-alert/dist/s-alert-css-effects/scale.css'
import style from "./Auth.module.scss"
import { UserComponent } from '../Header/User';
import { API } from '../../API';
import Helmet from 'react-helmet';


const YouAreInvited = ({ uid }: { uid: string }) => {
  // const userInfo = API.user(uid).useOn();
  return <div className={style.invite}>
    Вы были приглашены на этот сайт другим юзером. <br />
    {/* {userInfo && <UserComponent userInfo={userInfo} />} */}
    Из-за этого после регистрации вы получите премиум на неделю
  </div>
}

class Auth extends React.Component<any, any> {

  componentDidMount() {
    if (this.isInvite()) {
      localStorage.setItem('inviteUid', this.props.match.params.uid)
    }
  }

  isLogin = () => {
    return this.props.match.params.type === 'login'
  }

  isInvite = () => this.props.match.params.type === 'invite'

  MyLink = (props) => {
    return <Link to={this.isLogin() ? '/signup' : '/login'} {...props} />
  }

  render() {
    return (
      <div className={style.Login}>
        {this.isInvite() && <YouAreInvited uid={this.props.match.params.uid} />}
        <div className={style.form} {...{ fortest: 'login-page-form' }}>
          <div className={style.leftSide}>
            <div className={style.logo}>
              <Logo className={style.logoImage} />
              <div className={style.logoText}> Clear English </div>
            </div>
            <div className={style.wrapper}>
              <div className={style.title}>
                {this.isLogin() ? "Вход" : "Регистрация"}
              </div>
              <div className={style.text}>
                Повысь свой уровень английского с обучающими статьями и практическими тренировками перевода с английского на русский от ClearEnglish.
              </div>
            </div>
          </div>
          <AuthComponent authType={this.isLogin()} />
        </div>
        <Helmet>
          <body className="footer-none" />
        </Helmet>
      </div>
    );
  }
}

export default Auth;
