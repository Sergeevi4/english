import { database, auth } from 'firebase/app';
import 'firebase/database';
import { ObjectOf } from "./Utilities";
import { useState, useEffect } from 'react';

export type UserWord = {
    added: boolean;
    addedAt?: number;
    addedFrom?: {
        type: 'BOOK'; // Add other type in future
        id: string; // For example book id
    }
    progress?: ObjectOf<{
        results: Array<{date: number, isAnswerCorrect: boolean}>,
        spacedRepetitionGroup: number
    }>
}

type haveSeenArticle = {
    time: number
}

export type trainStat = {
    correctAnswers: number
    date: number
    inCorrectAnswers: number
    timeForPass: number
}

type visitStat = {
    from: number
    to: number
}

export interface User {
    haveSeenArticles: ObjectOf<haveSeenArticle>;
    online: boolean;
    packs: ObjectOf<boolean>;
    booksPreview: ObjectOf<any>;
    payment: {
        status: string
    };
    choosenLevel: number;
    stats: ObjectOf<ObjectOf<trainStat> | ObjectOf<visitStat>>;
    uid: string;
    userWords: ObjectOf<UserWord>;
    photoURL: string;
    email: string;
    displayName: string;
    invitedByMe: ObjectOf<{uid: string, date: number}>;
    trainPacksSetting: Array<{label: string, value: string}>;
}

interface DataBase {
    users: ObjectOf<User>
}

type articlePreview = {
    author: string;
    date: number;
    preview: string;
    tags: string[];
    title: string;
    url: string;
}

type articlesPreview = ObjectOf<articlePreview>

type comingSoon = ObjectOf<ObjectOf<{ email: string, displayName: string }>>

type packs = ObjectOf<{ a }>

type book = any;
type books = ObjectOf<book>
type booksPreview = ObjectOf<any>
type bookPreview = ObjectOf<any>;

type word = any

type article = {
    author: string,
    date: number,
    description: string,
    preview: string,
    text: string,
    title: string,
    url: string
}

const wrapFirebase = <T>(ref: firebase.database.Reference) => {
    return {
        on: (callback: (u: Partial<T>) => void) => {
            return ref.on('value', snapshot => {
                callback(snapshot.val())
            })
        },
        once: (callback?: (u: T) => void): Promise<T> => {
            return callback
                ? ref.once('value', snapshot => callback(snapshot.val())) as any
                : ref.once('value').then(snapshot => snapshot.val()) as any
        },
        set: (newVal: Partial<T>) => ref.set(newVal),
        update: (newVal: Partial<T>) => ref.update(newVal),
        onDisconnect: () => {
            return {
                set: (newVal: Partial<T>) => ref.onDisconnect().set(newVal),
                update: (newVal: Partial<T>) => ref.onDisconnect().update(newVal),
            }
        },
        value: ref, // for direct access
        useOn: () => {
            const [data, updateData] = useState(undefined);
            useEffect(() => {
                ref.on('value', snapshot => {
                    updateData(snapshot.val())
                })
            }, []);
            return data as T;
        },
        useOnce: () => {
            const [data, updateData] = useState(undefined);
            useEffect(() => {
                ref.once('value', snapshot => {
                    updateData(snapshot.val())
                })
            }, []);
            return data as T;
        }
    }
}

export const API = {
    user: (userId: string) => {
        const ref = database().ref('users/' + userId)
        return {
            ...wrapFirebase<User>(ref),

            haveSeenArticles: () => wrapFirebase<User["haveSeenArticles"]>(ref.child("haveSeenArticles")),
            haveSeenArticle: (articleId: string) => wrapFirebase<haveSeenArticle>(ref.child("haveSeenArticles/" + articleId)),

            stats: () => wrapFirebase<User["stats"]>(ref.child("stats")),
            stat: (statId: string) => (wrapFirebase<User["stats"]>(ref.child("stats/" + statId))),

            bookPreview: (bookId: string) => (wrapFirebase<any>(ref.child("booksPreview/" + bookId))),

            packs: () => wrapFirebase<User["packs"]>(ref.child("packs")),
            pack: (packId: string) => wrapFirebase<boolean>(ref.child(`packs/${packId}`)),
            trainPacksSetting: () =>  wrapFirebase<User["trainPacksSetting"]>(ref.child("trainPacksSetting")),

            userWords: () => wrapFirebase<User["userWords"]>(ref.child("userWords")),
        }
    },
    myUser: () => {
        return API.user(auth().currentUser.uid)
    },
    // userStas: (userId)
    users: () => wrapFirebase<DataBase["users"]>(database().ref('users')),
    books: () => wrapFirebase<books>(database().ref('books')),
    book: (bookId: string) => wrapFirebase<book>(database().ref('books/' + bookId)),
    booksPreview: () => wrapFirebase<booksPreview>(database().ref('booksPreview')),
    bookPreview: (bookId: string) => wrapFirebase<bookPreview>(database().ref('booksPreview/' + bookId)),
    article: (articleId: string) => wrapFirebase<article>(database().ref('articles/' + articleId)),
    articlePreview: () => wrapFirebase<articlesPreview>(database().ref('articlesPreview')),
    articlesSecrets: () => wrapFirebase<articlesPreview>(database().ref('articles')),
    comingSoons: () => wrapFirebase<comingSoon>(database().ref('comingSoons')),
    comingSoon: (type: string, userId: string) => wrapFirebase<{ email: string, displayName: string }>(database().ref(`comingSoons/${type}/${userId}`)),
    word: (word: string) => wrapFirebase<word>(database().ref('words/' + word)),
    words: () => wrapFirebase<ObjectOf<word>>(database().ref('words'))
}
