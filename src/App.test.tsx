import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import Header from './Components/Header/Header';
import { MemoryRouter } from 'react-router';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import puppeteer, { Page } from 'puppeteer';

Enzyme.configure({ adapter: new Adapter() })

// it('renders without crashing', () => {
//   const div = document.createElement('div');
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

// it('check user in header', () => {
//   const app = mount(<MemoryRouter><Header classes = {{header: ''}} store={{user: {}}} /></MemoryRouter>);
//   expect(app.contains(<div className="login-true">login</div>)).toBe(true);
// })

const serverURL = 'http://localhost:3000'

const testEmail = 'test-e2e@email.com';
const testPass = 'qweqwe';
const testName = 'test e2e'

export const doLogin = async (page: Page) => {
  await page.goto(serverURL + '/login');
  await page.waitFor(500);
  await page.type('[name="email"]', testEmail)
  await page.type('[name="password"]', testPass)
  await page.click('[name="signButton"]')
}

export const doSignUp = async (page: Page) => {
  await page.goto(serverURL + '/signup');
  await page.waitFor(500);
  await page.type('[name="email"]', `test-e2e-${Date.now()}@email.com`)
  await page.type('[name="password"]', testPass)
  await page.type('[name="name"]', testName)
  await page.click('[name="signButton"]')
}

describe('Google', () => {

  let page: Page = null;


  beforeAll(async () => {
    let browser = await puppeteer.launch({
      headless: true,
    });
    page = await browser.newPage();
    await page.goto(serverURL);
  });

  it('sign up flow', async () => {
    await expect(page.title()).resolves.toMatch('Учи английский просто');
    await page.waitFor(500);
    await page.click('[fortest="landing-mobile-signup-link"]');
    await page.waitFor(500);
    const signUpPageLoaded = !!(await page.$('[fortest="login-page-form"]'));
    expect(signUpPageLoaded).toBe(true);



  }, 100*1000);

  // it('check title', async () => {

  //   await expect(page.title()).resolves.toMatch('Учи английский просто');
  // });




});