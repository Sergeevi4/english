import { User } from "./API";
import { paymentSet } from "./Infos";
import { UserInfo } from "firebase";

export interface Store {
  // Yea, looks like interface, and value have vice versa naming.
  user: UserInfo,
  userInfo: User,
  paymentStatus: 'basic' | 'standard' | 'premium'
}

export interface IPropsStore {
  store: Store
}

declare global {
  interface Window { 
    PAGE_STATE: any; 
    PAGE_STATE_ARTICLE: any; 
    authWithPayment: boolean;
    infoForSeoRendered;
    googleyolo: any;
    hasSession: any;
  }
}
