// import React from 'react';
// import ReactDOM from 'react-dom';
// import Auth  from './Auth';
// import _ from 'lodash';

import { getBestFromAvailable } from './Utilities'

// import { shallow } from 'enzyme';

// import Enzyme, { mount } from 'enzyme';

// import Adapter from 'enzyme-adapter-react-16';
// import { Button } from '@material-ui/core';

// Enzyme.configure({ adapter: new Adapter() })

// it('check left-side-title logIn', () => {
//     const data = mount(<Auth />)
//     data.setState({authModeIsLogin: true}), () => {
//         const getElement = data.find('.left-side__title')
//         expect(getElement.text()).toEqual('Вход')
//     }
// })

// it('check left-side-title signUp', () => {
//     const data = mount(<Auth />)
//     data.setState({authModeIsLogin: false}), () => {
//         data.update()
//         const getElement = data.find('.left-side__title')
//         expect(getElement.text()).toEqual('Регистрация')
//     }
// })

// it('check text under inputs logIn', () => {
//     const data = mount(<Auth />)
//     data.setState({authModeIsLogin: true}), () => {
//         data.update()
//         const getElement = data.find('.right-side__checkbox')
//         expect(getElement.text()).toEqual('Забыли пароль?')
//     }
// })

// it('check text under inputs signUp', () => {
//     const data = mount(<Auth />)
//     data.setState({authModeIsLogin: false}), () => {
//         data.update()
//         const getElement = data.find('.right-side__checkbox')
//         expect(getElement.text()).toEqual('blabla agree with bla bla')
//     }
// })

it('check getBestFromAvailable', () => {
    const mockAvailableData = [
        { voiceURI: "Alex" },
        { voiceURI: "Alice" },
        { voiceURI: "Alva" },
        { voiceURI: "Amelie" },
        { voiceURI: "Anna" },
        { voiceURI: "Carmit" },
        { voiceURI: "Damayanti" },
        { voiceURI: "Daniel" },
        { voiceURI: "Diego" },
        { voiceURI: "Ellen" },
        { voiceURI: "Fiona" },
        { voiceURI: "Fred" },
        { voiceURI: "Ioana" },
        { voiceURI: "Joana" },
        { voiceURI: "Jorge" },
        { voiceURI: "Juan" },
        { voiceURI: "Kanya" },
        { voiceURI: "Karen" },
        { voiceURI: "Kyoko" },
        { voiceURI: "Laura" },
        { voiceURI: "Lekha" },
        { voiceURI: "Luca" },
        { voiceURI: "Luciana" },
        { voiceURI: "Maged" },
        { voiceURI: "Mariska" },
        { voiceURI: "Mei-Jia" },
        { voiceURI: "Melina" },
        { voiceURI: "Milena" },
        { voiceURI: "Moira" },
        { voiceURI: "Monica" },
        { voiceURI: "Nora" },
        { voiceURI: "Paulina" },
        { voiceURI: "Samantha" },
        { voiceURI: "Sara" },
        { voiceURI: "Satu" },
        { voiceURI: "Sin-ji" },
        { voiceURI: "Tessa" },
        { voiceURI: "Thomas" },
        { voiceURI: "Ting-Ting" },
        { voiceURI: "Veena" },
        { voiceURI: "Victoria" },
        { voiceURI: "Xander" },
        { voiceURI: "Yelda" },
        { voiceURI: "Yuna" },
        { voiceURI: "Yuri" },
        { voiceURI: "Zosia" },
        { voiceURI: "Zuzana" },
        { voiceURI: "Google Deutsch" },
        { voiceURI: "Google US English" },
        { voiceURI: "Google UK English Female" },
        { voiceURI: "Google UK English Male" },
        { voiceURI: "Google español" },
        { voiceURI: "Google español de Estados Unidos" },
        { voiceURI: "Google français" },
        { voiceURI: "Google हिन्दी" },
        { voiceURI: "Google Bahasa Indonesia" },
        { voiceURI: "Google italiano" },
        { voiceURI: "Google 日本語" },
        { voiceURI: "Google 한국의" },
        { voiceURI: "Google Nederlands" },
        { voiceURI: "Google polski" },
        { voiceURI: "Google português do Brasil" },
        { voiceURI: "Google русский" },
        { voiceURI: "Google 普通话（中国大陆）" },
        { voiceURI: "Google 粤語（香港）" },
        { voiceURI: "Google 國語（臺灣）" }
    ]; 
    
    const desired1 = {voiceURI: 'Google US English'}
    const desired2 = {voiceURI: 'Google UK English Male'}

    expect(getBestFromAvailable(mockAvailableData, [desired1])).toEqual(desired1)
    expect(getBestFromAvailable(mockAvailableData, [{voiceURI: 'not exist'}])).toEqual(undefined)
    expect(getBestFromAvailable(mockAvailableData, [{voiceURI: 'not exist'}, desired1])).toEqual(desired1)
    expect(getBestFromAvailable(mockAvailableData, [desired1, desired2])).toEqual(desired1)
    expect(getBestFromAvailable(mockAvailableData, [desired2, desired1])).toEqual(desired2) 
})