import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';

import { initStore } from './Store';
import App from './App';
import './index.scss';
import * as serviceWorker from './registerServiceWorker';
import history from './history';


import { create } from 'jss';
import { StylesProvider, jssPreset } from '@material-ui/styles';

import { SnackbarProvider } from 'notistack';
import _ from 'lodash';
import { User } from './API';

const styleNode = document.createComment('jss-insertion-point');
document.head.insertBefore(styleNode, document.head.firstChild);

const jss = create({
  ...jssPreset() as any,
  // We define a custom insertion point that JSS will look for injecting the styles in the DOM.
  insertionPoint: 'jss-insertion-point',
});

history.listen((location, action) => {
  document.querySelectorAll('#critical-css').forEach(el => el.remove())
  // console.log(`The current URL is ${location.pathname}${location.search}${location.hash}`)
  // console.log(`The last navigation action was ${action}`)
})

initStore({
  handleChange: {
    userInfo: (userInfo: User) => {
      let paymentStatus = _.get(userInfo, 'payment.status')
      let timeLeft;
      if (paymentStatus === 'basic') {
        const maxFriendsStack = 3;
        const oneFriendBonusTime = 7 * 24 * 60 * 60 * 1000;
        timeLeft = _.max(_.times(maxFriendsStack, (stackCount) => {
          const friend = _.sortBy(userInfo.invitedByMe, 'date').reverse()[stackCount];
          return friend ? new Date(friend.date + oneFriendBonusTime * stackCount) : 0;
        }))

        if (timeLeft > new Date()) {
          paymentStatus = 'standard'
        }
        console.log({timeLeft})
      }
      return {
        origin: userInfo,
        calculated: {
          paymentStatus,
          timeLeft: timeLeft,
        }
      }
    }
  }
})


ReactDOM.render((
  <Router history={history}>
    <SnackbarProvider maxSnack={5}>
      <App />
    </SnackbarProvider>
  </Router>
), document.getElementById('root') as HTMLElement
);
serviceWorker.default();
