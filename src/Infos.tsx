import { ObjectOf } from "./Utilities";
import _ from "lodash";

type IPaymentInfo = {
    train: number,
    packs: number,
}
export const paymentSet: ObjectOf<IPaymentInfo> = {
    'basic': {
        train: 5,
        packs: 2
    },
    'standard': {
        train: 10,
        packs: 4
    },
    'premium': {
        train: +Infinity,
        packs: +Infinity
    }
}

export const getUserLevelWordPriority = (userLevel, wordIndex) => {
    if (userLevel === 0) {
        return wordIndex > 400 ? 0.05 : 1
    }
    if (userLevel === 1) {
        return (wordIndex > 400 && wordIndex < 2000) ? 1 : 0.05 
    }

    return wordIndex > 2000 ? 1 : 0.05
}

export const spacedRepetitionGap = [
    4 * 60 * 60 * 1000, // 4h
    1 * 24 * 60 * 60 * 1000, // 1d
    2 * 24 * 60 * 60 * 1000, // 2d
    4 * 24 * 60 * 60 * 1000, // 4d
    8 * 24 * 60 * 60 * 1000, // 8d
    16 * 24 * 60 * 60 * 1000, // 16d
    32 * 24 * 60 * 60 * 1000, // 32d
    64 * 24 * 60 * 60 * 1000, // 64d
    128 * 24 * 60 * 60 * 1000, // 128d
    256 * 24 * 60 * 60 * 1000, // 256d
    512 * 24 * 60 * 60 * 1000, // 512d
    1024 * 24 * 60 * 60 * 1000, // 1024d
    2048 * 24 * 60 * 60 * 1000, // 2048d
    4096 * 24 * 60 * 60 * 1000, // 4096d
    4096 * 24 * 60 * 60 * 1000, // 4096d
    4096 * 24 * 60 * 60 * 1000, // 4096d
] 

export const getDefaultSpaceRepetitionForUser = (wordIndex: number, userLevel: number) => {
    const map = [
        [{to: +Infinity, group: 0}],
        [{to: 100, group: 2}, {to: 300, group: 1}],
        [{to: 100, group: 4}, {to: 300, group: 3}, {to: 600, group: 2}],
        [{to: 100, group: 5}, {to: 300, group: 4}, {to: 600, group: 3}, {to: 1200, group: 2}],
        [{to: 100, group: 7}, {to: 300, group: 6}, {to: 600, group: 5}, {to: 1200, group: 4}, {to: 1900, group: 3}],
    ]

    const userLevelInfo = map[userLevel]
    const part = _.find(userLevelInfo, info => info.to >= wordIndex)
    return part ? part.group : 0
}

export const allTrainingsInfo = {
    trainingEngRu: {
        name: 'Перевод слова',
        url: 'translate-eng-ru',
        hoverText: 'Выучи много английских слов, чтобы лучше читать тексты, разговаривать, смотреть видео и писать.'
    },
    trainingForTime: {
        name: 'Перевод слова за время',
        url: 'translate-for-time',
        hoverText: 'Научись вспоминать перевод слов моментально, чтобы быстро читать, а также, чтобы смотреть фильмы.'
    },
    trainingVoiceEngRu: {
        name: 'Распознавание на слух',
        url: 'translate-from-audio',
        hoverText: 'Научись понимать слова на слух, без этого в реальной жизни никак :)'
    },
    write: {
        name: 'Написание слов',
        url: 'write',
        hoverText: 'Научить писать английские слова без ошибок, этот навык будет полезным при переписке с иностранцами'
    }
}

export const dailyTasks = [
    {
        [allTrainingsInfo.trainingEngRu.url]: 3,    
        [allTrainingsInfo.trainingVoiceEngRu.url]: 2,
        article: 5,
    },
    {
        [allTrainingsInfo.write.url]: 2,
        [allTrainingsInfo.trainingForTime.url]: 3,
        article: 3,
    },
    {
        [allTrainingsInfo.trainingEngRu.url]: 3,
        [allTrainingsInfo.write.url]: 2,
        article: 4,
    },
    {
        [allTrainingsInfo.trainingForTime.url]: 2,
        [allTrainingsInfo.trainingVoiceEngRu.url]: 3,
        article: 3,
    },
    {
        [allTrainingsInfo.write.url]: 3,
        [allTrainingsInfo.trainingEngRu.url]: 2,
        article: 5,
    },
]

export const packsInfo = [
    { title: 'Dota 2', url: 'dota2' },
    { title: 'CS:GO', url: 'csgo' },
    { title: '150 самых популярных слов', url: 'top150' },
    { title: 'CSS + HTML', url: 'cssHtml' },
    { title: 'Пляжный отдых', url: 'beach'},
    { title: 'Интерфейс', url: 'interfacePack'},
    { title: 'Reddit', url: 'reddit'}
]