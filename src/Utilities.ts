import _ from 'lodash'
import { useState, useEffect, useReducer } from 'react';
import { subscribe, __store } from './Store';
import { IPropsStore, Store } from './interface';
import { paymentSet, allTrainingsInfo } from './Infos';

import Alert from 'react-s-alert'
import { trainStat } from './API';

export const getAllIndexes = (arr: any[], val: any) => {
    var indexes = [],
        i = -1;
    while ((i = arr.indexOf(val, i + 1)) != -1) {
        indexes.push(i);
    }
    return indexes;
}

export interface ObjectOf<T> {
    [K: string]: T;
}

export const getDateString = (date: Date) => {
    const isValid = date.getTime() === date.getTime()
    const monthArray = ["January", "February", "March",
        "April", "May", "June",
        "July", "August", "September",
        "October", "November", "December"]

    return isValid
        ? `${monthArray[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`
        : ``
}

export const dateForPayment = () => {
    const date = new Date()
    const month = `${date.getMonth() + 1}`.padStart(2, "0")
    const day = `${date.getDate()}`.padStart(2, "0")
    return `${date.getFullYear()}-${month}-${day} 00:00:00`
}

export const milliSecondToString = (milliSecond) => {
    let left = milliSecond
    const hours = Math.floor(milliSecond / (60 * 60 * 1000))
    milliSecond = milliSecond % (60 * 60 * 1000);
    const minutes = Math.floor(milliSecond / (60 * 1000))
    milliSecond = milliSecond % (60 * 1000)
    const seconds = Math.floor(milliSecond / 1000)

    return `${hours}:${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`
}



export const sortByKey = (obj: any, keyTransformer?: any) => {
    const withKey = _.map(obj, (text, key) => {
        return {
            index: keyTransformer ? keyTransformer(key) : +key,
            value: text
        }
    })
    return _.sortBy(withKey, 'index').map(item => item.value)
}

export const getPageState = () => {
    return window.PAGE_STATE ? window.PAGE_STATE : null
    // let result = null;
    // try {
    //     result = JSON.parse(window.PAGE_STATE)
    // } catch (e) {

    // }
    // return result;
}

export const sleep = (time) => {
    return new Promise(resolve => setTimeout(resolve, time))
}

export const pasreWord = (word): any => {
    if (!word) return { showMeFirst: '' }
    let showMeFirst = ''
    _.forEach(word.speeches, speech => {
        _.forEach(speech, row => {
            if (row.showMeFirst) {
                showMeFirst = row.ru
            }
        })
    })

    if (!showMeFirst) {
        const speech = _.maxBy(_.map(word.speeches) as any, _.size);
        showMeFirst = _.get(speech, '[0].ru')
    }
    return { showMeFirst } // TODO now we need only showMeFirst, in future we can return some additional stuff.
}

export const absoluteToFixed = ({ minTop, minBottom, defaultFixedTop, distanceOfStart = 100, element }) => {
    var easingFunction = (x) => {
        if (x >= distanceOfStart) {
            return (-1 / (2 * distanceOfStart)) * distanceOfStart * distanceOfStart + distanceOfStart
        }
        return (-1 / (2 * distanceOfStart)) * x * x + x
    }
    const easingTranslate = 0.5
    const onScroll = () => {
        const elementHeight = parseInt(getComputedStyle(element).height)
        const pageHeight = document.querySelector('html').scrollHeight
        const first = minTop === -1 ? -Infinity : minTop - defaultFixedTop - (easingTranslate * distanceOfStart)
        const second = minBottom === -1 ? +Infinity : pageHeight - (minBottom + elementHeight) - defaultFixedTop + (easingTranslate * distanceOfStart)
        const handleTop = () => {
            element.style.position = 'absolute'
            element.style.top = minTop + 'px'
        }
        const handleMiddle = (overTop, overBottom) => {
            element.style.position = 'fixed'
            if (overTop < overBottom) {
                if (minTop === -1) {
                    element.style.top = defaultFixedTop
                } else {
                    element.style.top = defaultFixedTop + easingTranslate * distanceOfStart - easingFunction(overTop) + 'px'
                }
            } else {
                if (minBottom === -1) {
                    element.style.top = defaultFixedTop
                } else {
                    element.style.top = defaultFixedTop - easingTranslate * distanceOfStart + easingFunction(overBottom) + 'px'
                }
            }
        }
        const handleBottom = () => {
            element.style.position = 'absolute'
            element.style.top = pageHeight - (minBottom + elementHeight) + 'px'
        }
        if (window.scrollY < first) {
            handleTop()
        } else if (window.scrollY < second) {
            handleMiddle(window.scrollY - first, second - window.scrollY)
        } else {
            handleBottom()
        }
    }
    window.addEventListener('scroll', onScroll)
}

export const speakText = ({ text, utteranceOptions = {}, onError }: { text: string, utteranceOptions?: Partial<SpeechSynthesisUtterance>, onError?: Function }) => {
    try {
        const utterance = new SpeechSynthesisUtterance(text);
        utterance.lang = 'en-US'
        // utterance.pitch = Math.random() * 2;
        // utterance
        utterance.volume = utteranceOptions.volume || 1
        const voice = getBestAvailableVoice();
        utterance.voice = voice as any;
        window.speechSynthesis.speak(utterance);
    } catch (e) {
        if (onError) {
            onError();
        } else {
            Alert.error("Произношение будет работать в Google Chrome (mobile)", {
                position: 'bottom',
                effect: 'myScale',
                timeout: 113500
            })
        }
    }
}

export const getBestFromAvailable = (available, desired) => {
    // let result = null;
    // _.some(desired, desiredItem => {
    //     return result = _.find(available, desiredItem) 
    // })
    // return result;
    // return _.find(desired, desiredItem => _.find(available, desiredItem) )

    return _.head(_.intersectionBy(desired.map(it => _.find(available, it)), available, 'voiceURI'))
}

export const getBestAvailableVoice = () => {
    const bestVoice = [
        { voiceURI: 'Google UK English Male', },
        { voiceURI: 'Google US English' },
        { voiceURI: 'Alex' },
        { voiceURI: 'Daniel' },
        { voiceURI: 'Fiona' },
        { voiceURI: 'Fred' },
        { voiceURI: 'Jorge' },
        { voiceURI: 'Microsoft Zira Desktop - English (United States)' },
    ]
    return getBestFromAvailable(speechSynthesis.getVoices(), bestVoice)
}

export const mapReplace = (str: string, map) => {
    var regex = [];
    for (var key in map)
        regex.push(key.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"));
    return str.replace(new RegExp(regex.join('|'), "g"), function (word) {
        return map[word];
    });
};

// IMPORTANT, I am not sure this peace of code is 'Utilities', maybe we need move it to different file?

export let isUserAndUserInfoLoadedResolve = null;
export const isUserAndUserInfoLoaded = new Promise((resolve) => {
    isUserAndUserInfoLoadedResolve = resolve;
})

export const useStore = () => {
    const [ignored, forceUpdate] = useReducer(x => x + 1, 0)
    // Because store everytime reference to the same object, useState don't cause rerender.
    // we need analog of forceUpdate
    const [store, setStore] = useState(__store);
    useEffect(() => subscribe('_ALL', (value) => {
        setStore(value)
        forceUpdate(ignored);
    }), []);
    return { ...store, ...store.calculated } as Store;
}

export const useIntervalRerender = () => {
    const [oldTickValue, makeTick] = useState(1);

    useEffect(() => {
        const timerID = setInterval(() => makeTick(oldTickValue + 1), 1000);
        return () => clearInterval(timerID);
    });
}

export const shuffleWithPriority = <T>(items: Array<{ el: T, priority: number }>) => {
    const ramdomizeItems = items.map(item => ({ ...item, randomPriority: item.priority / (0.001 + Math.random()) }))
    const sortedItems = _.sortBy(ramdomizeItems, it => -it.randomPriority)
    console.log({ sortedItems })
    return _.map(sortedItems, it => it.el)
}


const MILLISECOND_TO_FULL_RESTORE = 10 * 60 * 60 * 1000
export const getEnergyInfo = () => {
    if (!__store.userInfo) { return {} }
    const userStats = _.get((__store as Store).userInfo, 'stats', {}) as ObjectOf<ObjectOf<{ date: number, isNow?: boolean }>>


    const maxAllowedEnergy = _.get(paymentSet, [__store.paymentStatus, 'train'], 5);
    const MILLISECOND_TO_ONE_RESTORE = MILLISECOND_TO_FULL_RESTORE / maxAllowedEnergy

    const allStats = _.sortBy(_.flatMap(allTrainingsInfo, ({ url }) => _.map(userStats[url])), 'date')
    console.log({ allStats: _.map(allStats, item => ({ ...item, bDate: new Date(item.date) })) })
    // const spentTrain = allStats
    //     .filter(train => train.date > (Date.now() - MILLISECOND_TO_FULL_RESTORE))
    //     .length

    let { milliSecondToRestore, timeLast } = _.reduce(allStats, (acc, train) => {
        const timeDiff = acc.timeLast
            ? train.date - acc.timeLast
            : 0

        const newMilliSecondToRestore = Math.min(
            MILLISECOND_TO_FULL_RESTORE, // We cannot have milliSecondToRestore larger then maximum time to restore
            Math.max(0, acc.milliSecondToRestore - timeDiff) + MILLISECOND_TO_ONE_RESTORE // In case timeDiff larger then milliSecondToRestore we return 0 + MILLISECOND_TO_ONE_RESTORE
        )

        return {
            milliSecondToRestore: newMilliSecondToRestore,
            timeLast: train.date
        }
    }, { milliSecondToRestore: 0, timeLast: null })
    milliSecondToRestore = Math.min(MILLISECOND_TO_FULL_RESTORE, Math.max(0, milliSecondToRestore - (Date.now() - timeLast)));

    const energy = Math.floor((MILLISECOND_TO_FULL_RESTORE - milliSecondToRestore) / MILLISECOND_TO_ONE_RESTORE);
    const isTrainsLock = energy <= 0;

    // console.log({userStats, maxAllowedEnergy, energy, isTrainsLock, milliSecondToRestore, __store, paymentSet, timeWhenFullRestore})

    console.count('getEnergyInfo') // TODO something really strange happens here, this function called 92 times after each refresh page
    const formattedTimeToRestoreOneEnergy = milliSecondToString(milliSecondToRestore % MILLISECOND_TO_ONE_RESTORE)
    return { userStats, maxAllowedEnergy, energy, isTrainsLock, milliSecondToRestore, MILLISECOND_TO_FULL_RESTORE, MILLISECOND_TO_ONE_RESTORE, formattedTimeToRestoreOneEnergy }
}

export const escapeHtml = (text: string = '') => {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

export const replaceFirebaseKey = (text: string = '') => {
    return text
        .split('.').join('_')
        .split('#').join('_')
        .split('$').join('_')
        .split('/').join('_')
        .split('[').join('_')
        .split(']').join('_')
}
