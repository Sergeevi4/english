import _words from './word.json'
import _ from 'lodash'
import { API } from './API';
import { replaceFirebaseKey } from './Utilities';

const words = _words as any;

const parse = async () => {
    return null; // Do nothing
    const currentWords = await API.words().once();

    
    // console.log({currentWords})

    const correctSpeechName = {
        '_n.': 'noun',
        '_v.': 'verb',
        '_adv.': 'adverb',
        '_a.': 'adjective',
        '_interj.': 'interjection',
        '_prep.': 'preposition',
    }

    const keys = _.slice(_.toArray(words), 0, 500 * 1000);
    console.log({keys})

    const newWordsResult = {...currentWords};
    
    _.forEach(keys, (word, index) => {

        
        const currentWord = newWordsResult[word.word]
        // if (index % 500 === 0) {
        //     console.log('currentProgress: ', index, JSON.stringify(currentWord), JSON.stringify(_.get(currentWord, 'finishedCorrectness')));
        // }
        if (_.get(currentWord, 'finishedCorrectness') && _.get(currentWord, 'finishedCorrectness.main') !== 'autoFromMuller') { 
            console.log(`We don't change ${word.word} because it is already handle muy human`)
            return
         }
        let newWord = {
            word: word.word,
            transcription: word.trans,
            speeches: {},
            finishedCorrectness: {
                main: 'autoFromMuller'
            }
        }

        let speeches = word.speeches;

        if (word.speeches['_pl.']) {
            if (word.speeches['_pl.'].length === 1) {
                const replaceWordKey = word.speeches['_pl.'][0].split(' ')[1];
                const replaceNewWord = words[replaceWordKey]
                if (!replaceNewWord) {
                    console.log(`${replaceWordKey} doesn't exist :(`)
                    return;
                }
                newWord = {
                    word: replaceNewWord.word,
                    transcription: replaceNewWord.trans,
                    speeches: {},
                    finishedCorrectness: {
                        main: 'autoFromMuller'
                    }
                }
                speeches = replaceNewWord.speeches;
                console.log({replaceWordKey, replaceNewWord, word, newWord, speeches})
            }
            
           
            // newWord = {
            //     word: word.word,
            //     trans: word.trans,
            //     speeches: {},
            //     finishedCorrectness: {
            //         main: 'autoFromMuller'
            //     }
            // }
        }


        _.forEach(speeches, ((speech, speechName) => {
            const newSpeechName = correctSpeechName[speechName] || 'ERROR_' + speechName.split('.').join('_point_');
            
            if (! correctSpeechName[speechName]) {
                // console.warn(speechName, word)
                console.count('smthWrong')
            }

            newWord.speeches[newSpeechName] = _.map(speech, translate => {
                const parse = translate.split(";")
                const ru = parse.shift();
                const examples = parse.map(example => {
                    const removeMeta = /(_[a-zA-Zа-яёА-Яё]+.)/
                    const [, meta]: any = example.match(removeMeta) || []
                    const clearExample = example.replace(removeMeta, '')
                    const [,eng, ru]: any = clearExample.match(/([a-zA-Z\-.?!)(,: ]+) ([а-яёА-Яё\-.?!)(,: ]+)/) || []
                    return {eng: eng || null, ru: ru || null, meta: meta || null}
                })
                // console.log(examples)
                // console.log({ru, examples, translate, 1: translate.split(";")})
                return {ru, examples: _.filter(examples, it => _.some(it, part => part))}
            })
        }))
        newWordsResult[replaceFirebaseKey(word.word)] = newWord;
        // if (index % 500 === 0) {
        //     console.log('currentProgress: ', index, newWord);
        // }
        // return newWord
    })





    console.log('retina', newWordsResult.retina) // test parsed word
    console.log('get', newWordsResult.get) // test human filled word



    API.words().update(newWordsResult);
    // console.log(result)
}

parse();