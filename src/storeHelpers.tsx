import { __store } from "./Store";
import _ from "lodash";
import { packsInfo, paymentSet, dailyTasks } from "./Infos";
import { Store } from "./interface";
import { trainStat } from "./API";
import { ObjectOf } from "./Utilities";

export const packsStuff = (packId?: string) => {
    const packs = _.get(__store, 'userInfo.packs', {})
    const openedPackCount = _.filter(packsInfo, info => packs[info.url]).length;
    const canOpenPackCount = _.get(paymentSet, `[${__store.paymentStatus}].packs`, 2);
    const canOpenPack = openedPackCount < canOpenPackCount;
    const isOpen = packs[packId]

    return {
        openedPackCount,
        // canOpenPackCount, // private
        canOpenPack,
        packs,
        isOpen
    }
}

function roundDate(timeStamp){
    timeStamp -= timeStamp % (24 * 60 * 60 * 1000);//subtract amount of time since midnight
    timeStamp += new Date().getTimezoneOffset() * 60 * 1000;//add on the timezone offset
    return timeStamp;
}

const dayInMs = 24 * 60 * 60 * 1000

export const dailyProgress = () => {
    const stats = _.get(__store, 'userInfo.stats', {}) as Store["userInfo"]["stats"]
    for(let i = 6; i >= 1; i-- ){
        const allPass = _.every(_.times(i), (unused, day) => {
            const dayTasks = dailyTasks[day]
            const dayAgo = i - day
            return _.every(dayTasks, (count, taskType) => {
                const typeStats = stats[taskType] as ObjectOf<trainStat>
                // if(!typeStats) return false
                const dayStats = _.filter(typeStats, (item) => {
                    return roundDate(Date.now() - dayAgo * dayInMs) > item.date &&
                    roundDate(Date.now() - (dayAgo - 1) * dayInMs) < item.date 
                })
                return dayStats.length >= count
            })
        })
        if(allPass) return i 
    }
    return 0
}