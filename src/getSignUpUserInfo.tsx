import { top150 } from "./Components/Training/trainingWord";
import _ from "lodash";
import { API, User } from "./API";
import { database } from "firebase/app";

export const getSignUpUserInfo = (isNewUser: boolean, user) => {
    const startWords = {}
    _.forEach(top150, word => {
        startWords[word] = {added: true}
    });

    const inviteUid = localStorage.getItem('inviteUid')
    if (inviteUid) {
        API.user(inviteUid).value.child('/invitedByMe').push({uid: user.uid, date: database.ServerValue.TIMESTAMP})
    }
    return isNewUser
        ? { 
            packs: { top150: true }, 
            payment: { status: 'basic', updateDate: null },
            userWords: startWords,
            ...(inviteUid ? {wasInvitedBy: inviteUid} : {})
        } 
        : {}
}