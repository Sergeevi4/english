import * as React from 'react';
const store = {};
window.store = store;

export const __store = store;

const subscribers = {};
let onChangeKey = {};
export const initStore = ({handleChange}) => {
  onChangeKey = handleChange;
}
export const subscribe = (path, callback) => {
  if (!subscribers[path]) {
    subscribers[path] = [];
  }
  subscribers[path].push(callback);
  return () => {
    subscribers[path] = subscribers[path].filter(item => item !== callback);
  }
};
const pub = path => {
  if (subscribers[path]) {
    subscribers[path].forEach(callback => {
      callback(store);
    });
  }
  if (subscribers["_ALL"]) {
    subscribers["_ALL"].forEach(callback => {
      callback(store);
    });
  }
};

const pubHelper = obj => {
  for (let objKey in obj) {
    pub(objKey);
  }
};


// TODO I comment it out, because currently we don't use it, and probably never will.
// export const update = obj => {
//   for (let objKey in obj) {
//     store[objKey] = {
//       ...store[objKey],
//       ...obj[objKey]
//     };
//   }
//   pubHelper(obj);
// };

export const set = obj => {
  for (let objKey in obj) {
    if (onChangeKey[objKey]) {
      const val =  onChangeKey[objKey](obj[objKey]);
      store[objKey] = val.origin;
      Object.assign(store, val.calculated)
    } else {
      store[objKey] = obj[objKey];
    }
    
  }
  pubHelper(obj);
};

export const connect = path => ReactComponent => {
  return class ConnectedComponent extends React.Component {
    componentDidMount() {
      this.unsubscribe = subscribe(path, () => {
        console.log('simpeDux sub', ReactComponent.name)
        this.forceUpdate();
      })
    }
    componentWillUnmount() {
      console.log('simpeDux unMount', ReactComponent.name)
      this.unsubscribe();
    }
    render() {
      console.log('simpeDux redner', ReactComponent.name)
      return <ReactComponent {...this.props} store={{...store, ...store.calculated}} />;
    }
  };
};
