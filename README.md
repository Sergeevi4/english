## Mixins documentation



### flexCenter

Does not accept any parameters.
Aligns the element in the center.

#### Same as:
```
    display: flex
    align-items: center
    justify-content: center
```


### widthHeight(value, value)

Accept two parameters, width and height

#### Same as:
```
    width: value
    height: value
```


### positionTRBL(value, value, value, value, value)

Accept 5 parameters.
First it's position type for example `absolute`,
4 other parameters it's: `top`, `right`, `bottom`, `left` respectively.
For skip some parameters use `_`

#### Same as:
```
    position: value
    left: value
    top: value
    right: value
    bottom: value
```


### gradientText

Does not accept any parameters.
Changes text color for 'gradient'

#### Same as:
```
    color:#35b6e8
    background: #FFF
```


### res-to(value)

Accept one parameter - `resolution`

#### Same as:
```
    @media (max-width: (value))
```


### res-from(value)

Accept one parameter - `resolution`

#### Same as:
```
    @media (min-width: (value))
```


### res(value, value)

Accept two parameters - `mix-width` and `max-width`

#### Same as:
```
    @media (min-width: value) and (max-width: (value))
```